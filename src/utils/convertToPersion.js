
export function convertTypeTransaction(value) {
    
    switch (value) {
        case"increase":
            return 'فروش'

        case"decrease":
            return 'خرید'
        
        case"commission":
            return 'کمیسیون'

        case"withdraw_transaction_type":
            return 'برداشت'
        
        case"deposit_transaction_type":
            return 'واریز دستی'

        case"online_deposit_type":
            return 'واریز آنلاین'
            
        case"penalty":
            return 'جریمه'

        default:
            return ''
    }
}

export function convertTypeMessage(value) {
    
    switch (value) {
        case"automatic":
            return 'سیستمی'

        case"manual":
            return 'دستی'
        


        default:
            return ''
    }
}

export function convertTypeMessageStatus(value) {
    
    switch (value) {
        case"register":
            return 'ثبت‌نام'

        case"approve":
            return 'احراز هویت'
        
        case"harvest":
            return 'واریز'

        case"deposit":
            return 'برداشت'
        
        case"cancel_deposit":
            return 'لغو برداشت'

        case"not_approve":
            return 'عدم تایید احراز هویت'
            
        case"create_payment":
            return 'درخواست واریز'
        
        case"approve_payment":
            return 'تایید واریز'
            
        case"disagrement_payment":
            return 'عدم تایید واریز'
            
        case"create_withdraw":
            return 'درخواست برداشت'
            
        case"approve_withdraw":
            return 'تایید برداشت'
            
        case"cancel_withdraw":
            return 'لغو برداشت'
            
        case"disagrement_withdraw":
            return 'عدم تایید برداشت'
            
        case"update_profile":
            return 'بروزرسانی پروفایل'
            
        case"conflict_user_info":
            return 'مغایرت اطلاعات'
            
        case"sell":
            return 'فروش'
        
        case"buy":
            return 'خرید'
        
        case"invite":
            return 'کد دعوت'
            
        case"use_invite":
            return 'استفاده از کد دعوت'
            
        case"new_project":
            return 'پروژه جدید'
            
        case"above_fifty":
            return 'واریز میلیونی' 
            
        case"manual":
            return 'دستی'

        default:
            return ''
    }
}



