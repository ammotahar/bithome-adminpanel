const { file_fetcher } = require("store/common/action")

function download(blob, filename) {
    const url = window.URL.createObjectURL(blob)
    const a = document.createElement("a")
    a.style.display = "none"
    a.href = url
    // the filename you want
    a.download = filename
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
    window.URL.revokeObjectURL(url)
  }



export const exportExcelFile = (url , file_name) => {

    file_fetcher(url, {
      method: "GET",
    })
      .then(res => {
        res.blob().then(blob => download(blob, file_name))
      })
      .catch(e => {
        console.log(e)
      })
  }