module.exports = {
  projects: "/v1/projects/",
  projects_delete: (id) => `/v1/projects/${id}/`,
  projects_edit: (id) => `/v1/admin/projects/${id}/`,
  projects_detail: (id) => `/v1/projects/${id}/detail/`,
  projects_types: "/v1/projects/types/",
  media_types: "/v1/media/media_type/",
  media_upload: "/v1/media/upload/",
  media: "v1/media/"
}