import React from 'react';
import 'izitoast/dist/css/iziToast.min.css'
import 'izitoast/dist/js/iziToast';
import iziToast from 'izitoast/dist/js/iziToast';

const Message = (props) => {
  const click = () => {
    iziToast[props.type || 'info']({
      title: props.title || '',
      message: props.message || ''
    });
  }
  return (
    <div>
      <button onClick={click}>please click to show message</button>
    </div>
  );
};

export default Message;

export const MessageToast = (type="info", message="", title="",position='topCenter',timeout=3000) => {
  if(type)
  iziToast[type]({
    rtl: true,
    position: position,
    title: title,
    message: message,
    timeout: timeout,
  });
};
// bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center