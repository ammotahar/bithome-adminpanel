import React from "react"
import { Container, Row, Col } from "reactstrap"

const Footer = () => {
  return (
    <React.Fragment>
      <footer style={{position : 'fixed' , bottom : '0px' , left : '0px'}} className="footer">
        <Container fluid={true}>
          <Row>
            <Col md={6}>{new Date().getFullYear()} © AmnMoj</Col>
            <Col md={6}>
              <div className="text-sm-right d-none d-sm-block">
                طراحی و گسترش توسط  تجارت الکترونیک امن موج
              </div>
            </Col>
          </Row>
        </Container>
      </footer>
    </React.Fragment>
  )
}

export default Footer
