import PropTypes from "prop-types"
import React, { useEffect, useState } from "react"

// MetisMenu
import MetisMenu from "metismenujs"
import { withRouter } from "react-router-dom"
import { Link } from "react-router-dom"

//i18n
import { withTranslation } from "react-i18next"
import { BiBuildingHouse, FaChevronDown, FaFolder, FaUserCheck, MdDashboard } from "react-icons/all"
import { fetcher } from "../../store/common/action"
import { GET_COUNT_CHATS, GET_COUNT_CONTACTS, GET_COUNT_DEPOSIT, GET_COUNT_WITHDRAW, GET_USER_PERMISSIONS } from "../../helpers/url_helper"


const SidebarContent = props => {
  // Use ComponentDidMount and ComponentDidUpdate method symultaniously
  const [countTicket,setCountTicket]=useState()
  const [userPermissions,setUserpermissions]=useState([])
 let permissions = []



  useEffect(() => {
    const pathName = props.location.pathname

    const initMenu = () => {
      new MetisMenu("#side-menu")
      let matchingMenuItem = null
      const ul = document.getElementById("side-menu")
      const items = ul.getElementsByTagName("a")
      for (let i = 0; i < items.length; ++i) {
        if (pathName === items[i].pathname) {
          matchingMenuItem = items[i]
          break
        }
      }
      if (matchingMenuItem) {
        activateParentDropdown(matchingMenuItem)
      }
    }
    initMenu()
  }, [props.location.pathname])


  useEffect(()=>{
    getUserPermissions();
    getCountTicket();
  },[])

  // GET COUNT TICKET FUNCTION
  const getCountTicket = () => {
    fetcher(GET_COUNT_CHATS, {
      method: "GET"
    }).then(r => {
      if(r?.result)
        setCountTicket(r?.result?.count)
    }).catch(e => {
      // MessageToast("error", "باخطا مواجه شدید")
    })
  }  
  
  // GET USER PERMISSIONS FUNCTION 
  const getUserPermissions = () => {
    fetcher(GET_USER_PERMISSIONS, {
      method: "GET"
    }).then(r => {
      if(r?.result)
      setUserpermissions(r?.result)

    }).catch(e => {
      // MessageToast("error", "باخطا مواجه شدید")
    })
  }


  userPermissions?.map( permission => {
    permissions.push(permission?.permission?.title)
  })

  


  function activateParentDropdown(item) {
    item.classList.add("active")
    const parent = item.parentElement

    if (parent) {
      parent.classList.add("mm-active")
      const parent2 = parent.parentElement

      if (parent2) {
        parent2.classList.add("mm-show")

        const parent3 = parent2.parentElement

        if (parent3) {
          parent3.classList.add("mm-active") // li
          parent3.childNodes[0].classList.add("mm-active") //a
          const parent4 = parent3.parentElement
          if (parent4) {
            parent4.classList.add("mm-active")
          }
        }
      }
      return false
    }
    return false
  }


  const [countWithdrawal , setCountWithdrawal] = useState(0)
  const [countDeposit , setCountDeposit] = useState(0)

  useEffect(() => {
    getCountWithdrawal();
    getCountDeposit();
  }, [countWithdrawal , countDeposit])

  const getCountWithdrawal = () => {
    fetcher(GET_COUNT_WITHDRAW, {
      method: "GET"
    }).then(r => {
      setCountWithdrawal(r?.result?.payments_count)
    }).catch(e => {
      // MessageToast("error", "باخطا مواجه شدید")
    })
  }
  
  const getCountDeposit = () => {
    fetcher(GET_COUNT_DEPOSIT, {
      method: "GET"
    }).then(r => {
      setCountDeposit(r?.result?.payments_count)
    }).catch(e => {
      // MessageToast("error", "باخطا مواجه شدید")
    })
  }

  return (
    <React.Fragment>
      <div id="sidebar-menu">
        <ul className="metismenu list-unstyled" id="side-menu">
          <li className="menu-title">{props.t("Menu")} </li>
          
          <li  className={!permissions.includes("dashboard") && "d-none"}>
            <Link to="/#" className="waves-effect">
              <i><MdDashboard /> </i>
              {/* <span className="badge badge-pill badge-info float-right">
                03
              </span> */}
              <span>{props.t("Dashboards")}</span>
            </Link>
            
          </li>
          {permissions?.includes("user_permissions") ? <li >
            <Link to="/admin" className="">
              <i><FaUserCheck /> </i>
              <span >سطوح دسترسی</span>
            </Link>
          </li> : null}
          


          {permissions.includes("create_project") ? <li>
            <Link to="/projects-list" className="waves-effect">
              <i className="bx bx-folder"></i>
              <span>{props.t("Projects")}</span>
            </Link>

          </li> : null}
         {permissions.includes("create_estate") ? <li>
            <Link to="/estate-list" className="waves-effect">
              <i><BiBuildingHouse /></i>
              <span>املاک</span>
            </Link>
          </li> : null}


              <li className={!permissions.includes("approve_user") && "d-none"}>
     
              <Link to="/all-contact-list" className="has-arrow waves-effect">
                <i className="bx bxs-user-detail"></i>
                <span>کاربران</span>
              </Link>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <Link to="/all-contact-list">همه کاربران</Link>
                </li>
                <li>
                  <Link to="/new-contacts-list">کاربران جدید</Link>
                </li>
                <li>
                  <Link to="/confirm-contacts-list">کاربران تایید شده</Link>
                </li>       
                <li>
                  <Link to="/customers-contact-list">مشتریان</Link>
                </li>     
              </ul>
      
            </li> 


             <li className={!permissions.includes("accept_withdraw") && "d-none"}>
            <Link to="/invoices-deposit" className="has-arrow waves-effect">
              <i className="bx bx-receipt"></i>
              <span>حسابداری</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">

             <li>
                <Link to="/invoices-deposit">
                {countDeposit ? <span className="badge badge-pill badge-success float-right">{countDeposit}</span> : ""}
                    <span>واریز بانکی</span>
                </Link>
              </li> 
              <li>
                <Link to="/invoices-list"> تراکنش ها</Link>
              </li>

              <li>
                <Link to="/invoices-withdraw" className=" waves-effect">
                    {countWithdrawal ? <span className="badge badge-pill badge-success float-right">{countWithdrawal}</span> : ""}
                    <span>تایید برداشت</span>
                </Link>
              </li>

            </ul>
          </li>

          <li>
            <Link to="/history-order" className="has-arrow waves-effect">
              <i className="bx bx-history"></i>
              <span>تاریخچه</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to="/history-order">تاریخچه سفارش ها </Link>
              </li>
              <li>
                <Link to="/history-trade">تاریخچه  معاملات </Link>
              </li>
            </ul>
          </li>

          <li>
            <Link to="" className="has-arrow waves-effect">
              <i className="bx bxs-report"></i>
              <span>گزارشات</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to="/report-contact"> گزارش کاربران</Link>
              </li>   
              <li>
                <Link to="/report-wallets"> گزارش کیف پول</Link>
              </li>  
              <li>
                <Link to="/report-trade-customers"> گزارش  معامله مشتریان</Link>
              </li>
              <li>
                <Link to="/report-detailed-transaction"> گزارش تفضیلی معاملات</Link>
              </li>
              <li>
                <Link to="/report-log-contacts">گزارش لاگ کاربران</Link>
              </li>
              <li>
                <Link to="/report-assets-customers">گزارش دارایی مشتریان</Link>
              </li>
            </ul>
          </li>



          <li  className={!permissions.includes("send_message") && "d-none"}>
            <Link to="/#" className="has-arrow waves-effect">
              <i className="bx bx-message-alt"></i>
              <span>پیام ها</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to="/message-send">  ارسال پیام جدید </Link>
              </li>
              <li>
                <Link to="/message-history">    لیست پیام های ارسال شده </Link>
              </li>       
              <li>
                <Link to="/message-ready-history">    لیست پیام‌های آماده   </Link>
              </li>
            </ul>
          </li>

          {permissions.includes("answer_ticket") ? <li>
            <Link to="/chat" className=" waves-effect">
              <i className="bx bx-chat"></i>
              {countTicket ? <span className="badge badge-pill badge-success float-right">
                {countTicket}
              </span> : ""}
              <span>تیکت</span>
            </Link>
          </li> : null}

          
        </ul>
      </div>
    </React.Fragment>
  )
}

SidebarContent.propTypes = {
  location: PropTypes.object,
  t: PropTypes.any
}

export default withRouter(withTranslation()(SidebarContent))
