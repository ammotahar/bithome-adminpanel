import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap"

//i18n
import { withTranslation } from "react-i18next"
// Redux
import { connect } from "react-redux"
import { withRouter, Link } from "react-router-dom"
import { FaChevronDown } from "react-icons/all"
import { getProfile } from "../../../store/actions"
import { LOGOUT } from "../../../helpers/url_helper"
import { fetcher } from "store/common/action"

import {logoutUser} from "../../../store/auth/login/actions"
// users
// import user1 from "../../../assets/images/users/avatar-1.jpg"

const ProfileMenu = props => {
  // Declare a new state variable, which we'll call "menu"
  const [menu, setMenu] = useState(false)

  const [username, setusername] = useState("Admin")

  useEffect(() => {
    props.getProfile()

  }, [])

  useEffect(() => {
    if (localStorage.getItem("authUser")) {
      if (process.env.REACT_APP_DEFAULTAUTH === "firebase") {
        const obj = JSON.parse(localStorage.getItem("authUser"))
        setusername(obj.displayName)
      } else if (
        process.env.REACT_APP_DEFAULTAUTH === "fake" ||
        process.env.REACT_APP_DEFAULTAUTH === "jwt"
      ) {
        // const obj = JSON.parse(localStorage.getItem("authUser"))
        // setusername(obj.username)

      }
    }
  }, [props.success])

  const handleLogout = () =>{
    fetcher(LOGOUT, {method: "GET"},{})
    .then(r => {

    if (r?.result) {
      props.logoutUser()
     setTimeout(() => {
      window.location.href = '/login';
     }, 700);
    }
  }).catch(e => {
    // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
  })
  }

  return (
    <React.Fragment>
      <Dropdown
        isOpen={menu}
        toggle={() => setMenu(!menu)}
        className="d-inline-block"
      >
        <DropdownToggle
          className="btn header-item waves-effect"
          id="page-header-user-dropdown"
          tag="button"
        >
          {/*<img*/}
          {/*  className="rounded-circle header-profile-user"*/}
          {/*  src={user1}*/}
          {/*  alt="Header Avatar"*/}
          {/*/>*/}
          <span className="d-none d-xl-inline-block ml-2 mr-1">{
            (props.success?.credentials?.first_name || "") + " " + (props.success?.credentials?.last_name || "")
          }</span>
          <i className="d-none d-xl-inline-block font-size-10"><FaChevronDown /></i>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem tag="a" href="/profile">
            {" "}
            <i className="bx bx-user font-size-16 align-middle mr-1" />
            {props.t("Profile")}{" "}
          </DropdownItem>
          {/*<DropdownItem tag="a" href="/crypto-wallet">*/}
          {/*  <i className="bx bx-wallet font-size-16 align-middle mr-1" />*/}
          {/*  {props.t("My Wallet")}*/}
          {/*</DropdownItem>*/}
          {/*<DropdownItem tag="a" href="#">*/}
          {/*  <span className="badge badge-success float-right">11</span>*/}
          {/*  <i className="mdi mdi-settings font-size-17 align-middle mr-1" />*/}
          {/*  {props.t("Settings")}*/}
          {/*</DropdownItem>*/}
          {/*<DropdownItem tag="a" href="auth-lock-screen">*/}
          {/*  <i className="bx bx-lock-open font-size-16 align-middle mr-1" />*/}
          {/*  {props.t("Lock screen")}*/}
          {/*</DropdownItem>*/}
          <div className="dropdown-divider" />

          <DropdownItem onClick={handleLogout} to="/logout" className="dropdown-item">
            <i className="bx bx-power-off font-size-16 align-middle mr-1 text-danger" />
            <span>{props.t("Logout")}</span>
          </DropdownItem>

        </DropdownMenu>
      </Dropdown>
    </React.Fragment>
  )
}

ProfileMenu.propTypes = {
  success: PropTypes.any,
  t: PropTypes.any
}

const mapStatetoProps = state => ({
  success: state.Profile?.success?.result || {} ,
  // auth : state.auth.login
})
const mapDispatchToProps = dispatch => ({
  getProfile: () => dispatch(getProfile()),
  logoutUser: () => dispatch(logoutUser())
})
export default withRouter(
  connect(mapStatetoProps, mapDispatchToProps)(withTranslation()(ProfileMenu))
)
