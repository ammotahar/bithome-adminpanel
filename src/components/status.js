import React from "react"
import { SWEET_ALERT_PROP_TYPES } from "react-bootstrap-sweetalert/dist/prop-types"
import PropTypes from "prop-types"

const Status = ({ name }) => {
  return (
    <div>
      {(name === "pending") && <span className="font-size-12 badge-soft-warning badge badge-warning badge-pill">درحال انتظار</span>}
      {(name === "success") && <span className="font-size-12 badge-soft-success badge badge-success badge-pill"> موفقیت آمیز</span>}
      {(name === "active") && <span className="font-size-12 badge-soft-info badge badge-info badge-pill">  فعال</span>}
      {(name === "cancel") && <span className="font-size-12 badge-soft-danger badge badge-danger badge-pill">  لغو شده</span>}
      {(name === "failed") && <span className="font-size-12 badge-soft-danger badge badge-danger badge-pill">  ناموفق</span>}
    </div>
  )
}
Status.propTypes={
  name:PropTypes.string
}
export default Status