import React, { useState } from "react"
import { map } from "lodash"
import {
  Col,
  Pagination,
  PaginationItem,
  PaginationLink, Row
} from "reactstrap"

const PaginationCustom = (props) => {
  // const [pageSize,setPageSize]=useState(10)
  const { page, totalPage, handlePageClick ,chatsCount,addPageSize,pageSize} = props
  return (
    <Row>
      <Col xs="4">
        <div className="float-left">
          <select className="custom-select custom-select-sm ml-2"
                  defaultValue={pageSize}
                  onChange={(e)=> {
            // setPageSize(e.target.value)
            addPageSize(e.target.value)
            handlePageClick(1, e.target.value)
          }}>
            <option value="10" >
              10
            </option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
      </Col>
      <Col xs="4">
      <Pagination className="pagination pagination-rounded justify-content-center mt-2 mb-5 ">
        <PaginationItem disabled={page === 1}>
          <PaginationLink
            first
            href="#"
            onClick={() => handlePageClick( 1,pageSize)}
          />
        </PaginationItem>
        <PaginationItem disabled={page === 1}>
          <PaginationLink
            previous
            href="#"
            onClick={() => handlePageClick(page - 1,pageSize)}
          />
        </PaginationItem>
        { map(([page-3 ,page-2,page-1,page ,page+1 ,page+2 ,page+3]), (item, i) => (
          ((item > 0) && (item<=totalPage)) ? <PaginationItem active={item === page} key={"PaginationItem"+i}>
        {/*{console.log(item)}*/}
          <PaginationLink
          onClick={() => handlePageClick(item,pageSize)}
          href="#"
          >
        {item}
          </PaginationLink>
          </PaginationItem> :""
        ))}
        {/*<PaginationItem active={true}>*/}
        {/*  /!*{console.log(item)}*!/*/}
        {/*  <PaginationLink*/}
        {/*    onClick={() => handlePageClick(page)}*/}
        {/*    href="#"*/}
        {/*  >*/}
        {/*    {page}*/}
        {/*  </PaginationLink>*/}
        {/*</PaginationItem>*/}

        <PaginationItem disabled={page === totalPage}>
          <PaginationLink
            next
            href="#"
            onClick={() => handlePageClick(page + 1,pageSize)}
          />
        </PaginationItem>
        <PaginationItem disabled={page === totalPage}>
          <PaginationLink
            last
            href="#"
            onClick={() => handlePageClick(totalPage,pageSize)}
          />
        </PaginationItem>
      </Pagination>
      </Col>
      <Col xs="4" className="text-right">
        <span>نمایش </span>
        <span> {page} </span>
        {/* <span> - </span> */}
        <span> از </span>
        <span> {totalPage || 0} </span>
        {/* <span>{chatsCount || 0} </span> */}
      </Col>
    </Row>
  )
}

export default PaginationCustom