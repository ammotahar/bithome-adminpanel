import axios from "axios"
import accessToken from "./jwt-token-access/accessToken"
//pass new generated access token here
const token = accessToken
//apply base url for axios
const API_URL = ""

const axiosApi = axios.create({
  baseURL: API_URL
})

axiosApi.defaults.headers.common["Authorization"] = token

axiosApi.interceptors.response.use(
  response => response,
  error => Promise.reject(error)
)

// export async function get(url, config = {}) {
//   return await axiosApi.get(url, { ...config }).then(response => response.data)
// }
//
// export async function post(url, data, config = {}) {
//   return axiosApi
//     .post(url, { ...data }, { ...config })
//     .then(response => response.data)
// }
//
// export async function put(url, data, config = {}) {
//   return axiosApi
//     .put(url, { ...data }, { ...config })
//     .then(response => response.data)
// }
//
// export async function del(url, config = {}) {
//   return await axiosApi
//     .delete(url, { ...config })
//     .then(response => response.data)
// }
import { Url, Header, getRereshObject } from "utils/utils"
// import { RESET, LOADING } from "store/common/type"
// import { media_upload } from "utils/constant"
// import {refreshOtp} from 'redux/auth/action';
import store from "store"
import { loading } from "../store/common/action"
// import { connect } from "react-redux"
// import { apiError, loginUser, socialLogin } from "../store/auth/login/actions"
// import {message} from "antd";

let refresh = true
let timeAction = null

// export function fetcher(url = Url("/url-not-filled", {}), header = {}, ext = { noAuth: true, toText: false }) {
//   // store.dispatch(loading(true))
//   return fetch(Url(url, ext["query"]), Header(header, ext.noAuth))
//     .then(async (response) => {
//       if (!response.ok) {//4xx-5xx
//         if ((response.status === 401) && refresh) {//401
//           timeAction = setTimeout(() => {
//             refresh = true
//           }, 6000)
//           // store.dispatch(refreshOtp({
//           //     "refresh": getRereshObject()
//           // }));
//           window.location = "/login"
//           refresh = false
//         }
//         if ((response.status === 403) && refresh) {//401
//           timeAction = setTimeout(() => {
//             refresh = true
//           }, 6000)
//           message.error("اجازه دسترسی ندارید")
//           refresh = false
//         }
//         if ((response.status === 429) && refresh) {//401
//           timeAction = setTimeout(() => {
//             refresh = true
//           }, 6000)
//           message.error("پس از لحظاتی دوباره امتحان کنید")
//           refresh = false
//         }
//
//         if (response?.status && (response?.status !== 403) && (response?.status !== 401) && (response?.status !== 429)) {
//           throw new Error((await response.json()).result || response.status)
//         }
//
//
//       }
//
//       // store.dispatch(loading(false))
//       return response
//     })
//     .then(r => r[ext.toText ? "text" : "json"]())//r["json"]() r.json()
//     .catch(e => {
//       store.dispatch(loading(false))
//
//       return Promise.reject(e)
//     })
//
// }