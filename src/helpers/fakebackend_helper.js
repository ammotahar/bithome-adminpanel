import axios from "axios"
import { fetcher } from "store/actions"
import * as url from "./url_helper"

// Gets the logged in user data from local session
const getLoggedInUser = () => {
  const user = localStorage.getItem("user")
  if (user) return JSON.parse(user)
  return null
}

//is user is logged in
const isUserAuthenticated = () => {
  return getLoggedInUser() !== null
}

// Register Method
const postFakeRegister = data => {
  return axios
    .fetcher(url.POST_FAKE_REGISTER, data)
    .then(response => {
      if (response.status >= 200 || response.status <= 299) return response.data
      throw response.data
    })
    .catch(err => {
      let message
      if (err.response && err.response.status) {
        switch (err.response.status) {
          case 404:
            message = "Sorry! the page you are looking for could not be found"
            break
          case 500:
            message =
              "Sorry! something went wrong, please contact our support team"
            break
          case 401:
            message = "Invalid credentials"
            break
          default:
            message = err[1]
            break
        }
      }
      throw message
    })
}

// Login Method
const postFakeLogin = data => fetcher(url.POST_FAKE_LOGIN, { method: "POST", body: JSON.stringify(data) })
const postFakeOtpLogin = data => fetcher(url.POST_OTP_FAKE_LOGIN, { method: "POST", body: JSON.stringify(data) })
const postRefreshToken = data => fetcher(url.POST_REFRESH(data))

// postForgetPwd
const postFakeForgetPwd = data => fetcher(url.POST_FAKE_PASSWORD_FORGET, data)

// Edit profile
const postJwtProfile = data => fetcher(url.POST_EDIT_JWT_PROFILE, data)

const postFakeProfile = data => fetcher(url.POST_EDIT_PROFILE, data)
const getFakeProfile = data => fetcher(url.GET_EDIT_PROFILE, data)

const getMoney = data => fetcher(url.GET_MONEY(data))
const closeTicket = data => fetcher(url.CLOSE_TICKET(data))

// Register Method
const postJwtRegister = (url, data) => {
  return axios
    .fetcher(url, data)
    .then(response => {
      if (response.status >= 200 || response.status <= 299) return response.data
      throw response.data
    })
    .catch(err => {
      let message
      if (err.response && err.response.status) {
        switch (err.response.status) {
          case 404:
            message = "Sorry! the page you are looking for could not be found"
            break
          case 500:
            message =
              "Sorry! something went wrong, please contact our support team"
            break
          case 401:
            message = "Invalid credentials"
            break
          default:
            message = err[1]
            break
        }
      }
      throw message
    })
}

// Login Method
const postJwtLogin = data => fetcher(url.POST_FAKE_JWT_LOGIN, { method: "POST", body: JSON.stringify(data) })

// postForgetPwd
const postJwtForgetPwd = data => fetcher(url.POST_FAKE_JWT_PASSWORD_FORGET, data)

// postSocialLogin
export const postSocialLogin = data => fetcher(url.SOCIAL_LOGIN, data)

// get Products
export const getProducts = () => fetcher(url.GET_PRODUCTS)

// get Product detail
export const getProductDetail = id =>
  fetcher(`${url.GET_PRODUCTS_DETAIL}/${id}`, { params: { id } })

// get Events
export const getEvents = () => fetcher(url.GET_EVENTS)

// add Events
export const addNewEvent = event => fetcher(url.ADD_NEW_EVENT, event)

// update Event
export const updateEvent = event => fetcher(url.UPDATE_EVENT, event)

// delete Event
export const deleteEvent = event =>
  fetcher(url.DELETE_EVENT, { headers: { event } })

// get Categories
export const getCategories = () => fetcher(url.GET_CATEGORIES)

// get chats
export const getChats = (data) => fetcher(url.GET_CHATS, { method: "GET" },data)

// get groups
export const getGroups = () => fetcher(url.GET_GROUPS)

// get Contacts
export const getContacts = () => fetcher(url.GET_CONTACTS)

// get messages
export const getMessages = (roomId = "") =>
  fetcher(`${url.GET_MESSAGES}/${roomId}/`)

// post messages
export const addMessage = data => fetcher((url.ADD_MESSAGE(data?.id)), { method: "POST", body: JSON.stringify(data?.message) })

// get orders
export const getOrders = () => fetcher(url.GET_ORDERS)

// get cart data
export const getCartData = () => fetcher(url.GET_CART_DATA)

// get customers
export const getCustomers = () => fetcher(url.GET_CUSTOMERS)

// get shops
export const getShops = () => fetcher(url.GET_SHOPS)

// get wallet
export const getWallet = () => fetcher(url.GET_WALLET)

// get crypto order
export const getCryptoOrder = () => fetcher(url.GET_CRYPTO_ORDERS)

// get invoices
export const getInvoices = (query) => fetcher(url.GET_INVOICES,{},query)
export const getInvoicesDeposit = (query) => fetcher(url.GET_INVOICES_DEPOSIT,{},query)
export const getInvoicesWithdraw = (query) => fetcher(url.GET_INVOICES_WITHDRAW,{},query)

// get history
export const getHistoryOrder = (data) => fetcher(url.GET_HISTORY_ORDER, {}, data)
export const getHistoryTrade = (query) => fetcher(url.GET_HISTORY_TRADE,{},query)

// get invoice details
export const getInvoiceDetail = id =>
  fetcher(`${url.GET_INVOICE_DETAIL}/${id}`, { params: { id } })

// get project
export const getProjects = (data) => fetcher(url.GET_PROJECTS,{} ,data)

// create project
export const createProject = (data) => fetcher(url.GET_PROJECTS, { method: "POST", body: data })

export const getProjectsType = () => fetcher(url.GET_PROJECTS_TYPES, { method: "GET" })

// get project details
export const getProjectsDetails = (id) =>
  fetcher(`${url.GET_PROJECT_DETAIL}/${id}/estates/`)


// get States
export const getStates = (data) => fetcher(url.GET_States,{},data)

// create States
export const createStates = (data = "") => fetcher(url.GET_States, { method: "POST", body: data })
export const getEstatesType = () => fetcher(url.GET_ESTATES_TYPE, { method: "GET" })
export const getEstatesUsers = (data) => fetcher(url.GET_ESTATES_USERS, { method: "GET" })

// get States details
export const getStatesDetails = (id) =>
  fetcher(`${url.GET_States_DETAIL}${id}/`)

// get tasks
export const getTasks = () => fetcher(url.GET_TASKS)

// get contacts
export const getUsers = (data) => fetcher(url.GET_USERS, {}, data)

export const getUserProfile = () => fetcher(url.GET_USER_PROFILE)

export {
  getLoggedInUser,
  isUserAuthenticated,
  postFakeRegister,
  postFakeLogin,
  postFakeProfile,
  getFakeProfile,
  postFakeForgetPwd,
  postJwtRegister,
  postJwtLogin,
  postJwtForgetPwd,
  postJwtProfile,
  getMoney,
  postRefreshToken,
  postFakeOtpLogin,
  closeTicket,
  
}
