import { BASE_URL } from "utils"
//REGISTER
export const POST_FAKE_REGISTER = "/post-fake-register"

//LOGIN
export const POST_FAKE_LOGIN = "v1/users/login/"
export const POST_OTP_FAKE_LOGIN = "v1/users/login/two-step"
export const POST_REFRESH = token => "v1/users/login/refresh-token?token=" + token
export const POST_FAKE_JWT_LOGIN = "v1/users/login/"
export const POST_FAKE_PASSWORD_FORGET = "/fake-forget-pwd"
export const POST_FAKE_JWT_PASSWORD_FORGET = "/jwt-forget-pwd"
export const SOCIAL_LOGIN = "/social-login"
export const LOGOUT = "v1/users/logout/"

//PROFILE
export const POST_EDIT_JWT_PROFILE = "/post-jwt-profile"
export const POST_EDIT_PROFILE = "/v1/users/profile/"
export const GET_EDIT_PROFILE = "/v1/users/profile/"
export const GET_MONEY = id => "v1/admin/users/" + id + "/money/"

//PRODUCTS
export const GET_PRODUCTS = "/products"
export const GET_PRODUCTS_DETAIL = "/product"

//CALENDER
export const GET_EVENTS = "/events"
export const ADD_NEW_EVENT = "/add/event"
export const UPDATE_EVENT = "/update/event"
export const DELETE_EVENT = "/delete/event"
export const GET_CATEGORIES = "/categories"

//CHATS
export const GET_CHATS = "/v1/admin/ticketing/"
export const GET_GROUPS = "/groups"
export const GET_CONTACTS = "/contacts"
export const GET_MESSAGES = "/v1/ticketing"
export const ADD_MESSAGE = id => "/v1/admin/ticketing/" + id + "/reply/"
export const GET_COUNT_CHATS = "v1/admin/ticketing/count/"
export const CLOSE_TICKET = id => "v1/ticketing/" + id + "/close"
//ORDERS
export const GET_ORDERS = "/orders"

export const GET_COUNT_WITHDRAW = "/v1/admin/payments/count/?order_type=money_withdraw"
export const GET_COUNT_DEPOSIT = "/v1/admin/payments/count/?order_type=money_deposit_bank"

//CART DATA
export const GET_CART_DATA = "/cart"

//CUSTOMERS
export const GET_CUSTOMERS = "/customers"

//SHOPS
export const GET_SHOPS = "/shops"

//CRYPTO
export const GET_WALLET = "/wallet"
export const GET_CRYPTO_ORDERS = "/crypto/orders"

//INVOICES
export const GET_INVOICES = "/v1/admin/payments/transactions/"
export const GET_INVOICES_DEPOSIT = "v1/admin/payments/"
export const GET_INVOICES_WITHDRAW = "v1/admin/payments/"
export const GET_INVOICE_DETAIL = "/invoice"
export const GET_TYPES_TRANSACTION = "/v1/admin/types/"
export const GET_INVOICES_WITHDRAW_APPROVE = id => "v1/admin/payments/withdraw/" + id + "/approve/"
export const GET_INVOICES_WITHDRAW_CANCEL = id => "/v1/admin/payments/withdraw/" + id + "/cancel/"
export const GET_INVOICES_WITHDRAW_STATUS = id => "v1/admin/payments/withdraw/" + id + "/"
export const GET_INVOICES_DEPOSIT_APPROVE = id => "v1/admin/payments/deposit/" + id + "/"
export const GET_INVOICES_DEPOSIT_CANCEL = id => "/v1/admin/payments/deposit/" + id + "/cancel/"
export const DOWNLOAD_PIC_DIPOSIT = id => "/v1/admin/media/" + id + "/download/"


export const GET_HISTORY_ORDER = "v1/admin/orders/"
export const GET_HISTORY_TRADE = "v1/admin/trades/"

//PROJECTS
export const GET_PROJECTS = "/v1/admin/projects/"
export const GET_PROJECTS_POST = "/v1/admin/projects/"
export const GET_PROJECTS_TYPES = "/v1/projects/types/"
export const GET_PROJECT_DETAIL = "/v1/projects"
export const GET_COUNT_PROJECTS = "v1/admin/projects/count/"
//States
export const GET_States = "/v1/admin/estates/"
export const GET_States_POST = "/v1/admin/estates/"
export const GET_ESTATES_TYPE = "/v1/estates/types/"
export const GET_ESTATES_USERS = "/v1/admin/users/"

export const GET_States_DETAIL = (id) => "/v1/estates/" + id + "/detail/"
export const GET_States_DELETE = (id) => "/v1/estates/" + id + "/"
export const GET_COUNT_ESTATES = "v1/admin/estates/count/"
export const GET_LIST_ESTATE_PARTNERS =(id)=> "/v1/admin/estates/" + id + "/partners/"

//TASKS
export const GET_TASKS = "/tasks"

//CONTACTS
export const GET_USERS = "/v1/admin/users/"
export const GET_CUSTOMERS_LIST = "/v1/admin/users/customers/"
export const GGET_USERS_DETAIL = (id) => "/v1/admin/users/" + id + "/"
export const GET_USER_ChECK = (id) => "/v1/admin/users/" + id + "/approve/"
export const GET_USER_ACTIVE = (id) => "/v1/admin/users/" + id + "/active/"
export const GET_USER_PAYMENT = (id) => "v1/admin/users/" + id + "/payment/"
export const POST_USER_PAYMENT = (id) => "v1/admin/users/" + id + "/payment/deposit/"
export const POST_USER_UPDATE_STATUS_PAYMENT = (id) => "v1/admin/payments/manual/deposit/" + id + "/"
export const POST_USER_ESTATE = (id) => "v1/admin/users/" + id + "/estates/"
export const POST_USER_History = (id) => "v1/admin/users/" + id + "/trade/history/"
export const POST_USER_History_PAYMENT = (id) => "v1/admin/users/" + id + "/payment/history/"
export const POST_USER_History_ENTER = (id) => "v1/admin/users/"+id+"/login/activity/"
export const DASHBOARD_REPORTS =  "v1/admin/dashboard/stats/"
export const POST_USER_ORDER = (id) => "v1/admin/users/" + id + "/orders/"
export const GET_USER_PROFILE = "/user"
export const GET_COUNT_CONTACTS = "v1/admin/users/count/"
export const DOWNLOAD_FILE_EXCEL = "/v1/admin/users/download_excel/"
export const DOWNLOAD_CUSTOMER_FILE_EXCEL = "/v1/admin/users/customers/download/"

//Message
export const GET_NEW_MESSAGE = "v1/admin/messages/"
export const SEND_MESSAGE = "v1/admin/messages/manual/"
export const AUTO_SEND_MESSAGE = "v1/admin/messages/setmsg/"
export const GET_USERS_SEARCH_SEND_MESSAGE = "/v1/admin/users/search/"
export const GET_STATES_SEARCH_SEND_MESSAGE = "/v1/admin/estate/search/"
export const GET_UNIT_SENDERS = "/v1/admin/messages/sender_unit/"

//Message
export const LIST_ADMIN = "/v1/admin/users/"
export const GET_PERMISSION = "/v1/admin/permissions/"
export const ADD_PERMISSION = id => "/v1/admin/permissions/" + id + "/"
export const NEW_ADMIN = id => "/v1/admin/users/" + id + "/"

//Permisions
export const GET_USER_PERMISSIONS = "/v1/admin/permissions/me/"
export const GET_REPORTS_ACTIVITY =  `/v1/admin/trackers/`;

//Reports
export const GET_REPORT_CONTACTS = `/v1/admin/report/users/`
export const DOWNLOAD_REPORT_CONTACTS = `/v1/admin/report/users/export/`

export const GET_REPORT_LOG_CONTACTS = `/v1/admin/report/activity_logs/`
export const DOWNLOAD_REPORT_LOG_CONTACTS = `/v1/admin/report/activity_logs/export/`
export const GET_USERS_LOG_CONTACTS = `/v1/admin/users/search/`

export const GET_REPORT_ASSETS_CUSTOMERS = `/v1/admin/report/wealths/`
export const DOWNLOAD_REPORT_ASSETS_CUSTOMERS = `/v1/admin/report/wealths/export/`
export const GET_USERS_ASSETS_CUSTOMERS = `/v1/admin/users/search/`


export const GET_REPORT_WALLET = `/v1/admin/report/account_turnover/`
export const DOWNLOAD_REPORT_WALLET = `/v1/admin/report/account_turnover/export/`

export const GET_REPORT_TRADE_CUSTOMERS = `/v1/admin/report/customer_trades/`
export const DOWNLOAD_REPORT_TRADE_CUSTOMERS = `/v1/admin/report/customer_trades/export/`

export const GET_REPORT_DETAIlED_TRANSACTION = `/v1/admin/report/comprehensive_trade/`
export const DOWNLOAD_REPORT_DETAIlED_TRANSACTION = `/v1/admin/report/comprehensive_trade/export/`

