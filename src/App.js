import PropTypes from "prop-types"
import React, { useState } from "react"

import { Switch, Router, HashRouter, Route, BrowserRouter } from "react-router-dom"
import { connect } from "react-redux"

// Import Routes all
import { userRoutes, authRoutes } from "./routes/allRoutes"

// Import all middleware
import Authmiddleware from "./routes/middleware/Authmiddleware"

// layouts Format
import VerticalLayout from "./components/VerticalLayout/"
import HorizontalLayout from "./components/HorizontalLayout/"
import NonAuthLayout from "./components/NonAuthLayout"
import Login from "pages/Authentication/Login"
// Import scss
import "./assets/scss/theme.scss"

// Import Firebase Configuration file
import { initFirebaseBackend } from "./helpers/firebase_helper"
// Temprary Route 

import fakeBackend from "./helpers/AuthType/fakeBackend"
import { createHashHistory } from "history"
// Activating fake backend
fakeBackend()

const firebaseConfig = {
  apiKey: process.env.REACT_APP_APIKEY,
  authDomain: process.env.REACT_APP_AUTHDOMAIN,
  databaseURL: process.env.REACT_APP_DATABASEURL,
  projectId: process.env.REACT_APP_PROJECTID,
  storageBucket: process.env.REACT_APP_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGINGSENDERID,
  appId: process.env.REACT_APP_APPID,
  measurementId: process.env.REACT_APP_MEASUREMENTID
}

// init firebase backend
initFirebaseBackend(firebaseConfig)

const App = props => {
  // let authUser = localStorage.getItem("authUser")
  // const [authUser, setAuthUser] = useState(localStorage.getItem("authUser"))

  function getLayout() {
    let layoutCls = VerticalLayout

    switch (props.layout.layoutType) {
      case "horizontal":
        layoutCls = HorizontalLayout
        break
      default:
        layoutCls = VerticalLayout
        break
    }
    return layoutCls
  }
  const Layout = getLayout()
  return (
    <React.Fragment>
      <BrowserRouter>


        <Switch>

          {
            (props.login !== "1") && <Route exact component={Login} />
          }
          {

            authRoutes.map((route, idx) => (
              <Authmiddleware
                path={route.path}
                layout={NonAuthLayout}
                component={route.component}
                key={idx}
              />
            ))}

          {

            userRoutes.map((route, idx) => {
              let T = route.component
              return <Route path={route.path} exact
                            key={idx}
                            render={props =>
                              <Layout {...props}>
                                <T {...props} />
                              </Layout>}
              />
            })}
        </Switch>

      </BrowserRouter>
    </React.Fragment>
  )
}

App.propTypes = {
  layout: PropTypes.any
}

const mapStateToProps = state => {
  return {
    layout: state.Layout,
    login: state.Login?.success
  }
}

export default connect(mapStateToProps, null)(App)
