import { media, media_upload } from "utils/constant"
import { fetcher } from "store/common/action"

export const uploadFile = async (files, data) => {
  try {

    let upload = await fetcher(media_upload, {
      method: "POST",
      body: JSON.stringify({
        "file_name": files[0].name,
        "content_type": files[0].type
      })
    })
    let uploadUrl = await fetch(upload?.result?.upload_url,
      {
        method: "PUT",
        body: files[0]
      })
    let addMedia = await fetcher(media, {
      method: "POST",
      body: JSON.stringify({
        "title": data?.title,
        "media_type": data?.media_type,
        "bucket": upload?.result?.bucket,
        "object_name": upload?.result?.object_name
      })
    })
    return addMedia;
  } catch {

  }

}


export const uploadAuthFile = async (files, data) => {

  console.log("DATA :>> " , data);
  try {

    let upload = await fetcher(media_upload, {
      method: "POST",
      body: JSON.stringify({
        "file_name": files[0].name,
        "content_type": files[0].type
      })
    })

    console.log("Upload ::>>> ",upload);
    let uploadUrl = await fetch(upload?.result?.upload_url,
      {
        method: "PUT",
        body: files[0]
      })
      // console.log("Media_type :::>>> " , data);
      // console.log("Bucket :::>>> " , upload?.result?.bucket);
      // console.log("Object_name :::>>> " , upload?.result?.object_name);


    let addMedia = await fetcher(media, {

      method: "POST",
      body: JSON.stringify({
        // "content_type": "image/png",
        // "media_type": data?.media_type,
        "media_type": "media",
        "bucket": upload?.result?.bucket,
        "object_name": upload?.result?.object_name
      })
    })
    return addMedia;
  } catch {

  }

}