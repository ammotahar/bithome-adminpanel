import { call, put, takeEvery } from "redux-saga/effects"

// Crypto Redux States
import { GET_PROJECTS, GET_PROJECT_DETAIL, CREATE_PROJECT ,GET_PROJECTS_TYPE} from "./actionTypes"
import {
  getProjectsSuccess,
  getProjectsFail,
  getProjectDetailSuccess,
  getProjectDetailFail,
  successCreateProject,
  failedCreateProject,
  getProjectsTypeSuccess
} from "./actions"

//Include Both Helper File with needed methods
import { getProjects, getProjectsDetails, createProject ,getProjectsType} from "helpers/fakebackend_helper"

export function* fetchProjects({data}) {
  try {
    const response = yield call(getProjects,data)
     if(response?.code===200)
    yield put(getProjectsSuccess(response))

  } catch (error) {
    yield put(getProjectsFail(error))
  }
}
export function* fetchProjectsType() {
  try {
    const response = yield call(getProjectsType)
    yield put(getProjectsTypeSuccess(response))

  } catch (error) {
    // yield put(getProjectsFail(error))
  }
}

export function* fetchCreateProjects(data) {
  try {
    const response = yield call(createProject)

    yield put(successCreateProject(response))
  } catch (error) {
    yield put(failedCreateProject(error))
  }
}

export function* fetchProjectDetail({ projectId }) {
  try {
    const response = yield call(getProjectsDetails, projectId)
    yield put(getProjectDetailSuccess(response))
  } catch (error) {
    yield put(getProjectDetailFail(error))
  }
}

function* projectsSaga() {
  yield takeEvery(GET_PROJECTS, fetchProjects)
  yield takeEvery(CREATE_PROJECT, fetchCreateProjects)
  yield takeEvery(GET_PROJECT_DETAIL, fetchProjectDetail)
  yield takeEvery(GET_PROJECTS_TYPE, fetchProjectsType)
}

export default projectsSaga
