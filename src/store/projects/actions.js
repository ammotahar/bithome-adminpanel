import {
  GET_PROJECTS,
  GET_PROJECTS_FAIL,
  GET_PROJECTS_SUCCESS,
  GET_PROJECT_DETAIL,
  GET_PROJECT_DETAIL_FAIL,
  GET_PROJECT_DETAIL_SUCCESS,
  CREATE_PROJECT,
  SUCCESS_CREATE_PROJECT,
  FAILED_CREATE_PROJECT,
  GET_PROJECTS_TYPE,
  GET_PROJECTS_TYPE_SUCCESS,
} from "./actionTypes"
import { fetcher } from "store/common/action"
import { projects } from "utils/constant"
export const getProjects = (data) => ({
  type: GET_PROJECTS,
  data
})
export const getProjectsType = () => ({
  type: GET_PROJECTS_TYPE,
})
export const getProjectsTypeSuccess = (type) => ({
  type: GET_PROJECTS_TYPE_SUCCESS,
  payload: type
})
// import { call, put } from "redux-saga/effects"
import { all, call, put, takeEvery } from "redux-saga/effects"

export const getProjectsSuccess = projects => ({
  type: GET_PROJECTS_SUCCESS,
  payload: projects
})

export const getProjectsFail = error => ({
  type: GET_PROJECTS_FAIL,
  payload: error
})

export const getProjectDetail = projectId => ({
  type: GET_PROJECT_DETAIL,
  projectId
})

export const getProjectDetailSuccess = projectDetails => ({
  type: GET_PROJECT_DETAIL_SUCCESS,
  payload: projectDetails
})

export const getProjectDetailFail = error => ({
  type: GET_PROJECT_DETAIL_FAIL,
  payload: error
})
export const createProject = (data) => ({
  type: CREATE_PROJECT,
  data
});
export const successCreateProject = () => ({
  type: SUCCESS_CREATE_PROJECT,
});
export const failedCreateProject = () => ({
  type: FAILED_CREATE_PROJECT,
});
// export const getProjects = async () =>  {
//   await fetcher((projects),
//     {
//       method: "GET"
//     }, { query: { page: "1" } })
//     .then((response) => {
//       return response
//     })
//     .catch(r => {
//     })
// }
