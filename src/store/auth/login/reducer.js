import { removeToken } from "utils/utils"
import {
  LOGIN_USER,
  LOGIN_SUCCESS,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  API_ERROR,
  FIRST_LOGIN_SUCCESS,
  REMOVE_FIRST_LOGIN_SUCCESS,

} from "./actionTypes"

const initialState = {
  error: "",
  loading: false,
  firstLogin:{
    error: "",
    loading: false,
    success:false,
  },
  success: localStorage.getItem("authUser")
}

const login = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER:
      state = {
        ...state,
      }
      break
    case LOGIN_SUCCESS:
      state = {
        ...state,
         ...action.payload,
      }
      break
    case FIRST_LOGIN_SUCCESS:
      state = {
        ...state,
        firstLogin:{
          ...state.firstLogin,
          ...action.payload,

        },
      }
      break
    case REMOVE_FIRST_LOGIN_SUCCESS:
      state = {
        ...state,
        firstLogin:{
          error: "",
          loading: false,
          success:false,
        },
      }
      break
    case LOGOUT_USER:
      removeToken()
      state = { 
        ...state, 
        loading: false,
       }
      break
    case LOGOUT_USER_SUCCESS:
      removeToken()
      state = { 
        ...state,
        loading: false,
       }
      break
    case API_ERROR:
      state = { ...state, error: action.payload, loading: false }
      break
    default:
      state = { ...state }
      break
  }
  return state
}

export default login
