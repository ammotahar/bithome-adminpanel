import {
  LOGIN_USER,
  LOGIN_SUCCESS,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  API_ERROR,
  SOCIAL_LOGIN,
  REFRESH_TOKEN,
  LOGIN_OTP_USER,
  FIRST_LOGIN_SUCCESS,
  REMOVE_FIRST_LOGIN_SUCCESS,
  // OTP_LOGIN_USER,
} from "./actionTypes"

export const loginUser = (user, history) => {

  return {
    type: LOGIN_USER,
    payload: { user, history },
  }
}
// export const otpLoginUser = (user, history) => {
//
//   return {
//     type: OTP_LOGIN_USER,
//     payload: { user, history },
//   }
// }
export const postLoginOtpUser = (user, history) => {

  return {
    type: LOGIN_OTP_USER,
    payload: { user, history },
  }
}
export const refreshOtp = () => {
  return {
    type: REFRESH_TOKEN,
  }
}
export const loginSuccess = user => {
  // console.log("----------")
  return {
    type: LOGIN_SUCCESS,
    payload: user,
  }
}
export const firstLoginSuccess = user => {
  return {
    type: FIRST_LOGIN_SUCCESS,
    payload: user,
  }
}
export const removeFirstLoginSuccess = () => {
  return {
    type: REMOVE_FIRST_LOGIN_SUCCESS,
  }
}


export const logoutUser = history => {
  return {
    type: LOGOUT_USER,
    payload: { history },
  }
}

export const logoutUserSuccess = () => {
  return {
    type: LOGOUT_USER_SUCCESS,
    payload: {},
  }
}

export const apiError = error => {
  return {
    type: API_ERROR,
    payload: error,
  }
}

export const socialLogin = (data, history, type) => {
  return {
    type: SOCIAL_LOGIN,
    payload: { data, history, type },
  }
}
