import { call, put, takeEvery, takeLatest } from "redux-saga/effects"
import { Refresh, removeToken, setToken } from "utils/utils"
// Login Redux States
import { LOGIN_USER, LOGOUT_USER, LOGIN_OTP_USER, REFRESH_TOKEN, SOCIAL_LOGIN } from "./actionTypes"
import { apiError, loginSuccess, logoutUserSuccess, firstLoginSuccess } from "./actions"
import { MessageToast } from "utils/message"
//Include Both Helper File with needed methods
import { getFirebaseBackend } from "../../../helpers/firebase_helper"
import {
  postFakeLogin,
  postJwtLogin,
  postSocialLogin,
  postRefreshToken,
  postFakeOtpLogin
} from "../../../helpers/fakebackend_helper"

const fireBaseBackend = getFirebaseBackend()

function* loginUser({ payload: { user, history } }) {
  yield put(firstLoginSuccess({ loading: true }))
  try {
    const response = yield call(postFakeLogin, {
      input: user.input,
      password: user.password
    }, history)
    // setToken(response?.result?.access_token,response?.result?.refresh_token)
    // if (response?.result?.national_code) {
    //   const responseOtp = yield call(postFakeOtpLogin, {
    //     input: user.input,
    //     password: user.password
    //   }, history)
    // }
    yield put(firstLoginSuccess({ loading: false }))
    if (response?.result?.national_code) {
      yield put(firstLoginSuccess({
        success: response?.result, loading: false
      }))
      // localStorage.setItem("authUser", "1")
      // history.push("/dashboard")
    } else {
      if (response?.result === "User Not Found")
        MessageToast("error", "کاربر وجود ندارد")

      if (response?.result?.detail === "Inactive user")
        MessageToast("error", "کاربر غیرفعال است")

    }

  } catch (error) {
    yield put(apiError(error))
    yield put(firstLoginSuccess({ loading: false }))
  }
}

function* loginOtpUser({ payload: { user, history } }) {
  yield put(loginSuccess({ loading: true }))
  try {
    const response = yield call(postFakeOtpLogin, {
      input: user.input,
      password: user.password,
      otp: user.otp
    }, history)
    setToken(response?.result?.access_token, response?.result?.refresh_token)
    yield put(loginSuccess({ loading: false }))
    if (response?.result?.access_token) {
      yield put(loginSuccess({ success: "1",loading: false  }))
      localStorage.setItem("authUser", "1")
      history.push("/dashboard")
    } else {

      if (response?.result === "Otp is wrong or expired")
        MessageToast("error", "کد وارد شده صحیح نیست")
      if (response?.result === "User Not Found")
        MessageToast("error", "کاربر وجود ندارد")

      if (response?.result?.detail === "Inactive user")
        MessageToast("error", "کاربر غیرفعال است")

    }

  } catch (error) {
    yield put(apiError(error))
  }
}

function* refreshOtp() {

  try {
    const refresh = Refresh()
    // alert(refresh)
    const response = yield call(postRefreshToken, refresh)
    setToken(response?.result?.access_token, response?.result?.refresh_token)

    if (response?.result?.access_token) {
      yield put(loginSuccess("1"))
      localStorage.setItem("authUser", "1")
    } else {
      yield put(loginSuccess("0"))
      localStorage.setItem("authUser", "0")
      window.location = "/login"
    }

  } catch (error) {
    yield put(apiError(error))
  }
}

function* logoutUser({ payload: { history } }) {
  try {
    localStorage.removeItem("authUser")
    removeToken()
    if (process.env.REACT_APP_DEFAULTAUTH === "firebase") {
      const response = yield call(fireBaseBackend.logout)
      yield put(logoutUserSuccess(response))
    }
    history.push("/login")
  } catch (error) {
    yield put(apiError(error))
  }
}

function* socialLogin({ payload: { data, history, type } }) {
  try {
    if (process.env.REACT_APP_DEFAULTAUTH === "firebase") {
      const fireBaseBackend = getFirebaseBackend()
      const response = yield call(
        fireBaseBackend.socialLoginUser,
        data,
        type
      )
      // localStorage.setItem("authUser", JSON.stringify(response))
      yield put(loginSuccess(response))
    } else {
      const response = yield call(postSocialLogin, data)
      localStorage.setItem("authUser", JSON.stringify(response))
      yield put(loginSuccess(response))
    }
    history.push("/dashboard")
  } catch (error) {
    yield put(apiError(error))
  }
}

function* authSaga() {
  yield takeEvery(LOGIN_USER, loginUser)
  yield takeEvery(LOGIN_OTP_USER, loginOtpUser)
  yield takeLatest(SOCIAL_LOGIN, socialLogin)
  yield takeEvery(LOGOUT_USER, logoutUser)
  yield takeEvery(REFRESH_TOKEN, refreshOtp)
}

export default authSaga
