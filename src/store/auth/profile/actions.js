import { PROFILE_ERROR, PROFILE_SUCCESS, EDIT_PROFILE,GET_PROFILE ,GET_MONEY,GET_MONEY_SUCCESS} from "./actionTypes"

export const getProfile = () => {
  return {
    type: GET_PROFILE,
  }
}
export const getMoney = (id) => {
  return {
    type: GET_MONEY,
    id
  }
}
export const getMoneySuccess = (r) => {
  return {
    type: GET_MONEY_SUCCESS,
    payload: r,
  }
}
export const editProfile = user => {
  return {
    type: EDIT_PROFILE,
    payload: { user },
  }
}

export const profileSuccess = msg => {
  return {
    type: PROFILE_SUCCESS,
    payload: msg,
  }
}

export const profileError = error => {
  return {
    type: PROFILE_ERROR,
    payload: error,
  }
}
