import { call, put, takeEvery } from "redux-saga/effects"

// Crypto Redux States
import { GET_INVOICES, GET_INVOICE_DETAIL,GET_INVOICES_DEPOSIT,GET_INVOICES_WITHDRAW } from "./actionTypes"
import {
  getInvoicesSuccess,
  getInvoicesFail,
  getInvoiceDetailSuccess,
  getInvoiceDetailFail,
  getInvoicesSuccessDeposit,
  getInvoicesSuccessWithdraw,
  getInvoicesFailDeposit,
  getInvoicesFailWithdraw
} from "./actions"

//Include Both Helper File with needed methods
import { getInvoices, getInvoiceDetail,getInvoicesDeposit,getInvoicesWithdraw } from "helpers/fakebackend_helper"

function* fetchInvoices({ query }) {

  try {

    const response = yield call(getInvoices,query || "")
       if(response?.code===200)
    yield put(getInvoicesSuccess(response))
  } catch (error) {
    yield put(getInvoicesFail(error))
  }
}
function* fetchInvoicesDeposit({ query }) {
  try {
    const response = yield call(getInvoicesDeposit,query || "")
     if(response?.code===200)
    yield put(getInvoicesSuccessDeposit(response))
  } catch (error) {
    yield put(getInvoicesFailDeposit(error))
  }
}
function* fetchInvoicesWithdraw({ query }) {
  try {
    const response = yield call(getInvoicesWithdraw,query || "")
     if(response?.code===200)
    yield put(getInvoicesSuccessWithdraw(response))
  } catch (error) {
    yield put(getInvoicesFailWithdraw(error))
  }
}

function* fetchInvoiceDetail({ invoiceId }) {
  try {
    const response = yield call(getInvoiceDetail, invoiceId)
    yield put(getInvoiceDetailSuccess(response))
  } catch (error) {
    yield put(getInvoiceDetailFail(error))
  }
}

function* invoiceSaga() {
  yield takeEvery(GET_INVOICES, fetchInvoices)
  yield takeEvery(GET_INVOICES_DEPOSIT, fetchInvoicesDeposit)
  yield takeEvery(GET_INVOICES_WITHDRAW, fetchInvoicesWithdraw)
  yield takeEvery(GET_INVOICE_DETAIL, fetchInvoiceDetail)

}

export default invoiceSaga
