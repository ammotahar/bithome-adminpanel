import {
  GET_INVOICES,
  GET_INVOICES_FAIL,
  GET_INVOICES_SUCCESS,
  GET_INVOICE_DETAIL,
  GET_INVOICE_DETAIL_FAIL,
  GET_INVOICE_DETAIL_SUCCESS,
  GET_INVOICES_DEPOSIT ,
GET_INVOICES_SUCCESS_DEPOSIT,
GET_INVOICES_FAIL_DEPOSIT ,
  GET_INVOICES_WITHDRAW ,
GET_INVOICES_SUCCESS_WITHDRAW,
GET_INVOICES_FAIL_WITHDRAW,
} from "./actionTypes"

export const getInvoices = (query) => ({
  type: GET_INVOICES,
  query
})
export const getInvoicesSuccess = invoices => ({
  type: GET_INVOICES_SUCCESS,
  payload: invoices,
})
export const getInvoicesFail = error => ({
  type: GET_INVOICES_FAIL,
  payload: error,
})

export const getInvoicesDeposit = (query) => ({
  type: GET_INVOICES_DEPOSIT,
  query
})
export const getInvoicesSuccessDeposit = invoices => ({
  type: GET_INVOICES_SUCCESS_DEPOSIT,
  payload: invoices,
})
export const getInvoicesFailDeposit = error => ({
  type: GET_INVOICES_FAIL_DEPOSIT,
  payload: error,
})


export const getInvoicesWithdraw = (query) => ({
  type: GET_INVOICES_WITHDRAW,
  query
})
export const getInvoicesSuccessWithdraw = invoices => ({
  type: GET_INVOICES_SUCCESS_WITHDRAW,
  payload: invoices,
})
export const getInvoicesFailWithdraw = error => ({
  type: GET_INVOICES_FAIL_WITHDRAW,
  payload: error,
})


export const getInvoiceDetail = invoiceId => ({
  type: GET_INVOICE_DETAIL,
  invoiceId,
})

export const getInvoiceDetailSuccess = invoices => ({
  type: GET_INVOICE_DETAIL_SUCCESS,
  payload: invoices,
})

export const getInvoiceDetailFail = error => ({
  type: GET_INVOICE_DETAIL_FAIL,
  payload: error,
})
