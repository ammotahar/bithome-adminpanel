import {
  GET_INVOICES_FAIL,
  GET_INVOICES_SUCCESS,
  GET_INVOICE_DETAIL_SUCCESS,
  GET_INVOICE_DETAIL_FAIL,
  GET_INVOICES_DEPOSIT,
  GET_INVOICES_SUCCESS_DEPOSIT,
  GET_INVOICES_FAIL_DEPOSIT,
  GET_INVOICES_WITHDRAW,
  GET_INVOICES_SUCCESS_WITHDRAW,
  GET_INVOICES_FAIL_WITHDRAW
} from "./actionTypes"

const INIT_STATE = {
  invoices: [],
  invoiceDetail: {},
  error: {},
  invoicesDeposit:[],
  invoicesWithdraw:[],
}

const Invoices = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_INVOICES_SUCCESS:
      return {
        ...state,
        invoices: action.payload
      }
    case GET_INVOICES_SUCCESS_DEPOSIT:
      return {
        ...state,
        invoicesDeposit: action.payload
      }
    case GET_INVOICES_SUCCESS_WITHDRAW:
      return {
        ...state,
        invoicesWithdraw: action.payload
      }
    case GET_INVOICES_FAIL:
      return {
        ...state,
        error: action.payload
      }
    case GET_INVOICES_FAIL_DEPOSIT:
      return {
        ...state,
        errorDeposit: action.payload
      }
    case GET_INVOICES_FAIL_WITHDRAW:
      return {
        ...state,
        errorWithdraw: action.payload
      }
    case GET_INVOICE_DETAIL_SUCCESS:
      return {
        ...state,
        invoiceDetail: action.payload
      }

    case GET_INVOICE_DETAIL_FAIL:
      return {
        ...state,
        error: action.payload
      }

    default:
      return state
  }
}

export default Invoices
