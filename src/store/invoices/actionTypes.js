/* INVOICES */
export const GET_INVOICES = "GET_INVOICES"
export const GET_INVOICES_SUCCESS = "GET_INVOICES_SUCCESS"
export const GET_INVOICES_FAIL = "GET_INVOICES_FAIL"

export const GET_INVOICE_DETAIL = "GET_INVOICE_DETAIL"
export const GET_INVOICE_DETAIL_SUCCESS = "GET_INVOICE_DETAIL_SUCCESS"
export const GET_INVOICE_DETAIL_FAIL = "GET_INVOICE_DETAIL_FAIL"


export const GET_INVOICES_DEPOSIT = "GET_INVOICES_DEPOSIT"
export const GET_INVOICES_SUCCESS_DEPOSIT = "GET_INVOICES_SUCCESS_DEPOSIT"
export const GET_INVOICES_FAIL_DEPOSIT = "GET_INVOICES_FAIL_DEPOSIT"

export const GET_INVOICES_WITHDRAW = "GET_INVOICES_WITHDRAW"
export const GET_INVOICES_SUCCESS_WITHDRAW = "GET_INVOICES_SUCCESS_WITHDRAW"
export const GET_INVOICES_FAIL_WITHDRAW = "GET_INVOICES_FAIL_WITHDRAW"