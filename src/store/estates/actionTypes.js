/* States */
export const GET_States = "GET_States"
export const CREATE_States = "CREATE_States"
export const GET_States_SUCCESS = "GET_States_SUCCESS"
export const GET_States_FAIL = "GET_States_FAIL"

/* States DETAIL*/
export const GET_State_DETAIL = "GET_State_DETAIL"
export const GET_State_DETAIL_SUCCESS = "GET_State_DETAIL_SUCCESS"
export const GET_State_DETAIL_FAIL = "GET_State_DETAIL_FAIL"

/* States create*/
export const CREATE_State = "CREATE_State"
export const SUCCESS_CREATE_State = "SUCCESS_CREATE_State"
export const FAILED_CREATE_State = "FAILED_CREATE_State"


//extra
export const GET_STATES_TYPE = "GET_STATES_TYPE"
export const GET_USERS_ESTATES = "GET_USERS_ESTATES"
export const GET_STATES_TYPE_SUCCESS = "GET_STATES_TYPE_SUCCESS"
export const GET_STATES_TYPE_SUCCESS_USERS = "GET_STATES_TYPE_SUCCESS_USERS"
