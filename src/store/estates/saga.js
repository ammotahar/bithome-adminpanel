import { call, put, takeEvery } from "redux-saga/effects"

// Crypto Redux States
import { GET_States, GET_State_DETAIL, CREATE_State, GET_STATES_TYPE, CREATE_States, GET_USERS_ESTATES} from "./actionTypes"
import {
  getStatesSuccess,
  getStatesFail,
  getStateDetailSuccess,
  getStateDetailFail,
  successCreateState,
  failedCreateState,
  getEStatesSuccess,
  getEStatesSuccessUsers
} from "./actions"

//Include Both Helper File with needed methods
import { getStates, getStatesDetails, getEstatesType,getEstatesUsers } from "helpers/fakebackend_helper"

export function* fetchStates({data}) {
  try {
    // console.log('props')
    const response = yield call(getStates,data)
     if(response?.code===200)
      yield put(getStatesSuccess(response))

  } catch (error) {
    yield put(getStatesFail(error))
  }
}

export function* fetchEstatesType() {
  try {
    // console.log('props')
    const response = yield call(getEstatesType)
    yield put(getEStatesSuccess(response))

  } catch (error) {
    // yield put(getStatesFail(error))
  }
}
export function* users() {
  try {

    const response = yield call(getEstatesUsers)
    yield put(getEStatesSuccessUsers(response))

  } catch (error) {
    // yield put(getStatesFail(error))
  }
}

// export function* fetchCreateStates({ data }) {
//   try {
//     console.log(data)
//     const response = yield call(createStates, data)
//     yield put(successCreateState(response))
//   } catch (error) {
//     yield put(failedCreateState(error))
//   }
// }

export function* fetchStateDetail({ StateId }) {
  try {
    const response = yield call(getStatesDetails, StateId)
    yield put(getStateDetailSuccess(response))
  } catch (error) {
    yield put(getStateDetailFail(error))
  }
}

function* StatesSaga() {
  yield takeEvery(GET_States, fetchStates)
  // yield takeEvery(CREATE_State, fetchCreateStates)
  yield takeEvery(GET_State_DETAIL, fetchStateDetail)
  yield takeEvery(GET_USERS_ESTATES, users)
  yield takeEvery(GET_STATES_TYPE, fetchEstatesType)
}

export default StatesSaga
