import {
  GET_States_FAIL,
  GET_States_SUCCESS,
  GET_State_DETAIL_FAIL,
  GET_State_DETAIL_SUCCESS,
  CREATE_State,
  SUCCESS_CREATE_State,
  FAILED_CREATE_State,
  GET_STATES_TYPE_SUCCESS, GET_STATES_TYPE_SUCCESS_USERS
} from "./actionTypes"

const INIT_STATE = {
  States: [],

  error: {},
  estates_type: [],
  create: {},
  StateDetail:[],
  estates_users:[]
}

const States = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_States_SUCCESS:
      return {
        ...state,
        states: action.payload
      }

    case GET_States_FAIL:
      return {
        ...state,
        error: action.payload
      }

    case GET_State_DETAIL_SUCCESS:
      return {
        ...state,
        StateDetail: action.payload
      }

    case GET_State_DETAIL_FAIL:
      return {
        ...state,
        error: action.payload
      }

    case SUCCESS_CREATE_State:
      return {
        ...state,
        create: action.payload
      }
    case FAILED_CREATE_State:
      return {
        ...state,
        create_error: action.payload
      }
    case GET_STATES_TYPE_SUCCESS:
      return {
        ...state,
        estates_type: action.payload
      }
    case GET_STATES_TYPE_SUCCESS_USERS:
      return {
        ...state,
        estates_users: action.payload
      }
    default:
      return state
  }
}

export default States
