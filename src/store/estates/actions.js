import {
  GET_States,
  GET_States_FAIL,
  GET_States_SUCCESS,
  GET_State_DETAIL,
  GET_State_DETAIL_FAIL,
  GET_State_DETAIL_SUCCESS,
  CREATE_State,
  SUCCESS_CREATE_State,
  FAILED_CREATE_State,
  GET_STATES_TYPE,
  GET_STATES_TYPE_SUCCESS,
  CREATE_States,
  GET_USERS_ESTATES,
  GET_STATES_TYPE_SUCCESS_USERS
} from "./actionTypes"
// import { fetcher } from "store/common/action"
// import { States } from "utils/constant"
export const getStates = (data) => ({
  type: GET_States,
  data
})

// import { call, put } from "redux-saga/effects"
import { all, call, put, takeEvery } from "redux-saga/effects"

export const getEstatesType = () => ({
  type: GET_STATES_TYPE
})
export const getEstatesUsers = () => ({
  type: GET_USERS_ESTATES
})
export const getEStatesSuccessUsers = States => ({
  type: GET_STATES_TYPE_SUCCESS_USERS,
  payload: States
})
export const getEStatesSuccess = States => ({
  type: GET_STATES_TYPE_SUCCESS,
  payload: States
})
export const getStatesSuccess = States => ({
  type: GET_States_SUCCESS,
  payload: States
})

export const getStatesFail = error => ({
  type: GET_States_FAIL,
  payload: error
})

export const getStateDetail = StateId => ({
  type: GET_State_DETAIL,
  StateId
})

export const getStateDetailSuccess = StateDetails => ({
  type: GET_State_DETAIL_SUCCESS,
  payload: StateDetails
})

export const getStateDetailFail = error => ({
  type: GET_State_DETAIL_FAIL,
  payload: error
})
export const createEstate = (data) => ({
  type: CREATE_State,
  data
})
export const successCreateState = (create) => ({
  type: SUCCESS_CREATE_State,
  payload: create
})
export const failedCreateState = (create) => ({
  type: FAILED_CREATE_State,
  payload: create
})
// export const getStates = async () =>  {
//   await fetcher((States),
//     {
//       method: "GET"
//     }, { query: { page: "1" } })
//     .then((response) => {
//       return response
//     })
//     .catch(r => {
//     })
// }
