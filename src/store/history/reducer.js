import {
  GET_HISTORY_ORDER,
  GET_HISTORY_SUCCESS_ORDER,
  GET_HISTORY_FAIL_ORDER,
  GET_HISTORY_TRADE,
  GET_HISTORY_SUCCESS_TRADE,
  GET_HISTORY_FAIL_TRADE
} from "./actionTypes"

const INIT_STATE = {

  // invoiceDetail: {},
  error: {},
  historyTrade:[],
  historyOrder:[],
}

const Invoices = (state = INIT_STATE, action) => {
  switch (action.type) {

    case GET_HISTORY_SUCCESS_TRADE:
      return {
        ...state,
        historyTrade: action.payload
      }
    case GET_HISTORY_SUCCESS_ORDER:
      return {
        ...state,
        historyOrder: action.payload
      }
    case GET_HISTORY_FAIL_ORDER:
      return {
        ...state,
        errorOrder: action.payload
      }
    case GET_HISTORY_FAIL_TRADE:
      return {
        ...state,
        errorTrade: action.payload
      }
    // case GET_INVOICE_DETAIL_SUCCESS:
    //   return {
    //     ...state,
    //     invoiceDetail: action.payload
    //   }
    //
    // case GET_INVOICE_DETAIL_FAIL:
    //   return {
    //     ...state,
    //     error: action.payload
    //   }

    default:
      return state
  }
}

export default Invoices
