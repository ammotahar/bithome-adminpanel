import {
  GET_HISTORY_ORDER,
  GET_HISTORY_SUCCESS_ORDER,
  GET_HISTORY_FAIL_ORDER,
  GET_HISTORY_TRADE,
  GET_HISTORY_SUCCESS_TRADE,
  GET_HISTORY_FAIL_TRADE
} from "./actionTypes"


export const getHistoryOrder = (data) => ({
  type: GET_HISTORY_ORDER,
  data
})
export const getSuccessHistoryOrder = HISTORY => ({
  type: GET_HISTORY_SUCCESS_ORDER,
  payload: HISTORY,
})
export const getFailHistoryOrder = error => ({
  type: GET_HISTORY_FAIL_ORDER,
  payload: error,
})


export const getHistoryTrade= (query) => ({
  type: GET_HISTORY_TRADE,
  query
})
export const getSuccessHistoryTrade = HISTORY => ({
  type: GET_HISTORY_SUCCESS_TRADE,
  payload: HISTORY,
})
export const getFailHistoryTrade  = error => ({
  type: GET_HISTORY_FAIL_TRADE,
  payload: error,
})


// export const getInvoiceDetail = invoiceId => ({
//   type: GET_INVOICE_DETAIL,
//   invoiceId,
// })
//
// export const getInvoiceDetailSuccess = HISTORY => ({
//   type: GET_INVOICE_DETAIL_SUCCESS,
//   payload: HISTORY,
// })
//
// export const getInvoiceDetailFail = error => ({
//   type: GET_INVOICE_DETAIL_FAIL,
//   payload: error,
// })
