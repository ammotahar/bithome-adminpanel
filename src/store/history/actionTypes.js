/* HISTORY */
// export const GET_HISTORY = "GET_HISTORY"
// export const GET_HISTORY_SUCCESS = "GET_HISTORY_SUCCESS"
// export const GET_HISTORY_FAIL = "GET_HISTORY_FAIL"

// export const GET_INVOICE_DETAIL = "GET_INVOICE_DETAIL"
// export const GET_INVOICE_DETAIL_SUCCESS = "GET_INVOICE_DETAIL_SUCCESS"
// export const GET_INVOICE_DETAIL_FAIL = "GET_INVOICE_DETAIL_FAIL"


export const GET_HISTORY_ORDER = "GET_HISTORY_ORDER"
export const GET_HISTORY_SUCCESS_ORDER = "GET_HISTORY_SUCCESS_ORDER"
export const GET_HISTORY_FAIL_ORDER = "GET_HISTORY_FAIL_ORDER"

export const GET_HISTORY_TRADE = "GET_HISTORY_TRADE"
export const GET_HISTORY_SUCCESS_TRADE = "GET_HISTORY_SUCCESS_TRADE"
export const GET_HISTORY_FAIL_TRADE = "GET_HISTORY_FAIL_TRADE"