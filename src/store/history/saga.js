import { call, put, takeEvery } from "redux-saga/effects"

// Crypto Redux States
import {
  GET_HISTORY_ORDER,
  GET_HISTORY_TRADE
} from "./actionTypes"
import {
  getSuccessHistoryOrder,
  getFailHistoryOrder,
  getSuccessHistoryTrade,
  getFailHistoryTrade
} from "./actions"

//Include Both Helper File with needed methods
import { getHistoryOrder, getHistoryTrade} from "helpers/fakebackend_helper";


function* fetchHistoryOrder(data) {
  try {
    const response = yield call(getHistoryOrder,data?.data)
     if(response?.code===200)
    yield put(getSuccessHistoryOrder(response))
  } catch (error) {
    yield put(getFailHistoryOrder(error))
  }
}

function* fetchHistoryTrade({ query }) {
  try {
    const response = yield call(getHistoryTrade,query)
     if(response?.code===200)
    yield put(getSuccessHistoryTrade(response))
  } catch (error) {
    yield put(getFailHistoryTrade(error))
  }
}
//
// function* fetchInvoiceDetail({ invoiceId }) {
//   try {
//     const response = yield call(getInvoiceDetail, invoiceId)
//     yield put(getInvoiceDetailSuccess(response))
//   } catch (error) {
//     yield put(getInvoiceDetailFail(error))
//   }
// }

function* history() {
  yield takeEvery(GET_HISTORY_ORDER, fetchHistoryOrder)
  yield takeEvery(GET_HISTORY_TRADE, fetchHistoryTrade)
}

export default history
