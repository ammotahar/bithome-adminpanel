import { createStore, applyMiddleware, compose } from "redux"
import createSagaMiddleware from "redux-saga"
import { middleware as thunkMiddleware } from 'redux-saga-thunk'
import rootReducer from "./reducers"
import rootSaga from "./sagas"
// import thunk from 'redux-thunk';
const sagaMiddleware = createSagaMiddleware()
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
// const middleware = [thunk];
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware),   ( window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (fn) => fn))
)
sagaMiddleware.run(rootSaga)

export default store;
