import { takeEvery, put, call } from "redux-saga/effects"

// Chat Redux States
import {
  CLOSE_TICKET,
  GET_CHATS,
  GET_CONTACTS,
  GET_GROUPS,
  GET_MESSAGES,
  POST_ADD_MESSAGE,
} from "./actionTypes"
import {
  getChatsSuccess,
  getChatsFail,
  getGroupsSuccess,
  getGroupsFail,
  getContactsSuccess,
  getContactsFail,
  getMessagesSuccess,
  getMessagesFail,
  addMessageSuccess,
  addMessageFail
} from "./actions"

//Include Both Helper File with needed methods
import {
  getChats,
  getGroups,
  getContacts,
  getMessages,
  addMessage,
  closeTicket
} from "../../helpers/fakebackend_helper"
import { MessageToast } from "../../utils/message"

function* onGetChats({data}) {
  try {
    const response = yield call(getChats,data)
    if(response?.code===200)
    yield put(getChatsSuccess(response))
  } catch (error) {
    yield put(getChatsFail(error))
  }
}

function* onGetGroups() {
  try {
    const response = yield call(getGroups)
    yield put(getGroupsSuccess(response))
  } catch (error) {
    yield put(getGroupsFail(error))
  }
}

function* onGetContacts() {
  try {
    const response = yield call(getContacts)
    yield put(getContactsSuccess(response))
  } catch (error) {
    yield put(getContactsFail(error))
  }
}

function* onGetMessages({ roomId }) {
  try {
    const response = yield call(getMessages, roomId)
    yield put(getMessagesSuccess(response))
  } catch (error) {
    yield put(getMessagesFail(error))
  }
}

function* onAddMessage({ data }) {
  try {
    console.log(data)
    const response = yield call(addMessage, data)
    if(response?.code===200)
    yield put(addMessageSuccess(response))
    if(response?.code===403)
    MessageToast("error", "شما قبلا پاسخ این تیکت را داده اید")
  } catch (error) {
    yield put(addMessageFail(error))


  }
}


function* onCloseTicket({ payload }) {
  try {
    console.log(payload)
    const response = yield call(closeTicket, payload)
    // if(response?.code===200)
    // yield put(closeTicket(response))
    // if(response?.code===403)
    // MessageToast("error", "امکان بستن تیکت وجود ندارد")
  } catch (error) {
    // yield put(addMessageFail(error))


  }
}

function* chatSaga() {
  yield takeEvery(GET_CHATS, onGetChats)
  yield takeEvery(GET_GROUPS, onGetGroups)
  yield takeEvery(GET_CONTACTS, onGetContacts)
  yield takeEvery(GET_MESSAGES, onGetMessages)
  yield takeEvery(POST_ADD_MESSAGE, onAddMessage)
  yield takeEvery(CLOSE_TICKET, onCloseTicket)
}

export default chatSaga
