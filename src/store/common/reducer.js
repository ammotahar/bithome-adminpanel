import {RESET, LOADING} from 'store/common/type';


 const infoLoading = (state = false, action) => {
   // console.log(action)
    switch (action.type) {
        case RESET:
            return false;
        case LOADING:
            return action?.data ;
        default:
            return state;
    }
};
export default infoLoading;