import { Url, Header, getRereshObject } from "utils/utils"
import { RESET, LOADING } from "store/common/type"
import { media, media_upload } from "utils/constant"
// import {refreshOtp} from 'redux/auth/action';
import store from "store"
// import {message} from "antd";
import { call, put } from "redux-saga/effects"
import { MessageToast } from "utils/message"
import { refreshOtp } from "../auth/login/actions"
let refresh = true
let timeAction = null

export function fetcher(url = Url("/url-not-filled", {}), header = {}, ext = { noAuth: true, toText: false }) {
  store.dispatch(loading(true))
  return fetch(Url(url, ext["query"]), Header(header, ext.noAuth))
    .then(async (response) => {

      if (!response.ok) {//4xx-5xx
        if ((response.status === 401) && refresh) {//401
          timeAction = setTimeout(() => {
            refresh = true
          }, 6000)
          store.dispatch(refreshOtp());
          refresh = false
        }
        if ((response.status === 403) && refresh) {//401
          timeAction = setTimeout(() => {
            refresh = true
          }, 6000)
          MessageToast("error", "اجازه دسترسی ندارید")
          refresh = false
          return false
        }
        if ((response.status === 429) && refresh) {//401
          timeAction = setTimeout(() => {
            refresh = true
          }, 6000)
          MessageToast("error", "پس از لحظاتی دوباره امتحان کنید")
          refresh = false
      
        }

        if (response?.status && (response?.status !== 403) && (response?.status !== 401) && (response?.status !== 429)) {
          // console.log("response *****" , response.json());
          // throw new Error((await response.json()).result || response.status)
          // throw new Error(response)

          return Promise.reject(response.json())

        }
      }

      store.dispatch(loading(false))
      return response
    })
    // .then(r => r[ext.toText ? "text" : "json"]())//r["json"]() r.json()
    .then(r => r.json())//r["json"]() r.json()
    .catch(e => {
      store.dispatch(loading(false))

      return Promise.reject(e)
      
    })

}


export function file_fetcher(url = Url("/url-not-filled", {}), header = {}, ext = {noAuth: true, toText: false}) {
  store.dispatch(loading(true));
  return fetch(Url(url), Header(header, ext.noAuth))
      .then(async (response) => {
          if (!response.ok) {//4xx-5xx

              if ((response.status === 401) && refresh) {//401
                  timeAction = setTimeout(() => {
                      refresh = true;
                  }, 6000);
                  store.dispatch(refreshOtp({
                      "refresh": getRereshObject()
                  }));

                  refresh = false;
              }
              if ((response.status === 403 ) && refresh ) {//401
                  timeAction = setTimeout(() => {
                      refresh = true;
                  }, 6000);
                  message.error('اجازه دسترسی ندارید');
                  refresh = false;
          }
              if(response?.status && (response?.status !== 403) && (response?.status !== 401) && (response?.status !== 429)) {
                  // throw new Error((await response.json()).message || response.status)
                return  response
              }

          }
          store.dispatch(loading(false));
          return response;
      })
      .then(r => {
         console.log(r);
         return r
       }) 
      .catch(e => {
          store.dispatch(loading(false));
          return Promise.reject(e)

      })

}





export const reset = () => {
  return {
    type: RESET
  }
}
export const loading = (data) => {
  return {
    type: LOADING,
    data
  }
}


