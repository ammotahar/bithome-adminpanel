import React, { useEffect, useState } from "react"
import { Button, Col, Form, FormGroup, Row } from "reactstrap"
import { fetcher } from "store/actions"
import queryString from "query-string"
import AsyncSelect from "react-select/async"
import {
  GET_PROJECTS,
  GET_States,
  GET_USERS_ASSETS_CUSTOMERS,
  DOWNLOAD_REPORT_DETAIlED_TRANSACTION,
} from "../../../helpers/url_helper"
import DatePicker from "react-datepicker2"
import { Slider } from "antd"
import { exportExcelFile } from "utils/exportFiles"

const Filter = props => {
  const [remove, setRemove] = useState(false)
  const [created_at, setCreated_at] = useState(null)
  const [updated_at, setUpdated_at] = useState(null)
  const [typeInputSearch, setTypeInputSearch] = useState(null)
  const [typeInputProject, setTypeInputProject] = useState(null)
  const [typeInputEstate, setTypeInputEstate] = useState(null)

  const [created_at__gte, setCreated_at__gte] = useState(null)
  const [created_at__lte, setCreated_at__lte] = useState(null)

  useEffect(() => {
    if (remove) props.getReportDetailedTransaction()
  }, [props.params])

  // function for download excel file of 'report-assets-customers'
  const handleExportExcelFile = () => {
    const queries = queryString.stringify(props.params)
    exportExcelFile(
      `${DOWNLOAD_REPORT_DETAIlED_TRANSACTION}?${queries}`,
      "report-assets-customers.xlsx"
    )
  }

  // api call service for list customers and set in dropdown menu select filter user
  const loadResults = name => {
    return fetcher(
      GET_USERS_ASSETS_CUSTOMERS,
      {
        method: "GET",
      },
      {
        query: { name: name, limit: 50000 },
      }
    )
      .then(r => {
        if (r?.result) {
          let search_list = []
          r?.result.map(i =>
            search_list.push({
              label: i?.credentials?.full_name,
              value: i?.id,
              ...i,
            })
          )

          return search_list
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })
  }

  // api call service for list projects and set in dropdown menu select filter project
  const loadProjects = name => {
    return fetcher(
      GET_PROJECTS,
      {
        method: "GET",
      },
      {
        // query: { name: name, limit: 10 }
      }
    )
      .then(r => {
        if (!!r.result.items) {
          let project_list = []
          r.result.items.map(i =>
            project_list.push({ label: i.title, value: i.id })
          )

          return project_list
        }
      })
      .catch(e => {
        // MessageToast("error", "با خطا مواجه شدید")
      })
  }

  // api call service for list estates and set in dropdown menu select filter estate
  const loadEstate = name => {
    return fetcher(
      GET_States,
      {
        method: "GET",
      },
      {
        // query: { name: name, limit: 10 }
      }
    )
      .then(r => {
        if (!!r.result.items) {
          let estate_list = []
          r.result.items.map(i =>
            estate_list.push({ label: i.title, value: i.id })
          )

          return estate_list
        }
      })
      .catch(e => {
        // MessageToast("error", "با خطا مواجه شدید")
      })
  }

  const handleReset = input => {
    // setTypeInput(null)
    setTypeInputSearch(null)
    setTypeInputProject(null)
    setTypeInputEstate(null)
  }


  const handleChangeArea = (value) => {
    props.setParams({
      ...props.params,
      page: 0,
      area__gte: value[0],
      area__lte: value[1],
    })
  } 
  
  
  const handleChangeRemindArea = (value) => {
    props.setParams({
      ...props.params,
      page: 0,
      remaining_area__gte: value[0],
      remaining_area: value[1],
    })
  }

  const handleChangeAmount = (value) => {
    props.setParams({
      ...props.params,
      page: 0,
      amount__gte: value[0] ,
      amount__lte: value[1] ,
    })
  }

  return (
    <>
      <Form id="frm">
        <Row>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر فروشنده (نام / کد ملی)</label>
            <FormGroup className="ajax-select  mt-lg-0 select2-container">
              <AsyncSelect
                defaultOptions={true}
                isClearable={() => handleReset("type")}
                name="subject-list"
                id="subject-list"
                loadOptions={loadResults}
                defaultValue={!!typeInputSearch ? typeInputSearch : ""}
                className="basic-multi-select"
                onChange={e => {
                  if (e) {
                    setTypeInputSearch(e)
                    props.setParams({
                      ...props.params,
                      page: 0,
                      seller_id : e ? e.value : null,
                    })
                  }
                }}
                placeholder="انتخاب کنید"
              />
            </FormGroup>
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر خریدار (نام / کد ملی)</label>
            <FormGroup className="ajax-select  mt-lg-0 select2-container">
              <AsyncSelect
                defaultOptions={true}
                isClearable={() => handleReset("type")}
                name="subject-list"
                id="subject-list"
                loadOptions={loadResults}
                defaultValue={!!typeInputSearch ? typeInputSearch : ""}
                className="basic-multi-select"
                onChange={e => {
                  if (e) {
                    setTypeInputSearch(e)
                    props.setParams({
                      ...props.params,
                      page: 0,
                      buyer_id : e ? e.value : null,
                    })
                  }
                }}
                placeholder="انتخاب کنید"
              />
            </FormGroup>
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر بر اساس پروژه</label>
            <FormGroup className="ajax-select  mt-lg-0 select2-container">
              <AsyncSelect
                defaultOptions={true}
                isClearable={() => handleReset("type")}
                name="subject-list"
                id="subject-list"
                loadOptions={loadProjects}
                defaultValue={!!typeInputProject ? typeInputProject : ""}
                className="basic-multi-select"
                onChange={e => {
                  if (e) {
                    setTypeInputProject(e)
                    props.setParams({
                      ...props.params,
                      page: 0,
                      project_id : e ? e.value : null,
                    })
                  }
                }}
                placeholder="انتخاب کنید"
              />
            </FormGroup>
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر بر اساس ملک</label>
            <FormGroup className="ajax-select  mt-lg-0 select2-container">
              <AsyncSelect
                defaultOptions={true}
                isClearable={() => handleReset("type")}
                name="subject-list"
                id="subject-list"
                loadOptions={loadEstate}
                defaultValue={!!typeInputEstate ? typeInputEstate : ""}
                className="basic-multi-select"
                onChange={e => {
                  if (e) {
                    setTypeInputEstate(e)
                    props.setParams({
                      ...props.params,
                      page: 0,
                      estate_id : e ? e.value : null,
                    })
                  }
                }}
                placeholder="انتخاب کنید"
              />
            </FormGroup>
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر تاریخ معامله (از تاریخ)</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at__gte}
              isGregorian={false}
              onChange={value => {
                if (value) {
                  setCreated_at__gte(value)
                  setRemove(false)
                  props.setParams({
                    ...props.params,
                    page: 0,
                    created_at__gte: value.format("YYYY-MM-DDT00:00:00"),
                  })
                }
              }}
              name="created_at__gte"
              id="created_at__gte"
              removable={true}
            />
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر تاریخ معامله (تا تاریخ)</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at__lte}
              isGregorian={false}
              onChange={value => {
                if (value) {
                  setCreated_at__lte(value)
                  setRemove(false)
                  props.setParams({
                    ...props.params,
                    page: 0,
                    created_at__lte: value.format("YYYY-MM-DDT00:00:00"),
                  })
                }
              }}
              name="created_at__lte"
              id="created_at__lte"
              removable={true}
            />
          </Col>
        </Row>
        <Row className="mt-4">
          <Col lg="" xl="" className="pt-2">
            <label>فیلتر بر اساس متراژ</label>
            <div>
              <Slider
                range
                step={1}
                max={1000}
                defaultValue={[100, 300]}
                onAfterChange={handleChangeArea}
              />
            </div>
          </Col>
        </Row>
        <Row className="mt-4">
          <Col lg="" xl="" className="pt-2">
            <label>فیلتر بر اساس متراژ باقی‌مانده</label>
            <div>
              <Slider
                range
                step={1}
                max={50000}
                defaultValue={[3000, 10000]}
                onAfterChange={handleChangeRemindArea}
              />
            </div>
          </Col>
        </Row>

        <Row className="mt-4">
          <Col lg="" xl="" className="pt-2">
            <label>فیلتر بر اساس مبلغ</label>
            <div>
              <Slider
                range
                step={1000000}
                max={20000000000}
                defaultValue={[600000000, 2000000000]}
                onAfterChange={handleChangeAmount}
              />
            </div>
          </Col>
        </Row>

        <Row className="mt-4">
          <Col lg="6" xl="" className="mt-2">
            <label className="text-white d-block"> .</label>
            <Button
              color="primary"
              onClick={() => props.getReportDetailedTransaction()}
            >
              اعمال فیلتر
            </Button>
            <Button
              color="btn btn-outline-danger"
              className="mx-2"
              onClick={() => {
                document.getElementById("frm").reset()
                setCreated_at(null)
                setUpdated_at(null)
                props.setParams({ order: "-updated_at" })
                setRemove(true)
              }}
            >
              حذف فیلتر
            </Button>
            <Button color="success" onClick={handleExportExcelFile}>
              خروجی اکسل
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  )
}

export default Filter;
