import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, NavLink, withRouter } from "react-router-dom"
import {
  Button,
  Col,
  Container,
  Row, Spinner,
  Table
} from "reactstrap"

import Breadcrumbs from "components/Common/Breadcrumb"
// import { getUsers } from "store/contacts/actions"
import { fetcher } from "store/common/action"
import { GET_REPORT_ASSETS_CUSTOMERS, GET_REPORT_DETAIlED_TRANSACTION, GET_TYPES_TRANSACTION } from "helpers/url_helper"
import { MessageToast } from "utils/message"
import Filter from "./filter"
import PaginationCustom from "../../../components/paginationZero"
import '../../../../node_modules/antd/dist/antd.compact.min.css'
import { GET_PROJECTS } from "store/projects/actionTypes"
import numberWithCommas from "components/threeNumber"
import moment from "moment-jalaali"

const ReportDetailedTransaction = props => {

//   const { users, onGetUsers } = props
  const [reportDetailedTransactions, setReportDetailedTransactions] = useState([])
  const [params, setParams] = useState({ 
    order: "-updated_at" , 

  })
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)


  useEffect(() => {
    getReportDetailedTransaction()
  }, [params])


  useEffect(() => {
    setLoading(false)
  }, [])

  // get list reports detailed transactions 
  const getReportDetailedTransaction = (p = 0, s = pageSize) => {
    fetcher(GET_REPORT_DETAIlED_TRANSACTION, {
      method: "GET"
    },{query:{page: p, size: s, owner_id : props?.match?.params?.id ,  ...params}}).then(r => {
    if (r?.result) {
        setReportDetailedTransactions(r?.result?.items)
        setCounts(r?.result?.total)
    }
  }).catch(e => {
    // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
  })
}


  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getReportDetailedTransaction(p, s)
  }


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="گزارشات" breadcrumbItem="گزارش تفضیلی معاملات" />
          <Filter params={params} setParams={setParams} getReportDetailedTransaction={getReportDetailedTransaction} />
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                        <tr>
                            <th scope="col" className="text-center">ردیف</th>
                            <th scope="col" className="text-center">کد ملک</th>
                            <th scope="col" className="text-center">نام پروژه</th>
                            <th scope="col" className="text-center"> واحد</th>
                            <th scope="col" className="text-center"> متراژ </th>
                            <th scope="col" className="text-center"> تاریخ معامله</th>
                            <th scope="col" className="text-center"> ساعت معامله</th>
                            <th scope="col" className="text-center"> مبلغ ریال</th>
                            <th scope="col" className="text-center"> نام و نام خانوادگی فروشنده</th>
                            <th scope="col" className="text-center"> کد ملی فروشنده</th>
                            <th scope="col" className="text-center"> موجودی کیف پول فروشنده (پس از معامله)</th>
                            <th scope="col" className="text-center"> نام و نام خانوادگی خریدار</th>
                            <th scope="col" className="text-center"> کد ملی خریدار</th>
                            <th scope="col" className="text-center"> موجودی کیف پول خریدار(پس از معامله)</th>
                            <th scope="col" className="text-center"> میزان متراژ باقی‌مانده از واحد</th>
                        </tr>
                    </thead>
                    <tbody>
                    {reportDetailedTransactions?.length &&
                      reportDetailedTransactions?.map((item, i) =>
                        <tr>
                          <td className="text-center"> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          <td className="text-center">{item?.estate?.estate_identifier} </td>
                          <td className="text-center">{item?.estate?.project?.title}</td>
                          <td className="text-center">{item?.estate?.title}</td>
                          <td className="text-center">{item?.area}</td>
                          <td className="text-center">{item?.created_at ? moment(item?.created_at).format('jYYYY/jMM/jDD') : ''}</td>
                          <td className="text-center">{item?.created_at ? moment(item?.created_at).format('hh:mm:ss') : ''}</td>               
                          <td className="text-center">{item?.amount ? numberWithCommas(item?.amount) : ''}</td>               
                          <td className="text-center">{item?.seller?.full_name}</td>               
                          <td className="text-center">{item?.seller?.national_code}</td>               
                          <td className="text-center">{item?.seller?.seller_inventory ? numberWithCommas(item?.seller?.seller_inventory) : ''}</td>               
                          <td className="text-center">{item?.buyer?.full_name}</td>               
                          <td className="text-center">{item?.buyer?.national_code}</td>               
                          <td className="text-center">{item?.buyer?.buyer_inventory ? numberWithCommas(item?.buyer?.buyer_inventory) : ''}</td>               
                          <td className="text-center">{item?.remaining_area}</td>               
                                             
                        </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom 
            handlePageClick={handlePageClick} 
            page={page}
            totalPage={Math.floor(counts / pageSize) + 1}
            addPageSize={setPageSize}
            pageSize={pageSize}
            chatsCount={counts} 
                            
                />
          <div className="text-center">
            {loading ? <Spinner /> : ""}
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}



export default ReportDetailedTransaction;

