import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, NavLink, withRouter } from "react-router-dom"
import {
  Button,
  Col,
  Container,
  Row, Spinner,
  Table
} from "reactstrap"

import Breadcrumbs from "components/Common/Breadcrumb"
// import { getUsers } from "store/contacts/actions"
import { fetcher } from "store/common/action"
import { GET_REPORT_ASSETS_CUSTOMERS, GET_TYPES_TRANSACTION } from "helpers/url_helper"
import { MessageToast } from "utils/message"
import Filter from "./filter"
import PaginationCustom from "../../../components/paginationZero"
import '../../../../node_modules/antd/dist/antd.compact.min.css'
import { GET_PROJECTS } from "store/projects/actionTypes"

const ReportAssetsCustomers = props => {

//   const { users, onGetUsers } = props
  const [reportAssetsCustomers, setReportAssetsCustomers] = useState([])
  const [params, setParams] = useState({ 
    order: "-updated_at" , 

  })
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)


  useEffect(() => {
    getReportAssetsCustomers()
  }, [params])


  console.log("params ---->>>>" , params);

  useEffect(() => {
    setLoading(false)
  }, [])

  // get list reports log of contacts 
  const getReportAssetsCustomers = (p = 0, s = pageSize) => {
    fetcher(GET_REPORT_ASSETS_CUSTOMERS, {
      method: "GET"
    },{query:{page: p, size: s, owner_id : props?.match?.params?.id ,  ...params}}).then(r => {
    if (r?.result) {
        setReportAssetsCustomers(r?.result?.items)
        setCounts(r?.result?.total)
    }
  }).catch(e => {
    // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
  })
}

  


  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getReportAssetsCustomers(p, s)
  }


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="گزارشات" breadcrumbItem="گزارش دارایی مشتریان" />
          <Filter params={params} setParams={setParams} getReportAssetsCustomers={getReportAssetsCustomers} />
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                        <tr>
                            <th scope="col" className="text-center">ردیف</th>
                            <th scope="col" className="text-center">شناسه ID</th>
                            <th scope="col" className="text-center">نام و نام خانوادگی</th>
                            <th scope="col" className="text-center"> کد ملی</th>
                            <th scope="col" className="text-center"> پروژه </th>
                            <th scope="col" className="text-center"> طبقه</th>
                            <th scope="col" className="text-center"> متراژ</th>
                        </tr>
                    </thead>
                    <tbody>
                    {reportAssetsCustomers?.length &&
                      reportAssetsCustomers?.map((item, i) =>
                        <tr>
                          <td className="text-center"> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          <td className="text-center">{item?.owner_id} </td>
                          <td className="text-center">{(item?.full_name)}</td>
                          <td className="text-center">{item?.national_code}</td>
                          <td className="text-center">{item?.project}</td>
                          <td className="text-center">{item?.estate_floor}</td>
                          <td className="text-center">{item?.square_meter}</td>               
                                             
                        </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom 
            handlePageClick={handlePageClick} 
            page={page}
            totalPage={Math.floor(counts / pageSize) + 1}
            addPageSize={setPageSize}
            pageSize={pageSize}
            chatsCount={counts} 
                            
                />
          <div className="text-center">
            {loading ? <Spinner /> : ""}
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}

// ReportContacts.propTypes = {
//   users: PropTypes.array,
//   onGetUsers: PropTypes.func
// }

// const mapStateToProps = ({ contacts }) => ({
//   users: contacts.users?.result?.items || []
// })

// const mapDispatchToProps = dispatch => ({
//   onGetUsers: (...data) => dispatch(getUsers(...data))
// })

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(ReportContacts))

export default ReportAssetsCustomers;

