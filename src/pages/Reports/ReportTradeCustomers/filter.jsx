import React, { useEffect, useState } from "react"
import { Button, Col, Form, FormGroup, Row } from "reactstrap"
import DatePicker from "react-datepicker2"
import { fetcher } from "store/actions"
import {
  DOWNLOAD_REPORT_TRADE_CUSTOMERS,
  GET_USERS_LOG_CONTACTS,
} from "helpers/url_helper"
import queryString from "query-string"
import AsyncSelect from "react-select/async"
import { Slider } from "antd"
import { exportExcelFile } from "utils/exportFiles"
const Filter = props => {
  const [remove, setRemove] = useState(false)
  const [created_at__gte, setCreated_at__gte] = useState(null)
  const [created_at__lte, setCreated_at__lte] = useState(null)
  const [typeInputSearch, setTypeInputSearch] = useState(null)

  useEffect(() => {
    if (remove) props.getWalletReports()
  }, [props.params])

  // function for download excel file of 'report-trade-customers'
  const handleExportExcelFile = () => {
    const queries = queryString.stringify(props.params)
    exportExcelFile(
      `${DOWNLOAD_REPORT_TRADE_CUSTOMERS}?${queries}`,
      "report-trade-customers.xlsx"
    )
  }

  // api call service for list contact and set in dropdown menu select filter user
  const loadResults = name => {
    return fetcher(
      GET_USERS_LOG_CONTACTS,
      {
        method: "GET",
      },
      {
        query: { name: name, limit: 50000 },
      }
    )
      .then(r => {
        if (r?.result) {
          let search_list = []
          r?.result.map(i =>
            search_list.push({
              label: i?.credentials?.full_name,
              value: i?.id,
              ...i,
            })
          )

          return search_list
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })
  }

  const handleReset = input => {
    setTypeInputSearch(null)
  }

  function onChange(value) {
    console.log("onChange: ", value)
  }

  
  function onAfterChange(value) {
    console.log("onAfterChange: ", value)
    props.setParams({
      ...props.params,
      page: 0,
      final_inventory__gte: value[0] * 1000,
      final_inventory__lte: value[1] * 1000,
    })
  }

  return (
    <>
      <Form id="frm">
        <Row>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر کاربر</label>
            <FormGroup className="ajax-select  mt-lg-0 select2-container">
              <AsyncSelect
                defaultOptions={true}
                isClearable={() => handleReset("type")}
                name="subject-list"
                id="subject-list"
                loadOptions={loadResults}
                defaultValue={!!typeInputSearch ? typeInputSearch : ""}
                className="basic-multi-select"
                onChange={e => {
                  if (e) {
                    setTypeInputSearch(e)
                    props.setParams({
                      ...props.params,
                      page: 0,
                      user_id: e ? e.value : null,
                    })
                  }
                }}
                placeholder="انتخاب کنید"
              />
            </FormGroup>
          </Col>

          <Col lg="3" xl="2" className="mt-2">
            <label>شارژ کیف پول از تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at__gte}
              isGregorian={false}
              onChange={value => {
                if (value) {
                  setCreated_at__gte(value)
                  setRemove(false)
                  props.setParams({
                    ...props.params,
                    page: 0,
                    created_at__gte: value.format("YYYY-MM-DDT00:00:00"),
                  })
                }
              }}
              name="created_at__gte"
              id="created_at__gte"
              removable={true}
            />
          </Col>
          <Col lg="3" xl="2" className="mt-2">
            <label>شارژ کیف پول تا تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at__lte}
              isGregorian={false}
              onChange={value => {
                if (value) {
                  setCreated_at__lte(value)
                  setRemove(false)
                  props.setParams({
                    ...props.params,
                    page: 0,
                    created_at__lte: value.format("YYYY-MM-DDT00:00:00"),
                  })
                }
              }}
              name="created_at__lte"
              id="created_at__lte"
              removable={true}
            />
          </Col>
        </Row>
        <Row className="mt-4">
          <Col lg="" xl="" className="pt-2">
            <label>فیلتر بر اساس موجودی کیف پول (تومان)</label>
            <div>
              <Slider
                range
                step={1000000}
                max={50000000000}
                defaultValue={[2000000000, 5000000000]}
                onChange={onChange}
                onAfterChange={onAfterChange}
              />
            </div>
          </Col>
        </Row>
        <Row className="mt-4">
          <label className="text-white d-block"> .</label>
          <Button color="primary" onClick={() => props.getWalletReports()}>
            اعمال فیلتر
          </Button>
          <Button
            color="btn btn-outline-danger"
            className="mx-2"
            onClick={() => {
              document.getElementById("frm").reset()
              setCreated_at__gte(null)
              setCreated_at__lte(null)
              props.setParams({ order: "-updated_at" })
              setRemove(true)
            }}
          >
            حذف فیلتر
          </Button>
          <Button color="success" onClick={handleExportExcelFile}>
            خروجی اکسل
          </Button>
        </Row>
      </Form>
    </>
  )
}

export default Filter;
