import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, NavLink, withRouter } from "react-router-dom"
import {
  Button,
  Col,
  Container,
  Row, Spinner,
  Table
} from "reactstrap"

import Breadcrumbs from "components/Common/Breadcrumb"
// import { getUsers } from "store/contacts/actions"
import { fetcher } from "store/common/action"
import { GET_REPORT_CONTACTS } from "helpers/url_helper"
import { MessageToast } from "utils/message"
import Filter from "./filter"
import PaginationCustom from "../../../components/paginationZero"
import moment from "moment-jalaali"
import icon_pic from '../../../assets/images/download_pic_icon.svg'
const ReportContacts = props => {

//   const { users, onGetUsers } = props
  const [contactReports, setContactReports] = useState([])
  const [params, setParams] = useState({ 
    order: "-updated_at"
 })
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)


  useEffect(() => {
    getContactReports()
  }, [params])


  useEffect(() => {
    setLoading(false)
  }, [])

  // get list contact report
  const getContactReports = (p = 0, s = pageSize) => {
    fetcher(GET_REPORT_CONTACTS, {
      method: "GET"
    // },{query:{page: p, size: s, owner_id : props?.match?.params?.id ,  ...params}}).then(r => {
    },{query:{page: p, size: s ,  ...params}}).then(r => {
    if (r?.result) {
      setContactReports(r?.result?.items)
      setCounts(r?.result?.total)
    }
  }).catch(e => {
    // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
  })
}
  


  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getContactReports(p, s)
  }


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="گزارشات" breadcrumbItem="گزارش کاربران" />
          <Filter params={params} setParams={setParams} getContactReports={getContactReports} />
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                        <tr>
                            <th scope="col" className="text-center">ردیف</th>
                            <th scope="col" className="text-center">نام و نام خانوادگی</th>
                            <th scope="col" className="text-center"> کد ملی</th>
                            <th scope="col" className="text-center"> شماره موبایل</th>
                            <th scope="col" className="text-center"> تاریخ تولد</th>
                            <th scope="col" className="text-center"> آدرس ایمیل</th>
                            <th scope="col" className="text-center"> آدرس منزل</th>
                            <th scope="col" className="text-center"> شماره شبا</th>
                            <th scope="col" className="text-center">  عکس کاربر</th>
                        </tr>
                    </thead>
                    <tbody>
                    {contactReports?.length &&
                      contactReports?.map((item, i) =>
                        <tr>
                          <td className="text-center"> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          <td className="text-center">{(item?.credentials?.first_name || "") + " " + (item?.credentials?.last_name || "")}</td>
                          <td>{item?.national_code}</td>
                          <td>{item?.mobile}</td>
                          <td className="text-center">
                            {item?.credentials?.birth_date ? moment(item?.credentials?.birth_date).format('jYYYY/jMM/jDD') : ''}
                          </td>
                          <td className="text-center">
                            {item?.credentials?.email}
                          </td>               
                          <td className="text-left">
                            {item?.credentials?.address}
                          </td>           
                          <td className="text-center">
                            {item?.bank_accounts[0]?.sheba_number}
                          </td>       
                          <td className="text-center">
                              <a target="_blank" href={item?.picture_link}>
                                <img src={icon_pic} alt="pic-icon" ></img>
                              </a>
                          </td>
                 
                        </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom 
            handlePageClick={handlePageClick} page={page}
            totalPage={Math.floor(counts / pageSize) + 1}
            addPageSize={setPageSize}
            pageSize={pageSize}
            chatsCount={counts} 
                            
                />
          <div className="text-center">
            {loading ? <Spinner /> : ""}
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}

// ReportContacts.propTypes = {
//   users: PropTypes.array,
//   onGetUsers: PropTypes.func
// }

// const mapStateToProps = ({ contacts }) => ({
//   users: contacts.users?.result?.items || []
// })

// const mapDispatchToProps = dispatch => ({
//   onGetUsers: (...data) => dispatch(getUsers(...data))
// })

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(ReportContacts))

export default ReportContacts;
