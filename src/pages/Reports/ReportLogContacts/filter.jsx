import React, { useEffect, useState } from "react"
import { Button, Col, Form, FormGroup, Row } from "reactstrap"
import DatePicker from "react-datepicker2"
import { fetcher } from "store/actions"
import {
  DOWNLOAD_REPORT_LOG_CONTACTS,
  GET_USERS_LOG_CONTACTS,
} from "helpers/url_helper"
import queryString from "query-string"
import AsyncSelect from "react-select/async"
import { exportExcelFile } from "utils/exportFiles"

const Filter = props => {
  const [remove, setRemove] = useState(false)
  const [created_at__gt, setCreated_at__gt] = useState(null)
  const [created_at__lt, setCreated_at__lt] = useState(null)
  const [typeInputSearch, setTypeInputSearch] = useState(null)

  useEffect(() => {
    if (remove) props.getContactLogReports()
  }, [props.params])

  // function for download excel file of 'report-log-contacts'
  const handleExportExcelFile = () => {
    const queries = queryString.stringify(props.params)
    exportExcelFile(
      `${DOWNLOAD_REPORT_LOG_CONTACTS}?${queries}`,
      "report-log-contacts.xlsx"
    )
  }

  // api call service for list contact and set in dropdown menu select filter user
  const loadResults = name => {
    return fetcher(
      GET_USERS_LOG_CONTACTS,
      {
        method: "GET",
      },
      {
        query: { name: name, limit: 50000 },
      }
    )
      .then(r => {
        if (r?.result) {
          let search_list = []
          r?.result.map(i =>
            search_list.push({
              label: i?.credentials?.full_name,
              value: i?.id,
              ...i,
            })
          )

          return search_list
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })
  }

  const handleReset = input => {
    setTypeInputSearch(null)
  }

  return (
    <>
      <Form id="frm">
        <Row>
          <Col lg="4" xl="3" className="mt-2">
            <label>فیلتر کاربر</label>
            <FormGroup className="ajax-select  mt-lg-0 select2-container">
              <AsyncSelect
                defaultOptions={true}
                isClearable={() => handleReset("type")}
                name="subject-list"
                id="subject-list"
                loadOptions={loadResults}
                defaultValue={!!typeInputSearch ? typeInputSearch : ""}
                className="basic-multi-select"
                onChange={e => {
                  if (e) {
                    setTypeInputSearch(e)
                    props.setParams({
                      ...props.params,
                      page: 0,
                      user_id: e ? e.value : null,
                    })
                  }
                }}
                placeholder="انتخاب کنید"
              />
            </FormGroup>
          </Col>

          <Col lg="3" xl="2" className="mt-2">
            <label>ایجاد شده در تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at__gt}
              isGregorian={false}
              onChange={value => {
                if (value) {
                  setCreated_at__gt(value)
                  setRemove(false)
                  props.setParams({
                    ...props.params,
                    page: 0,
                    created_at__gt: value.format("YYYY-MM-DD"),
                  })
                }
              }}
              name="created_at__gt"
              id="created_at__gt"
              removable={true}
            />
          </Col>
          <Col lg="3" xl="2" className="mt-2">
            <label>بروز شده در تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at__lt}
              isGregorian={false}
              onChange={value => {
                if (value) {
                  setCreated_at__lt(value)
                  setRemove(false)
                  props.setParams({
                    ...props.params,
                    page: 0,
                    created_at__lt: value.format("YYYY-MM-DD"),
                  })
                }
              }}
              name="created_at__lt"
              id="created_at__lt"
              removable={true}
            />
          </Col>
        </Row>
        <Row className="mt-4">
          <label className="text-white d-block"> .</label>
          <Button color="primary" onClick={() => props.getContactLogReports()}>
            اعمال فیلتر
          </Button>
          <Button
            color="btn btn-outline-danger"
            className="mx-2"
            onClick={() => {
              document.getElementById("frm").reset()
              setCreated_at__gt(null)
              setCreated_at__lt(null)
              props.setParams({ order: "-updated_at" })
              setRemove(true)
            }}
          >
            حذف فیلتر
          </Button>
          <Button color="success" onClick={handleExportExcelFile}>
            خروجی اکسل
          </Button>
        </Row>
      </Form>
    </>
  )
}

export default Filter
