import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, NavLink, withRouter } from "react-router-dom"
import {
  Button,
  Col,
  Container,
  Row, Spinner,
  Table
} from "reactstrap"
import moment from "moment-jalaali"
import Breadcrumbs from "components/Common/Breadcrumb"
// import { getUsers } from "store/contacts/actions"
import { fetcher } from "store/common/action"
import { GET_REPORT_LOG_CONTACTS } from "helpers/url_helper"
import { MessageToast } from "utils/message"
import Filter from "./filter"
import PaginationCustom from "../../../components/paginationZero"


const ReportLogContacts = props => {

//   const { users, onGetUsers } = props
  const [contactLogReports, setContactLogReports] = useState([])
  const [params, setParams] = useState({ order: "-updated_at" })
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)
  

  useEffect(() => {
    getContactLogReports()
  }, [params])


  useEffect(() => {
    setLoading(false)
  }, [])

  // get list reports log of contacts 
  const getContactLogReports = (p = 0, s = pageSize) => {
    fetcher(GET_REPORT_LOG_CONTACTS, {
      method: "GET"
    },{query:{page: p, size: s ,  ...params}}).then(r => {
    if (r?.result) {
        setContactLogReports(r?.result?.items)
        setCounts(r?.result?.total)
    }
  }).catch(e => {
    // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
  })
}

  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getContactLogReports(p, s)
  }


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="گزارشات" breadcrumbItem="گزارش لاگ کاربران" />
          <Filter params={params} setParams={setParams} getContactLogReports={getContactLogReports} />
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                        <tr>
                            <th scope="col" className="text-center">ردیف</th>
                            <th scope="col" className="text-center">شناسه ID</th>
                            <th scope="col" className="text-center"> ID</th>
                            <th scope="col" className="text-center">نام و نام خانوادگی</th>
                            <th scope="col" className="text-center"> کد ملی</th>
                            <th scope="col" className="text-center"> شماره موبایل </th>
                            <th scope="col" className="text-center"> پلتفرم</th>
                            <th scope="col" className="text-center"> تاریخ</th>
                            <th scope="col" className="text-center"> ساعت ورود</th>
                            <th scope="col" className="text-center"> ساعت خروج</th>
                        </tr>
                    </thead>
                    <tbody>
                    {contactLogReports?.length &&
                      contactLogReports?.map((item, i) =>
                      <tr>
                          <td className="text-center"> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          <td className="text-center">{item?.user_id} </td>
                          <td className="text-center">{item?.id} </td>
                          <td className="text-center">{(item?.user?.credentials?.full_name)}</td>
                          <td className="text-center">{item?.user?.national_code}</td>
                          <td className="text-center">{item?.user?.mobile}</td>
                          <td className="text-center">{item?.platform}</td>               
                          <td className="text-center">{item?.created_at ? moment(item?.created_at).format('jYYYY/jMM/jDD') : ''}</td>
                          <td className="text-center">{item?.created_at ? moment(item?.created_at).format(' hh:mm:ss - jYYYY/jMM/jDD') : ''}</td>           
                          <td className="text-center">{item?.logout_time ? moment(item?.logout_time).format('jYYYY/jMM/jDD') : ''}</td>           
                        </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom 
            handlePageClick={handlePageClick} 
            page={page}
            totalPage={Math.floor(counts / pageSize) + 1}
            addPageSize={setPageSize}
            pageSize={pageSize}
            chatsCount={counts} 
                            
                />
          <div className="text-center">
            {loading ? <Spinner /> : ""}
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}

// ReportContacts.propTypes = {
//   users: PropTypes.array,
//   onGetUsers: PropTypes.func
// }

// const mapStateToProps = ({ contacts }) => ({
//   users: contacts.users?.result?.items || []
// })

// const mapDispatchToProps = dispatch => ({
//   onGetUsers: (...data) => dispatch(getUsers(...data))
// })

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(ReportContacts))

export default ReportLogContacts;
