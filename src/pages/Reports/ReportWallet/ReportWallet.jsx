import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, NavLink, withRouter } from "react-router-dom"
import {
  Button,
  Col,
  Container,
  Row, Spinner,
  Table
} from "reactstrap"
import moment from "moment-jalaali"
import Breadcrumbs from "components/Common/Breadcrumb"
// import { getUsers } from "store/contacts/actions"
import { fetcher } from "store/common/action"
import { GET_REPORT_WALLET } from "helpers/url_helper"
import { MessageToast } from "utils/message"
import Filter from "./filter"
import PaginationCustom from "../../../components/paginationZero"
import numberWithCommas from "components/threeNumber"


const ReportWallet = props => {

//   const { users, onGetUsers } = props
  const [walletReports, setWalletReports] = useState([])
  const [params, setParams] = useState({ order: "-updated_at" })
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)
  

  useEffect(() => {
    getWalletReports()
  }, [params])


  useEffect(() => {
    setLoading(false)
  }, [])

  // get list reports log of contacts 
  const getWalletReports = (p = 0, s = pageSize) => {
    fetcher(GET_REPORT_WALLET, {
      method: "GET"
    },{query:{page: p, size: s ,  ...params}}).then(r => {
    if (r?.result) {
        setWalletReports(r?.result?.items)
        setCounts(r?.result?.total)
    }
  }).catch(e => {
    // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
  })
}

  const handlePageClick = (p = page, s = pageSize) => {
    setPage(p)
    getWalletReports(p, s)
  }

  const convertTypeTransaction = (value) => {
      switch (value) {
            case "money_deposit":
              return  'آنلاین';

            case "money_deposit_bank":
                return  'دستی';

            case "money_withdraw":
                    return  '';
      
          default:
              return 'آنلاین';
      }
  }


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="گزارشات" breadcrumbItem="گزارش کیف پول" />
          <Filter params={params} setParams={setParams} getWalletReports={getWalletReports} />
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                        <tr>
                            <th scope="col" className="text-center">ردیف</th>
                            <th scope="col" className="text-center">مشتری ID</th>
                            <th scope="col" className="text-center">نام و نام خانوادگی</th>
                            <th scope="col" className="text-center"> کد ملی</th>
                            <th scope="col" className="text-center"> تاریخ تولد</th>
                            <th scope="col" className="text-center"> تاریخ شارژ کیف پول</th>
                            <th scope="col" className="text-center"> ساعت شارژ کیف پول</th>
                            <th scope="col" className="text-center"> درگاه واریز(آنلاین/دستی)</th>
                            <th scope="col" className="text-center"> موجودی اولیه کیف پول(ریال)</th>
                            <th scope="col" className="text-center"> واریز </th>
                            <th scope="col" className="text-center"> برداشت</th>
                            <th scope="col" className="text-center"> موجودی کیف پول پس از شارژ کیف پول(ریال)</th>
                        </tr>
                    </thead>
                    <tbody>
                    {walletReports?.length &&
                      walletReports?.map((item, i) =>
                      <tr>
                          <td className="text-center"> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          <td className="text-center">{item?.user_id} </td>
                          <td className="text-center">{(item?.user?.full_name)}</td>
                          <td className="text-center">{item?.user?.national_code}</td>
                          <td className="text-center">{item?.user?.birth_date }</td>
                          <td className="text-center">{item?.created_at ? moment(item?.created_at).format('jYYYY/jMM/jDD') : ''}</td>               
                          <td className="text-center">{item?.created_at ? moment(item?.created_at).format('HH:MM:DD') : ''}</td>
                          <td className="text-center">{item?.transaction_type   ? convertTypeTransaction(item?.transaction_type) : ''}</td>           
                          <td className="text-center">{item?.inventory ? numberWithCommas(item?.inventory): ''}</td>           
                          <td className="text-center">     
                            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                                    <input
                                        type="checkbox"
                                        className="custom-control-input"
                                        id={"customCheckcolorUser" + (i + 1)}
                                        checked={item?.transaction_type === "money_deposit" || item?.transaction_type === "money_deposit_bank"}
                                    />
                                    <label
                                        className="custom-control-label"
                                        htmlFor={"customCheckcolorUser" + (i + 1)}
                                    >
                                    </label>
                            </div>
                        </td>           
                        <td className="text-center">
                            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                                    <input
                                        type="checkbox"
                                        className="custom-control-input"
                                        id={"customCheckcolorUser" + (i + 1)}
                                        checked={item?.transaction_type === "money_withdraw"}
                                    />
                                    <label
                                        className="custom-control-label"
                                        htmlFor={"customCheckcolorUser" + (i + 1)}
                                    >
                                    </label>
                            </div>
                        </td>           
                        <td className="text-center">{item?.final_inventory ? numberWithCommas(item?.final_inventory) : ''}</td>           
                    </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom 
            handlePageClick={handlePageClick} 
            page={page}
            totalPage={Math.floor(counts / pageSize) + 1}
            addPageSize={setPageSize}
            pageSize={pageSize}
            chatsCount={counts} 
                            
                />
          <div className="text-center">
            {loading ? <Spinner /> : ""}
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}



export default ReportWallet;
