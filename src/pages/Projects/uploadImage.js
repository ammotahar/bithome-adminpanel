import React, { useEffect, useState } from "react"
import { Button, CardBody, CardTitle, Col, Form, FormGroup, Input, Label, Row, Table } from "reactstrap"
import { FaTimesCircle, FaTrash } from "react-icons/all"
import { MessageToast } from "../../utils/message"
import Dropzone from "react-dropzone"
import { uploadFile } from "../../store/projects/function"
import { fetcher } from "../../store/common/action"
import { media, media_types, projects_detail, projects_types } from "../../utils/constant"

const initialImg = {
  "title": null,
  "media_type": null,
  img: null,
  loading: false
}
const UploadImage = (props) => {
  const [image, setImage] = useState(initialImg)
  const [mediaType, setMediaType] = useState([])
  const { error, setError, selectedFiles, setselectedFiles, setFormData, formData ,delete_Media} = props


  useEffect(() => {

    fetcher(media_types, {
      method: "GET"
    }).then(r => {
      if (r?.result) {
        console.log("category > ",r?.result);
        setMediaType(r?.result)
      }
    }).catch(e => {
    })

  }, [])

  function handleAcceptedFiles(files) {
    if (image?.title && image?.media_type) {
      setImage({ ...image, loading: true })
      uploadFile(files, image).then(r => {
        if (r?.result) {
          setImage({ ...image, img: (files[0]), ...r?.result, loading: false })
        } else {
          setImage({ ...image, loading: false })
        }
      }).catch(e => {
        setImage({ ...image, loading: false })
      })

    } else {
      !image?.title && setError(["name_image"])
      !image?.media_type && setError(["media_type"])

    }
  }

  const deleteMedia = (id) => {
    fetcher(media + id + "/", {
      method: "DELETE"
    }).then(r => {
      let list = []
      let listId = []
      list = selectedFiles.filter(t => t?.id !== id)
      listId = list.map(t => t?.id)
      setselectedFiles(list)
      delete_Media(listId )
    }).catch(e => {
      let list = []
      let listId = []
      list = selectedFiles.filter(t => t?.id !== id)
      listId = list.map(t => t?.id)
      setselectedFiles(list)
      delete_Media(listId )
    })
  }
  return (
    <>
      <CardTitle className="mb-3 mt-5">تصاویر</CardTitle>
      <FormGroup className="mb-4" row>
        <Label className="col-form-label col-lg-2">
          آپلود فایل
        </Label>

        <Col lg="5">
          <div>
            <Label className="col-form-label  d-block">
              دسته‌بندی تصویر
            </Label>
            <select className="form-control"
                    id="category"
                    onChange={(e) => setImage({ ...image, media_type: e.target.value })}
                    name="category">
              <option className="text-secondary"> انتخاب دسته بندی تصویر</option>
              {
                mediaType.map((option, i) => <option value={option?.title}>{option?.title_fa}</option>)
              }
            </select>
            {error.some(t => t === "media_type") && <small className="text-danger">
              <span className="pr-1">  <FaTimesCircle /></span>

              {"نوع تصویر را انتخاب  کنید"}
            </small>}
          </div>
          <div className="mt-3">

            <Label
              className="col-form-label  d-block"
            >
              نام تصویر
            </Label>
            <Input
              id="name_image"
              name="name_image"
              type="text"
              className="form-control"
              placeholder=" نام تصویر ..."
              onChange={(e) => setImage({ ...image, title: e.target.value })}
            />
            {error.some(t => t === "name_image") && <small className="text-danger">
              <span className="pr-1">  <FaTimesCircle /></span>

              {"نام تصویر را وارد کنید"}
            </small>}
          </div>
          <div className="mt-3">
            <Button type="button" color="primary" onClick={() => {
              if (image?.id && (!formData?.medias.some(t => t === image?.id))) {
                let list = selectedFiles
                list.push(image)
                setselectedFiles(list)
                setFormData(image?.id)

                setImage({
                  ...image,
                  "title": image?.title,
                  "media_type": image?.media_type?.title,
                  img: null,
                  loading: false
                })
              } else {
                MessageToast("warning", "تصویری آپلود کنید")
              }

            }}>
              اضافه کردن تصویر
            </Button>
          </div>
        </Col>
        <Col lg="5">
          <Form>
            <small>پس از انتخاب دسته بندی و نام فایل ، فایل موردنظر را آپلود کنید</small>
            <Dropzone
              onDrop={acceptedFiles => {
                handleAcceptedFiles(acceptedFiles)
              }}
            >
              {({ getRootProps, getInputProps }) => (
                <div className="dropzone">
                  <div
                    className="dz-message needsclick"
                    {...getRootProps()}
                  >
                    <input {...getInputProps()} />

                    <div className="dz-message needsclick">

                      {
                        image?.loading ?
                          <div className="text-center my-3">
                                        <span className="text-success">
                                          <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />
                                        درحال دریافت تصویر ...
                                        </span>
                          </div>
                          :
                          (image?.img ? <img src={URL.createObjectURL(image?.img)} alt="" /> :
                            <>
                              <div className="mb-3">
                                <i className="display-4 text-muted bx bxs-cloud-upload" />
                              </div>
                              <h4>فایل را بکشید یا کلیک کنید</h4>
                            </>)
                      }

                    </div>
                  </div>
                </div>
              )}
            </Dropzone>

          </Form>
        </Col>
        {selectedFiles?.length ?  <>
        <Label className="col-form-label col-lg-2">
          لیست تصاویر
        </Label>
        <Col lg="10">
          <div
            className="dropzone-previews mt-3"
            id="file-previews">
            <div className="table-responsive">
              <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                <thead>
                <tr>
                  <th>شماره</th>
                  <th scope="col" style={{ width: "100px" }}>تصویر</th>
                  <th scope="col">عنوان تصویر</th>
                  {/*<th scope="col">سایز</th>*/}
                  <th scope="col">دسته بندی</th>
                  <th scope="col" className="text-center">حذف</th>
                </tr>
                </thead>
                <tbody>
                {selectedFiles.map((f, i) => (
                  <tr key={i}>
                    <td>{i + 1}</td>
                    <td>
                      <img
                        data-dz-thumbnail=""
                        height="80"
                        className="avatar-sm rounded bg-light"
                        alt={f?.img?.name}
                        src={f?.url}
                      />
                    </td>
                    <td>{f?.title}</td>
                    {/*<td>{formatBytes(f.img.size)}</td>*/}
                    <td>{f?.media_type?.title}</td>
                    <td className="text-center">
                              <span className="text-danger " style={{ cursor: "pointer" }}
                                    onClick={() => deleteMedia(f?.id)}>
                                <FaTrash />
                              </span>
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </div>

          </div>
        </Col>
          </>:""
        }
      </FormGroup>

    </>
  )
}

export default UploadImage