import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { NavLink, Link, withRouter } from "react-router-dom"
import { map } from "lodash"
import {
  Badge,
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
  Table,
  Dropdown,
  Modal, Button
} from "reactstrap"
import { fetcher } from "store/common/action"
//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"
import { projects, projects_delete } from "utils/constant"


import { getProjects } from "store/actions"
import { BiDotsVertical, FaPlusCircle } from "react-icons/all"
import { infoLoading } from "../../store/common/reducer"
import { MessageToast } from "../../utils/message"
import PaginationCustom from "../../components/paginationZero"
import { GET_COUNT_ESTATES, GET_COUNT_PROJECTS } from "../../helpers/url_helper"


const Action = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false)
  const [modal, setModal] = useState(false)
  return <div dir="ltr" className="text-right ">
    <Button href="#" color="danger" onClick={() => setModal(true)}>
      حذف
    </Button>
  {/*  <Dropdown isOpen={dropdownOpen}*/}
  {/*                                                        toggle={() => setDropdownOpen(!dropdownOpen)}>*/}
  {/*  <DropdownToggle caret className="btn-light">*/}
  {/*    <span className="text-dark"> <BiDotsVertical /></span>*/}

  {/*  </DropdownToggle>*/}
  {/*  <DropdownMenu right>*/}

  {/*    <DropdownItem href="#" className="text-info">*/}
  {/*      <Link to={"/projects-overview/" + props?.id + "/" + props?.title}>جزییات</Link>*/}

  {/*    </DropdownItem>*/}
  {/*    <DropdownItem href="#" className="text-warning">*/}
  {/*      <NavLink to={`/projects-create/${props?.id}`} className="d-block w-100">ویرایش</NavLink>*/}

  {/*    </DropdownItem>*/}
  {/*    <DropdownItem href="#" className="text-danger" onClick={() => setModal(true)}>*/}
  {/*      حذف*/}
  {/*    </DropdownItem>*/}
  {/*  </DropdownMenu>*/}
  {/*</Dropdown>*/}
    <Modal centered={true}  isOpen={modal} toggle={() => setModal(false)}>
      <div className="p-4">
        <p className="pb-4">آیا از حذف این پروژه اطمینان دارید؟</p>
        <div className="row">

          <div className="col-6">
            <Button type="button" color="danger" className="w-100"
                    onClick={() => setModal(false)}>خیر</Button>
          </div>

          <div className="col-6">
            <Button type="button" color="primary" className="w-100"
                    onClick={ ()=> {
                      props.deleteProject()
                      setModal(false)
                    }}>بله</Button>
          </div>
        </div>
      </div>
    </Modal>
  </div>
}
const ProjectsList = props => {

  const { projects, onGetProjects ,listCount} = props
  const [params, setParams] = useState({})
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  // const [counts, setCounts] = useState(0)
  // console.log(props)
// const [projectsList,setProjectsList]=useState([]);
//   useEffect(() => {
//     fetcher(projects,{
//       method:"GET"
//     }).then(r=>{
//       if(r?.result){
//         setProjectsList(r?.result)
//       }
//     }).catch(e=>{})
//     // props.getProjects()
//   }, [])

  useEffect(() => {
    setFilter()

  }, [onGetProjects])

  // const getCount = (id) => {
  //   fetcher(GET_COUNT_PROJECTS, {
  //     method: "GET"
  //   }).then(r => {
  //     if(r?.result)
  //       setCounts(r?.result.estates_count)
  //   }).catch(e => {
  //     // MessageToast("error", "باخطا مواجه شدید")
  //   })
  // }
  const deleteProject = (id) => {
    fetcher(projects_delete(id), {
      method: "DELETE"
    }).then(r => {
      onGetProjects()
      MessageToast("success", "پروژه حذف شد")
    }).catch(e => {
      onGetProjects()
      MessageToast("error", "باخطا مواجه شدید")
    })

  }
  const setFilter = (p = 0, s = pageSize) => {
    onGetProjects({  query: { page: p, size: s,...params } })
  }
  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    setFilter(p, s)
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="پروژه‌ها" breadcrumbItem="لیست پروژه‌ها" />

          <div className="text-right">
            <Link to="/projects-create" className="btn btn-info">
              <span className="pr-2"><FaPlusCircle/></span>
              ایجاد پروژه جدید </Link>
          </div>

          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        ردیف
                      </th>
                      <th scope="col"> نام پروژه</th>
                      {/*<th scope="col" className="text-center"> تعداد ملک</th>*/}
                      {/*<th scope="col">سال شروع</th>*/}
                      {/*<th scope="col">سال پایان</th>*/}
                      {/*<th scope="col">وضعیت</th>*/}
                      <th scope="col">موقعیت پروژه</th>
                      <th scope="col" className="text-center"> نمایش</th>
                      <th scope="col" className="text-center">  فعالیت </th>
                      <th scope="col" className="text-center">  جزییات </th>
                      <th scope="col" className="text-center">  ویرایش </th>
                      {/*<th scope="col">نام سازنده</th>*/}
                      {/*<th scope="col">مهندس سازنده</th>*/}
                      {/*<th scope="col">ناظر</th>*/}
                      {/*<th scope="col">شرکت سازنده</th>*/}
                      {/*<th scope="col" className="text-right">عملیات</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {map(projects, (project, index) => (
                      <tr key={index}>
                        <td className="text-center">
                        {page == 0 ?  ++index : (page * pageSize) + ++index }
                          {/*<img*/}
                          {/*  src={companies[project.img]}*/}
                          {/*  alt=""*/}
                          {/*  className="avatar-sm"*/}
                          {/*/>*/}
                          {/*<span className="avatar-sm"><FaUserAlt/></span>*/}
                        </td>
                        <td style={{ minWidth: "200px" }}>
                          <h5 className="text-truncate font-size-16">

                              {project?.title}

                          </h5>
                          {/*<p className="text-muted mb-0 text-wrap">*/}
                          {/*  {project?.description}*/}
                          {/*</p>*/}
                        </td>
                        {/*<td className="text-center">2</td>*/}
                        {/*<td>{project?.start_year}</td>*/}
                        {/*<td>{project?.end_year}</td>*/}
                        {/*<td>*/}
                        {/*  <Badge color={project.color}>*/}
                        {/*    {project?.status}*/}
                        {/*  </Badge>*/}
                        {/*</td>*/}
                        <td>{project?.province + "،" + project?.city + "،" + project?.district}</td>
                        <td className="text-center">
                          <input type="checkbox" checked={!project?.is_archived} />
                        </td>
                        <td className="text-center">
                          <input type="checkbox" checked={project?.is_approved}  />
                        </td>
                        <td className="text-center">
                          <Link className="btn btn-primary" to={"/projects-overview/" + project?.id + "/" + project?.title}>لیست واحدها</Link>

                        </td>
                        <td className="text-center">
                          <NavLink className="btn btn-success"  to={`/projects-create/${project?.id}`} >ویرایش</NavLink>
                        </td>
                        {/*<td>*/}
                        {/*  <Action deleteProject={()=>deleteProject(project?.id)}  id={project?.id} title={project?.title} />*/}
                        {/*</td>*/}

                      </tr>
                    ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom handlePageClick={handlePageClick} page={page}
                            totalPage={Math.floor(listCount / pageSize) + 1}
                            addPageSize={setPageSize}
                            pageSize={pageSize}
                            chatsCount={listCount} />
          {/*<Row>*/}
          {/*  <Col xs="12">*/}
          {/*    <div className="text-center my-3">*/}
          {/*      <Link to="#" className="text-success">*/}
          {/*        <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />*/}
          {/*        بیشتر ...*/}
          {/*      </Link>*/}
          {/*    </div>*/}
          {/*  </Col>*/}
          {/*</Row>*/}

        </Container>
      </div>
    </React.Fragment>
  )
}

ProjectsList.propTypes = {
  projects: PropTypes.array,
  onGetProjects: PropTypes.func
}

const mapStateToProps = ({ projects }) => ({
  projects: projects.projects?.result?.items,
  listCount: projects.projects?.result?.total,
  infoLoading: projects.infoLoading
})

const mapDispatchToProps = dispatch => ({
  onGetProjects: (...data) => dispatch(getProjects(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProjectsList))