import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { Link, NavLink, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { isEmpty, map } from "lodash"
import {
  Badge, Button,
  Col,
  Container,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Modal,
  Row,
  Table
} from "reactstrap"

import Breadcrumbs from "components/Common/Breadcrumb"

import { getProjectDetail } from "store/projects/actions"
import { BiDotsVertical } from "react-icons/all"
import { fetcher } from "../../../store/common/action"
import { GET_States_DETAIL } from "../../../helpers/url_helper"
import { MessageToast } from "../../../utils/message"

const Action = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false)
  const [modal, setModal] = useState(false)
  return <div dir="ltr" className="text-right "><Dropdown isOpen={dropdownOpen}
                                                          toggle={() => setDropdownOpen(!dropdownOpen)}>
    <DropdownToggle caret className="btn-light">
      <span className="text-dark"> <BiDotsVertical /></span>

    </DropdownToggle>
    <DropdownMenu right>

      {/*<DropdownItem href="#" className="text-info">*/}
      {/*  جزییات*/}
      {/*</DropdownItem>*/}
      <DropdownItem href="#" className="text-warning">
        <NavLink to={`/state-create/${props?.id}`} className="d-block w-100">ویرایش</NavLink>
      </DropdownItem>
      <DropdownItem href="#" className="text-danger" onClick={() => setModal(true)}>
        حذف
      </DropdownItem>


    </DropdownMenu>
  </Dropdown>
    <Modal centered={true} isOpen={modal} toggle={() => setModal(false)}>
      <div className="p-4">
        <p className="pb-4">آیا از حذف این پروژه اطمینان دارید؟</p>
        <div className="row">

          <div className="col-6">
            <Button type="button" color="danger" className="w-100"
                    onClick={() => setModal(false)}>خیر</Button>
          </div>

          <div className="col-6">
            <Button type="button" color="primary" className="w-100"
                    onClick={() => {
                      props.deleteEstate()
                      setModal(false)
                    }
                    }>بله</Button>
          </div>
        </div>
      </div>
    </Modal></div>
}
const ProjectsOverview = props => {

  const {
    projectDetail,
    match: { params },
    onGetProjectDetail
  } = props

  useEffect(() => {
    if (params && params.id) {
      onGetProjectDetail(params.id)
    } else {
      onGetProjectDetail(0) //remove this after full integration
    }
  }, [params])

  const deleteEstate = (id) => {
    fetcher(GET_States_DETAIL(id), {
      method: "DELETE"
    }).then(r => {
      onGetProjectDetail(params.id)
      MessageToast("success", "ملک حذف شد")
      // setModal(false)
    }).catch(e => {
      MessageToast("error", "باخطا مواجه شدید")
    })

  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="پروژه ها" breadcrumbItem={`جزییات پروژه ${params?.title}`} />

          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        شماره
                      </th>
                      <th scope="col">نام ملک</th>
                      {/*<th scope="col" className="text-center"> پروژه ملک</th>*/}
                      {/*<th scope="col">تاریخ</th>*/}
                      {/*<th scope="col">وضعیت</th>*/}
                      <th scope="col">موقعیت پروژه</th>
                      {/*<th scope="col">نام سازنده</th>*/}
                      <th scope="col" className="text-center"> نماد ملک</th>
                      <th scope="col" className="text-right">عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    {map(projectDetail, (project, index) => (
                      <tr key={index}>
                        <td className="text-center">
                          {index + 1}
                          {/*<img*/}
                          {/*  src={companies[project.img]}*/}
                          {/*  alt=""*/}
                          {/*  className="avatar-sm"*/}
                          {/*/>*/}
                          {/*<span className="avatar-sm"><FaUserAlt/></span>*/}
                        </td>
                        <td>
                          <h5 className="text-truncate font-size-14">
                            <Link
                              to={`/projects-overview/${project.id}`}
                              className="text-dark"
                            >
                              {project.title}
                            </Link>
                          </h5>
                          <p className="text-muted mb-0">
                            {project.description}
                          </p>
                        </td>
                        {/*<td className="text-center">2</td>*/}
                        {/*<td>{project.area}</td>*/}
                        {/*<td>*/}
                        {/*  <Badge color={project.color}>*/}
                        {/*    {project.status}*/}
                        {/*  </Badge>*/}
                        {/*</td>*/}
                        <td>{project.area}</td>
                        {/*<td>*/}
                        {/*  رضایی*/}
                        {/*</td>*/}
                        <td className="text-center">
                          {project?.estate_identifier}
                        </td>
                        <td>
                          <Action  deleteEstate={() => deleteEstate(project?.id)} setModal={() => setModal(true)} id={project?.id} title={project?.title}/>

                        </td>
                      </tr>
                    ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>

          <Row>
            {/*<Col xs="12">*/}
            {/*  <div className="text-center my-3">*/}
            {/*    <Link to="#" className="text-success">*/}
            {/*      <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />*/}
            {/*      Load more*/}
            {/*    </Link>*/}
            {/*  </div>*/}
            {/*</Col>*/}
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

ProjectsOverview.propTypes = {
  projectDetail: PropTypes.any,
  match: PropTypes.object,
  onGetProjectDetail: PropTypes.func
}

const mapStateToProps = ({ projects }) => ({
  projectDetail: projects.projectDetail?.result
})

const mapDispatchToProps = dispatch => ({
  onGetProjectDetail: id => dispatch(getProjectDetail(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProjectsOverview))
