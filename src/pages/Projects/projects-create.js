import React, { useEffect, useState } from "react"
import { Link, Redirect } from "react-router-dom"
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Table,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap"
import { projects_types, projects, media, projects_detail, projects_edit } from "utils/constant"
import { GET_PROJECTS_POST } from "helpers/url_helper"
import "react-datepicker/dist/react-datepicker.css"
import { MessageToast } from "utils/message"
//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"
import { FaTimesCircle } from "react-icons/all"
import { fetcher } from "store/common/action"
import PropTypes, { object } from "prop-types"
import { createProject, getProjectsType } from "store/projects/actions"
import { connect } from "react-redux"
import UploadImage from "pages/Projects/uploadImage"
import MapsLeaflet from "../Maps/MapsLeaflet"
import DatePicker from 'react-datepicker2';
import moment from "jalali-moment"
// import moment from "moment"



const FormListInput = [
  { name: "نام پروژه", value: "title", min: 2, max: 50 },
  // { name: "تعداد ملک", value: "number" },
  // { name: "تعداد واحد در طبقه", type: "number", value: "unit_count", min: 0, max: 10000000 },
  { name: "شرکت سازنده", value: "constructor_co", min: 0, max: 100 },
  { name: "مهندس ناظر", value: "supervisor", min: 0, max: 100 },
  { name: " نام سازنده", value: "constructor", min: 0, max: 100 },
  { name: "  شرکت ناظر", value: "supervisor_co", min: 0, max: 100 },

  { name: "مهندس سازه", value: "architect", min: 0, max: 100 },
  { name: " استان", value: "province", min: 0, max: 100 },
  { name: " شهر", value: "city", min: 0, max: 100 },
  { name: "محله", value: "district", min: 0, max: 100 },
  { name: "  آدرس", value: "address", min: 0, max: 100 }
]

const FormListInputYears = [
  { name: "سال شروع" , value: "start_year", type: "string" },
  { name: "سال پایان" , value: "end_year", type: "string" },

]



const FormListInputNumber = [
  // { name: "سال شروع پروژه", type: "number", value: "start_year", min: 3, max: 5 },
  // { name: "سال اتمام پروژه", type: "number", value: "end_year", min: 3, max: 5 },
  // { name: "متراژ زیربنا", value: "project_area", type: "number", min: 0, max: 10000000 },
  { name: "تعداد طبقات", type: "number", value: "floors", min: 0, max: 10000000 },
  { name: "تعداد واحد ها", type: "number", value: "units", min: 0, max: 10000000 },

]
const FormListRange = [
  { name: "درصد پیشرفت", value: "completion_percentage", min: 0, max: 100, type: "range" },
]
const FormListSelect = [

  {
    name: "نوع سند", value: "bonds_type", children: [
      { name: "تک برگ", value: "تک برگ" },
      { name: " رسمی", value: " رسمی" }

    ]
  }
]
const FormListTextArea = [
  { name: "توضیحات", value: "description" }
]
const FormListCheckbox = [
  { name: " عدم نمایش در سایت", value: "is_archived" },
  { name: "فعالیت در سایت", value: "is_approved" }
]


const initialState = {
  "title": "",
  "province": "",
  "city": "",
  "district": "",
  "project_area": null,
  "ground_area" : null ,
  "useful_area" : null ,
  "floors": null,
  "units": null,
  "start_year": null,
  "end_year": null,
  "architect": "",
  "supervisor": "",
  "constructor": "",
  "supervisor_co": "",
  "constructor_co": "",
  "bonds_type": "",
  "description": "",
  "project_type": "",
  "medias": [],
  "completion_percentage": 0,
  // "income_type":""
}
const initialImg = {
  "title": null,
  "media_type": null,
  img: null,
  loading: false
}
const ProjectsCreate = (props) => {
  const [selectedFiles, setselectedFiles] = useState([])
  const [formData, setFormData] = useState(initialState)
  const [error, setError] = useState([])
  const [next, setNext] = useState(false)
  const [click, setClick] = useState(false)


  useEffect(() => {

    props.getProjectsType()

    if (props?.match?.params?.id) {
      fetcher(projects_detail(props?.match?.params?.id), {
        method: "GET"
      }).then(r => {
        if (r?.result) {
          let medias = []
          medias = r?.result?.medias.map(t => t?.id)
          let project_type = r?.result?.project_type.title
          setFormData({ ...r?.result, medias, project_type })
          setselectedFiles(r?.result?.medias)
        }
      }).catch(e => {
      })

    }
  }, [])


  const addData = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }
  const create = () => {
    setClick(true)
    let error = []
    error = Object.keys(formData).filter(t => !formData[t])
    !formData?.project_location?.length && (error.push("project_location"))
    error = error.filter(t => {
      let o = [
        "bonds_type",
        "project_type",
        "is_approved",
        "is_archived",
        "completion_percentage"
      ].some(p => p === t)
      return !o
    })
    setError(error)
    if (error.length) {
      console.log(error)
      MessageToast("error", "اطلاعات صحیح وارد کنید")
    } else {
      fetcher(GET_PROJECTS_POST, {
        method: "POST",
        body: JSON.stringify(formData)
      }).then(r => {
        MessageToast("success", "پروژه جدید ایجاد شد")
        setNext(true)
      }).catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })
    }
  }
  const updateProject = () => {
    setClick(true)
    let error = []
    error = Object.keys(formData).filter(t => ((!formData[t]) && initialState[t]))
    if (error.length) {

      setError(error)

    } else {

      console.log("formData?.id ---" , formData?.id);

      fetcher(projects_edit(formData?.id), {
        method: "PUT",
        body: JSON.stringify(formData)
      }).then(r => {
        // console.log(r)
        // console.log("updateProject", formData?.id)
        MessageToast("success", "پروژه آپدیت شد")
        setNext(true)
      }).catch(e => {
        MessageToast("success", "با خطا مواجه شدید")
      })
    }
  }
  if (next) {
    return <Redirect to="/projects-list" />
  }


  const limitFloat = (input) => {
    if( !!(String(input.split('.')[1]).length > 2  &&  input.split('.')[1]) != '' ){
        return false ;
    }else{
        return true ;
    }
 }


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          {
            props?.match?.params?.id ?
              <Breadcrumbs title="پروژه ها" breadcrumbItem="ویرایش پروژه " />
              :
              <Breadcrumbs title="پروژه ها" breadcrumbItem="ایجاد پروژه جدید" />
          }


          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <CardTitle className="mb-4">اطلاعات پروژه </CardTitle>
                  <Form>
                    {/*--------------------------------------------------input-------------------------------------------------*/}
                    {
                      FormListInput.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            {item?.name}
                          </Label>
                          <Col lg="10">
                            <Input
                              onWheel={e => {
                                e.target.blur()
                              }}
                              id={item?.value}
                              name={item?.value}
                              type={item?.type || "text"}
                              className="form-control"
                              placeholder={item?.name + " ..."}
                              onChange={addData}

                              defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                              value={formData?.[item?.value] ? formData?.[item?.value] : ""}
                              invalid={click && (!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.length)) || !((formData?.[item?.value]?.length) < item?.max))}
                              valid={formData?.[item?.value] && (item?.min < (formData?.[item?.value]?.length))&& ((formData?.[item?.value]?.length) < item?.max)}
                            />
                            {item?.type === "range" ? <span>{formData?.[item?.value]}%</span> : ""}
                            {/*{error.some(t => t === item?.value) && <small className="text-danger">*/}
                            {/*  <span className="pr-1">  <FaTimesCircle /></span>*/}

                            {/*  {item?.name + " را بصورت صحیح وارد کنید "}*/}
                            {/*</small>}*/}
                          </Col>
                        </FormGroup>

                        </div>
                      )
                    }

                    {/*-------------------------------------------------- Date -------------------------------------------------*/}

                        <Row>
                          <Col className="px-0" lg="2">
                            <Label
                                htmlFor="projectname"
                                className="col-form-label pl-2"
                              >
                                  شروع پروژه
                              </Label>
                          </Col>
                              <Col lg="10">
                              <Input
                              className="form-control form-control"
                              onWheel={e => {
                                e.target.blur()
                              }}
                              onChange={(e) => setFormData({
                                ...formData ,
                                start_year : e.target.value   
                                // start_year : moment(e._d).locale("en").format("YYYY-MM-DD") 
                              })}
                              defaultValue={formData?.start_year ? formData?.start_year : ''}
                              placeholder="مثال: مهرماه 1400"
                              invalid={click && !formData?.start_year}
                              valid={formData?.start_year}
                            />

                              {/* <DatePicker
                                  isGregorian={false}
                                  timePicker={false}
                                  onChange={(e) => setFormData({
                                    ...formData ,   
                                    start_year : moment(e._d).locale("en").format("YYYY-MM-DD") 
                                  })}
                                  value={moment(formData?.start_year).isValid() ? moment(formData?.start_year) : moment()}
                                  placeholder="مثال: 1368/01/20"
                                />   */}
                              </Col>
                        </Row>
                        
                        <Row className="my-4">
                          <Col className="" lg="2">
                            <Label
                                htmlFor="projectname"
                                className="col-form-label px-0"
                              >
                                  پایان پروژه
                              </Label>
                          </Col>
                              <Col lg="10">

                              <Input
                              className="form-control form-control"
                              onWheel={e => {
                                e.target.blur()
                              }}
                              onChange={(e) => setFormData({
                                ...formData ,  
                                end_year : e.target.value
                                // end_year : moment(e._d).locale("en").format("YYYY-MM-DD") 
                              })}
                              defaultValue={formData?.end_year ? formData?.end_year : ''}
                              placeholder="مثال: تیرماه 1401"
                              invalid={click && !formData?.end_year}
                              valid={formData?.end_year}

                            />

                              {/* <DatePicker
                                  isGregorian={false}
                                  timePicker={false}
                                  onChange={(e) => setFormData({
                                    ...formData ,   
                                    end_year : moment(e._d).locale("en").format("YYYY-MM-DD") 
                                  })}
                                  value={moment(formData?.end_year).isValid() ? moment(formData?.end_year) : moment()}
                                  placeholder="مثال: 1368/01/20"
                                />   */}
                              </Col>
                        </Row>


                      
                    {/*--------------------------------------------------input-------------------------------------------------*/}





                    <div >
                        <FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            متراژ
                          </Label>
                          <Col lg="10">

                       <div>
                       <Input
                        type="number"
                        className="form-control form-control"
                        placeholder="متراژ..."
                         onChange={e =>
                              setFormData(state => limitFloat(e.target.value) ? { ...formData, project_area : e.target.value } : state)
                            }
                            value={formData?.project_area  ? formData?.project_area : '' }
                            invalid={click && !formData?.project_area}
                            valid={formData?.project_area}
                          />
                       </div>

                          </Col>
                        </FormGroup>
                      </div>


                      <div >
                        <FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            متراژ زمین
                          </Label>
                          <Col lg="10">

                       <div>
                       <Input
                        type="number"
                        className="form-control form-control"
                        placeholder="متراژ زمین..."
                         onChange={e =>
                              setFormData(state => limitFloat(e.target.value) ? { ...formData, ground_area : e.target.value } : state)
                            }
                            value={formData?.ground_area  ? formData?.ground_area : '' }
                            invalid={click && !formData?.ground_area}
                            valid={formData?.ground_area}
                          />
                       </div>

                          </Col>
                        </FormGroup>
                      </div>         
                      
                      
                       <div >
                        <FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            متراژ زیربنای مفید
                          </Label>
                          <Col lg="10">

                       <div>
                       <Input
                        type="number"
                        className="form-control form-control"
                        placeholder="متراژ زیربنای مفید..."
                         onChange={e =>
                              setFormData(state => limitFloat(e.target.value) ? { ...formData, useful_area : e.target.value } : state)
                            }
                            value={formData?.useful_area  ? formData?.useful_area : '' }
                            invalid={click && !formData?.useful_area}
                            valid={formData?.useful_area}
                          />
                       </div>

                          </Col>
                        </FormGroup>
                      </div>




                    {
                      FormListInputNumber.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            {item?.name}
                          </Label>
                          <Col lg="10">
                            <Input
                              onWheel={e => {
                                e.target.blur()
                              }}
                              id={item?.value}
                              name={item?.value}
                              type="number"
                              className="form-control"
                              placeholder={item?.name + " ..."}
                              onChange={addData}
                              defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                              value={formData?.[item?.value] ? formData?.[item?.value] : ""}
                              invalid={click && (!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.toString()?.length)) || !((formData?.[item?.value]?.toString()?.length) < item?.max))}
                              valid={formData?.[item?.value] && (item?.min < (formData?.[item?.value]?.toString()?.length) ) && ((formData?.[item?.value]?.toString()?.length)< item?.max)}
                            />
                            {item?.type === "range" ? <span>{formData?.[item?.value]}%</span> : ""}
                            {/*{error.some(t => t === item?.value) && <small className="text-danger">*/}
                            {/*  <span className="pr-1">  <FaTimesCircle /></span>*/}

                            {/*  {item?.name + " را بصورت صحیح وارد کنید "}*/}
                            {/*</small>}*/}
                          </Col>
                        </FormGroup>

                        </div>
                      )
                    }
                    {/*--------------------------------------------------percent-------------------------------------------------*/}
                    {
                      FormListRange.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            {item?.name}
                          </Label>
                          <Col lg="9">
                            <Input
                              id={item?.value}
                              name={item?.value}
                              type={item?.type || "text"}
                              className="form-control"
                              placeholder={item?.name + " ..."}
                              onChange={addData}
                              // defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                              value={formData?.[item?.value] ? formData?.[item?.value] : 0}

                              invalid={click && (!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.length)) || !((formData?.[item?.value]?.length) < item?.max))}
                              valid={formData?.[item?.value] && (item?.min < (formData?.[item?.value]?.length)) && ((formData?.[item?.value]?.length) < item?.max)}

                            />
                          </Col>
                        <Col lg="1">
                          <div className="text-right pt-2"><span>{formData?.[item?.value]}%</span></div>

                        </Col>
                        </FormGroup>

                        </div>
                      )
                    }
                    <div><FormGroup className="mb-4" row>
                      <Label
                        htmlFor="project_location"
                        className="col-form-label col-lg-2"
                      >
                        موقعیت
                      </Label>
                      <Col lg="10">
                        <MapsLeaflet position={formData?.project_location}
                                     setPosition={(t) => setFormData({ ...formData, project_location: t })} />
                        {error.some(t => t === "project_location") &&
                        <small className="text-danger">
                          <span className="pr-1">  <FaTimesCircle /></span>

                          {"انتخاب موقعیت بر روی نقشه ... "}
                        </small>}
                      </Col>
                    </FormGroup>

                    </div>


                    {/*--------------------------------------------------select-------------------------------------------------*/}
                    {FormListSelect.map((item, i) => <div key={i} className="form-group row">
                      <label className="col-md-2 col-form-label">  {item?.name}</label>
                      <div className="col-md-10">
                        <select className="form-control"
                                id={item?.value}
                                onChange={addData}

                                value={formData?.[item?.value] ? formData?.[item?.value] : ""}
                                name={item?.value}>
                          <option className="text-secondary"> انتخاب {item?.name} ...</option>
                          {
                            item?.children.map((option, i) => <option value={option?.value}>{option?.name}</option>)
                          }
                        </select>
                        {error.some(t => t === item?.value) && <small className="text-danger">
                          <span className="pr-1">  <FaTimesCircle /></span>

                          {item?.name + " را بصورت صحیح انتخاب کنید "}
                        </small>}
                      </div>
                    </div>)}
                    <div className="form-group row">
                      <label className="col-md-2 col-form-label"> نوع کاربری پروژه</label>
                      <div className="col-md-10">
                        <select className="form-control"
                                onChange={addData}
                                id="project_type"
                                value={formData?.project_type}
                                name="project_type">
                          <option className="text-secondary"> انتخاب نوع کاربری پروژه ...</option>
                          {
                            props.project_types && props.project_types.map((option, i) => <option
                              value={option?.title}>{option?.title_fa}</option>)
                          }
                        </select>
                        {error.some(t => t === "project_type") && <small className="text-danger">
                          <span className="pr-1">  <FaTimesCircle /></span>

                          {"نوع کاربری پروژه" + " را بصورت صحیح وارد کنید "}
                        </small>}
                      </div>
                    </div>

                    {/*--------------------------------------------------textarea-------------------------------------------------*/}
                    {
                      FormListTextArea.map((item, i) => <FormGroup key={i} className="mb-4" row>
                          <Label
                            htmlFor="projectdesc"
                            className="col-form-label col-lg-2"
                          >
                            {item?.name}
                          </Label>
                          <Col lg="10">
                        <textarea
                          className="form-control"
                          id={item?.value}
                          name={item?.value}
                          rows="3"
                          defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                          placeholder={item?.name + " ..."}
                          onChange={addData}
                        />
                            {error.some(t => t === item?.value) && <small className="text-danger">
                              <span className="pr-1">  <FaTimesCircle /></span>

                              {item?.name + " را بصورت صحیح وارد کنید "}
                            </small>}
                          </Col>
                        </FormGroup>
                      )
                    }
                    {/*--------------------------------------------------checkbox-------------------------------------------------*/}

                    {
                      FormListCheckbox.map((item, i) =>
                        <div className="form-group row">
                          <label className="col-md-2 col-form-label"> </label>
                          <div className="col-md-10">
                            <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                              <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                                <input
                                  name={item.value}
                                  type="checkbox"
                                  className="custom-control-input"
                                  id={"customCheckcolorExtra" + (i + 2)}
                                  checked={ formData[item.value]}
                                  onChange={(e) => {
                                    setFormData({ ...formData, [item.value]: e.target.checked })
                                  }}
                                />

                                <label
                                  className="custom-control-label"
                                  htmlFor={"customCheckcolorExtra" + (i + 2)}>
                                  {item.name}
                                </label>
                              </div>


                            </FormGroup>
                          </div>
                        </div>
                      )
                    }


                  </Form>
                  {/*--------------------------------------------------upload-------------------------------------------------*/}
                  <UploadImage error={error} setError={setError} selectedFiles={selectedFiles}
                               setselectedFiles={setselectedFiles} delete_Media={(list) => {
                    setFormData({ ...formData, medias: list })
                  }}
                               setFormData={(id) => setFormData({ ...formData, medias: [...formData?.medias, id] })}
                               formData={formData}
                  />

                  {/*--------------------------------------------------submit-------------------------------------------------*/}
                  <Row className="justify-content-end">
                    <Col lg="10" className="text-right">
                      {
                        props?.match?.params?.id ?
                          <>
                            <Button type="button" color="warning" className="mr-2">
                              <Link to="/projects-list" className="px-5 mr-2 w-100 text-white">انصراف</Link>

                            </Button>
                            <Button type="button" color="primary" onClick={updateProject} className="px-5">
                              ویرایش پروژه
                            </Button>

                          </>
                          :
                          <Button type="submit" color="primary" onClick={create} className="px-5">
                            ایجاد پروژه
                          </Button>
                      }

                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}
ProjectsCreate.propTypes = {
  projects: PropTypes.array,
  onGetProjects: PropTypes.func,
  createProject: PropTypes.func,
  getProjectsType: PropTypes.func,
  project_types: PropTypes.array

}

const mapStateToProps = ({ projects }) => ({
  projects: projects.projects?.result,
  create: projects.create?.result,
  project_types: projects.project_types?.result
})

const mapDispatchToProps = dispatch => ({
  createProject: (...data) => dispatch(createProject(...data)),
  getProjectsType: (...data) => dispatch(getProjectsType(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((ProjectsCreate))

