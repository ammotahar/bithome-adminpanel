import React, { useEffect, useState } from "react"
import { Badge, Button, Col, Container, Row, Spinner, Table } from "reactstrap"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"
import { map } from "lodash"
import moment from "moment-jalaali"
//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"

//Import Card invoice
// import CardInvoice from "./card-invoice"
import { getHistoryOrder } from "store/actions"
import Status from "components/status"
import numberWithCommas from "../../components/threeNumber"
import PaginationCustom from "../../components/paginationZero"

const Filters = [
  { label: "همه سفارش ها", value: "" },
  { label: "سفارش های فعال", value: "active" },
  { label: "سفارش های در انتظار ", value: "pending" },
  { label: "سفارش های لغو شده", value: "cancel" },
  { label: "سفارش های موفق", value: "success" }

]

const Order = props => {
  const { historyOrder, getHistoryOrder,listCount } = props
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [type, setType] = useState("")
  useEffect(() => {
    getOrder(type, {page: page, size: pageSize })
  }, [])
  useEffect(() => {
    setLoading(false)
  }, [historyOrder])
  const getOrder = (type = type,query) => {
    setLoading(true)
    let filter = { query: {...query} }
    type && (filter = { query: { status_type: type ,...query} })
    getHistoryOrder(filter)
  }
  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    getOrder( type,{ page: p, size: s } )
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title=" تاریخچه" breadcrumbItem="  تاریخچه سفارش ها" />


          <Row>
            <Col lg="4">
              <label>دسته بندی براساس</label>
              <select className="form-control" onChange={(e) => {
                setType(e.target.value)
                getOrder(e.target.value,{page: 1, size: pageSize})
              }}>
                {
                  Filters.map((item, i) => <option key={i} value={item.value}>{item?.label}</option>)
                }

              </select>
            </Col>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">

                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                    <tr>
                      <th scope="col">
                        شماره
                      </th>
                      <th scope="col">نام ملک</th>
                      <th scope="col">کدملک</th>
                      <th scope="col">
                        مبلغ(تومان)
                        <small> (تومان)</small>
                      </th>
                      <th scope="col">
                        کارمزد
                        <small> (تومان)</small>
                      </th>
                      <th scope="col">
                        قیمت نهایی
                        <small> (تومان)</small>
                      </th>
                      <th scope="col">
                        مقدار سفارش
                        <small> (مترمربع)</small>
                      </th>
                      <th scope="col">نوع</th>
                      <th scope="col">تاریخ</th>
                      <th scope="col"> وضعیت</th>

                      {/*<th scope="col" className="text-right">عملیات</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {map(historyOrder, (project, index) => (
                      <tr key={index}>
                        <td>
                        {page == 0 ?  ++index : (page * pageSize) + ++index }
                        </td>
                        <td>
                          {project.title}

                        </td>
                        <td>
                          {project.estate_identifier}

                        </td>

                        <td>{numberWithCommas(project?.amount)}</td>
                        <td>{numberWithCommas(project?.commission_price)}</td>
                        <td>{numberWithCommas(project?.final_price)}</td>
                        <td>{numberWithCommas(project?.current_area)}</td>
                        <td>
                          {project?.order_title ==="estate_sell" && "فروش"}
                          {project?.order_title ==="estate_buy" && "خرید"}
                        </td>
                        <td>{moment(project?.created_at, "YYYY-MM-DDThh:mm:ss").format("jYYYY/jMM/jDD hh:mm")}</td>
                        <td>

                          <Status name={project?.status} />

                          {/*{console.log(project?.id)}*/}
                        </td>

                      </tr>
                    ))}
                    </tbody>
                  </Table>

                </div>
                {
                  loading ? <div className="text-center"><Spinner/></div> : ""
                }
              </div>
            </Col>
          </Row>
          <Row>
            {/*<Col xs="12">*/}
            {/*  <div className="text-center my-3">*/}
            {/*    <Link to="#" className="text-success">*/}
            {/*      <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />*/}
            {/*      Load more*/}
            {/*    </Link>*/}
            {/*  </div>*/}
            {/*</Col>*/}
          </Row>
        </Container>
        <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor((listCount) / pageSize ) + 1}
                          addPageSize={setPageSize}
                          pageSize={pageSize}
                          chatsCount={listCount} />
      </div>
    </React.Fragment>
  )
}

Order.propTypes = {
  historyOrder: PropTypes.array,
  getHistoryOrder: PropTypes.func
}

const mapStateToProps = ({ history }) => ({
  historyOrder: history.historyOrder?.result?.items || [],
  listCount: history.historyOrder?.result?.total || 0,
})

const mapDispatchToProps = dispatch => ({
  getHistoryOrder: (...data) => dispatch(getHistoryOrder(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Order))
