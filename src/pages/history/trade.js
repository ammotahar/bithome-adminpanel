import React, { useEffect, useState } from "react"
import { Badge, Button, Col, Container, Row, Table } from "reactstrap"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"
import { map } from "lodash"
import moment from "moment-jalaali"
//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"

//Import Card invoice
// import CardInvoice from "./card-invoice"
import { getHistoryTrade } from "store/actions"
import Status from "components/status"
import numberWithCommas from "../../components/threeNumber"
import PaginationCustom from "../../components/paginationZero"

const Trade = props => {
  const { historyTrade, getHistoryTrade ,listCount} = props
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)

  useEffect(() => {
    getHistoryTrade({ query: {page: page, size: pageSize } })
  }, [])

  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    getHistoryTrade({ query: { page: p, size: s } })
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title=" تاریخچه" breadcrumbItem="  تاریخچه معاملات" />

          <Row>
            {/*{map(invoices, (invoice, key) => (*/}
            {/*  <CardInvoice data={invoice} key={"_invoice_" + key} />*/}
            {/*))}*/}
          </Row>
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                    <tr>
                      <th scope="col">
                        شماره
                      </th>
                      <th scope="col">کد ملک</th>
                      <th scope="col">خریدار</th>
                      <th scope="col">فروشنده</th>
                      <th scope="col">مقدار سفارش(مترمربع)</th>
                      <th scope="col">  مبلغ(تومان)</th>
                      {/*<th scope="col"> شماره حساب</th>*/}
                      <th scope="col">  کارمزد خریدار(تومان)</th>
                      <th scope="col">  کارمزد فروشنده(تومان)</th>


                      <th scope="col">تاریخ</th>
                      {/*<th scope="col">کدپیگیری</th>*/}

                      {/*<th scope="col">توضیحات</th>*/}
                      <th scope="col"> وضعیت</th>

                      {/*<th scope="col" className="text-right">عملیات</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {map(historyTrade, (project, index) => (
                      <tr key={index}>
                        <td>
                        {page == 0 ?  ++index : (page * pageSize) + ++index }
                        </td>
                        <td>  {project?.estate_code}</td>

                        <td>
                          {project.buyer}

                        </td>
                        <td>
                          {project.seller}

                        </td>
                        <td>{project?.area}</td>
                        <td>{numberWithCommas(project?.amount)}</td>
                        {/*<td >{project?.user_bank_account}</td>*/}
                        <td >{project?.buyer_commission}</td>
                        <td >{project?.seller_commission}</td>

                           <td>{moment(project?.created_at, "YYYY-MM-DDThh:mm:ss").format("jYYYY/jMM/jDD hh:mm")}</td>

                        {/*<td>  {project?.tracking_code && project?.tracking_code.slice(project?.tracking_code.lastIndexOf('-')+1)}</td>*/}

                        {/*<td>{project?.description}</td>*/}

                        <td>

                          <Status name={project?.status}/>

                          {/*{console.log(project?.id)}*/}
                        </td>

                      </tr>
                    ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            {/*<Col xs="12">*/}
            {/*  <div className="text-center my-3">*/}
            {/*    <Link to="#" className="text-success">*/}
            {/*      <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />*/}
            {/*      Load more*/}
            {/*    </Link>*/}
            {/*  </div>*/}
            {/*</Col>*/}
          </Row>
        </Container>
        <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor((listCount) / pageSize ) + 1}
                          addPageSize={setPageSize}
                          pageSize={pageSize}
                          chatsCount={listCount} />
      </div>
    </React.Fragment>
  )
}

Trade.propTypes = {
  historyTrade: PropTypes.array,
  getHistoryTrade: PropTypes.func,
}

const mapStateToProps = ({ history }) => ({
  historyTrade: history.historyTrade?.result?.items || [],
  listCount: history.historyTrade?.result?.total || 0,
})

const mapDispatchToProps = dispatch => ({
  getHistoryTrade: (...data) => dispatch(getHistoryTrade(...data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Trade))
