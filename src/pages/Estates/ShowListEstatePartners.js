import React, {useState} from 'react';
import {Button, Row , Col ,Table ,  Modal, ModalHeader, ModalBody, ModalFooter , Container} from 'reactstrap';

function ShowListEstatePartners(props) {

    const {buttonLabel, className} = props;
    const [closeAll,setCloseAll] = useState(true);
    const toggle = () => props?.setModalShowListPartners(false);

    return (
        <React.Fragment>
            {/* <Button color="danger" onClick={toggle}>{buttonLabel}</Button> */}
            <Modal isOpen={props?.modalShowListPartners} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>لیست شرکا</ModalHeader>
                <ModalBody  >
                <Container  fluid>



          <Row >
            <Col md="12">
              <div style={{overflowX : 'auto'}}  className="">
                <div  className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        ردیف
                      </th>
                      <th scope="col"> نام و نام خانوادگی</th>
     
                      <th scope="col"> سهم (متر مربع)</th>
            
           
                    </tr>
                    </thead>
                    <tbody>
                {props.partnersList.map((partner, index) => (
                      <>
                        <tr key={index}>
                            <td className="text-center">
                                {index + 1}
                            </td>
                            {/* <td style={{ minWidth: "200px" }}>
                            <h5 className="text-truncate font-size-16">
                                {partner}
                            </h5>
                        
                            </td> */}
            
                            <td>
                                {partner?.full_name}
                            </td>
                            <td className="text-center">
                                {partner?.square_meter}
                            </td>
                      </tr>
                      
                      </>
                    ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          {/* <PaginationCustom handlePageClick={handlePageClick} page={page}
                            totalPage={Math.floor(counts / pageSize) + 1}
                            addPageSize={setPageSize}
                            pageSize={pageSize}
                            chatsCount={counts} /> */}


        </Container>
                </ModalBody>
                <ModalFooter>
                    {/* <Button color="primary" onClick={toggle}>Do Something</Button>{' '} */}
                    <Button color="primary" onClick={toggle}>بستن</Button>
                </ModalFooter>
            </Modal>
        </React.Fragment>
    )
}

export default ShowListEstatePartners;
