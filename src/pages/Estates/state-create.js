import React, { useEffect, useState } from "react"
import { Link, Redirect } from "react-router-dom"
import Dropzone from "react-dropzone"
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Table,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap"
import Select from "react-select"
//Import Date Picker
// import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"

//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"
import { FaInfo, FaInfoCircle, FaTimesCircle, FaTrash } from "react-icons/all"
import PropTypes from "prop-types"
import {
  getEstatesType,
  getEstatesUsers
} from "store/estates/actions"
import { connect } from "react-redux"
import { GET_States_POST, GET_States_DETAIL, GET_REPORT_CONTACTS } from "helpers/url_helper"
import MapsLeaflet from "../Maps/MapsLeaflet"
import UploadImage from "../Projects/uploadImage"
import { MessageToast } from "../../utils/message"
import { projects, projects_detail } from "../../utils/constant"
import { getProjects } from "store/projects/actions"
import { fetcher } from "store/common/action"
import AddDetail from "pages/Estates/addDetail"
import AssignThePropertyToTheOwner from "./AssignThePropertyToTheOwner"

const FormListInput = [
  { name: "نام ملک", value: "title", min: 2, max: 50 },
  { name: " سرویس سرمایشی", value: "cooling_system", min: 0, max: 10000000 },
  { name: " سرویس گرمایشی", value: "heating_system", min: 0, max: 10000000 },
  // { name: "سرویس بهداشتی", value: "bathroom",min:0,max:10000000 },
  { name: "استان", value: "province", min: 0, max: 50 },
  { name: "شهر", value: "city", min: 0, max: 50 },
  { name: "محله", value: "district", min: 0, max: 50 },
  { name: "آدرس", value: "address", min: 0, max: 50 }
]
const FormListInputNumber = [
  { name: "سال ساخت", value: "build_year", type: "number", min: 3, max: 5 },
  // { name: "متراژ", value: "area", type: "number", min: 0, max: 10000000 },
  // { name: " طبقه", value: "floor", type: "number", min: 0, max: 10000000 },
  { name: " واحد", value: "unit", type: "number", min: 0, max: 10000000 },
  { name: " تعداد اتاق ها", value: "room_count", type: "number", min: 0, max: 10000000 },
  { name: "  طبقه ", value: "floor", type: "number", min: 0, max: 10000000 },
  { name: " تعداد  طبقات", value: "floor_count", type: "number", min: 0, max: 10000000 },
  { name: "  تعداد واحد در  طبقه", value: "unit_count", type: "number", min: 0, max: 10000000 },
  { name: "  قیمت", value: "price_range", type: "number", min: 0, max: 1000000000 },
  { name: "پارکینگ", value: "parking", type: "number", min: 0, max: 10000000 },
  { name: " تعدادسرویس بهداشتی", value: "bathroom_count", type: "number", min: 0, max: 10000000 }
]
const FormListSelect = [
  {
    name: "سرویس بهداشتی", value: "bathroom", children: [
      { name: "ایرانی", value: "ایرانی" },
      { name: "فرنگی", value: "فرنگی" },
      { name: "ایرانی و فرنگی", value: "ایرانی و فرنگی" }
    ]
  },
  {
    name: "نوع درآمد", value: "income_type", children: [
      { name: "اجاره ", value: "اجاره " },
      { name: " سرمایه گذاری در ساخت", value: " سرمایه گذاری در ساخت" },
      { name: " سرمایه گذاری در بازسازی", value: " سرمایه گذاری در بازسازی" },
      { name: "Time Sharing", value: "Time Sharing" }
    ]
  }
]
const FormListCheckbox = [
  { name: "آسانسور", value: "have_elevator" },
  { name: "انبار", value: "have_storeroom" },
  { name: " عدم نمایش در سایت", value: "is_archived" },
  { name: "فعالیت در سایت", value: "is_approved" }
]

const FormListTextArea = [
  { name: "توضیحات", value: "description" }
]

const initialState = {
  "details": {
    // "انبار" : "ندارد",
    // "استخر" : "دارد"
  },

  "estate_type": "",
  "estate_location": [],
  "province": "",
  "city": "",
  "district": "",
  "area": null,
  "max_area_sell": null,
  "build_year": null,
  "parking": null,
  "bathroom": "",
  "floor": 0,
  "unit": null,
  "room_count": null,
  "floor_count": null,
  "unit_count": null,
  "owner":[],
  "have_elevator": false,
  "have_storeroom": false,

  "cooling_system": "",
  "heating_system": "",
  "description": "",
  "address": "",
  "price_range": "",

  "project_id": null,
  "default_owner_id": null,
  "user_id": null,

  "medias": [],
  "income_type": ""
}


const StateCreate = (props) => {
  // const [startDate, setstartDate] = useState(new Date())
  // const [endDate, setendDate] = useState(new Date())
  const [selectedFiles, setselectedFiles] = useState([])
  const [formData, setFormData] = useState(initialState)
  const [error, setError] = useState([])
  const [next, setNext] = useState(false)
  const [click, setClick] = useState(false)
  const [projects, setProjects] = useState([])
  const [users, setUsers] = useState([{ label: "محمدرضا تهوری", value: 1 }, { label: "جعفر رهبر مدنی", value: 28 } , { label: "محمدعلی احمدی", value: 332 }])
  const [selectedProject, setSelectedProject] = useState({})
  const [selectedUser, setSelectedUser] = useState({})
  const [owner, setOwner] = useState(false)
  const [inputList , setInputList] = useState([]);
  const [pageSize, setPageSize] = useState(50)
  const [params, setParams] = useState({
    page : 0,
  })

  useEffect(() => {
    props.getEstatesType()
    props.getProjects()
    props.getEstatesUsers()
    if (props?.match?.params?.id) {
      // props.getStateDetail(props?.match?.params?.id)
      fetcher(GET_States_DETAIL(props?.match?.params?.id), {
        method: "GET"
      }).then(r => {

        if (r?.result) {

          let medias = []
          medias = r?.result?.medias.map(t => t?.id)
          let estate_type = r?.result?.estate_type.title
          let project_id = r?.result?.project?.id
          let user_id = r?.result?.user?.id
          setOwner(r?.result?.default_owner_id ? true : false)
          setFormData({ ...r?.result, medias, estate_type, project_id, user_id })
          setselectedFiles(r?.result?.medias)
          if (r?.result?.project)
            setSelectedProject({ label: r?.result?.project?.title, value: r?.result?.project?.id })
          if (r?.result?.user)
            setSelectedUser({
              label: (r?.result?.user?.credentials?.first_name + " " + r?.result?.user?.credentials?.last_name),
              value: r?.result?.user?.id
            })
        }
      }).catch(e => {
      })
    }
  }, [])
  
  useEffect(() => {
    let list = []
    props.projects?.items?.length &&  (list =props.projects.items.map(i => ({ label: i.title, value: i.id, ...i })))
    setProjects(list)
  }, [props.projects])
  // useEffect(() => {
  //   let list = []
  //   if (props.estates_users?.length)
  //     list = props.estates_users.map(i => ({
  //       label: ((i?.credentials?.first_name || "") + " " + (i?.credentials?.last_name || "")),
  //       value: i.id, ...i
  //     }))
  //   setUsers(list)
  // }, [props.estates_users])


  useEffect(() => {
    getContactReports()
  }, [params])


  const [usersToOwner, setUsersToOwner] = useState([])

    // get list contact report
    const getContactReports = (p = 0, s = pageSize) => {
      fetcher(GET_REPORT_CONTACTS, {
        method: "GET"
      // },{query:{page: p, size: s, owner_id : props?.match?.params?.id ,  ...params}}).then(r => {
      },{query:{page: p, size: s ,  ...params}}).then(r => {
      if (r?.result) {
        setUsersToOwner(r?.result?.items)
        setCounts(r?.result?.total)
      }
    }).catch(e => {
      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }



  const limitFloat = (input) => {
    if( !!(String(input.split('.')[1]).length > 2  &&  input.split('.')[1]) != '' ){
        return false ;
    }else{
        return true ;
    }
 }

  const addData = e => {
    if (e.target.value)
      setFormData({ ...formData, [e.target.name]: e.target.value })
    else
      setFormData({ ...formData, [e.target.name]: "" })
  }

  const create = (method = "POST", id = "") => {
    setClick(true)
    let error = []
    error = Object.keys(formData).filter(t => ((!formData[t])))
    !formData?.estate_location?.length && (error.push("project_location"))
    error = error.filter(t => {
      let o = [
        "have_elevator",
        "have_storeroom",
        "project_id",
        "default_owner_id",
        "is_approved",
        "project",
        "updated_at",
        "is_archived",
        "is_auction",
        "floor", 
        'estate_last_trade',
         'user_id'

      ].some(p => p === t)
      return !o
    })
    setError(error)
    if(!formData?.price_range && formData?.default_owner_id){
      MessageToast("error", "برای ثبت سفارش قیمت  نباید صفر باشد")
    }
    else if (error.length) {
      console.log("error.length --->>>" , error);
      MessageToast("error", "اطلاعات صحیح وارد کنید")
    } else {
      fetcher(GET_States_POST + (id ? (id + "/") : ""), {
        method,
        body: JSON.stringify({...formData , owner : inputList})
      }).then(r => {
        if (method === "POST") {
          MessageToast("success", "ملک جدید ایجاد شد")
        } else {
          MessageToast("success", "ملک ویرایش شد")
        }
        setNext(true)
      }).catch(e => {
        e.then(res => {
          MessageToast("error", Array.isArray(res.result) ?  "مجموع متراژ اختصاص داده شده از متراژ ملک بیشتر است" : "با خطا مواجه شدید")
        })
      })
    }
  }

  if (next) {
    return <Redirect to="/estate-list" />
  }

  // console.log(formData, formData?.bathroom?.length)
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          {
            props?.match?.params?.id ?
              <Breadcrumbs title=" املاک" breadcrumbItem="ویرایش ملک " />
              :
              <Breadcrumbs title=" املاک" breadcrumbItem="ثبت ملک جدید" />
          }

          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <CardTitle className="mb-4">اطلاعات ملک </CardTitle>
                  <Form>
                    {/*--------------------------------------------------input-------------------------------------------------*/}
                    {
                      FormListInput.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            {item?.name}
                          </Label>
                          <Col lg="10">
                            <Input
                              onWheel={e => {
                                e.target.blur()
                              }}
                              id={item?.value}
                              name={item?.value}
                              type="text"
                              className="form-control"
                              placeholder={item?.name + " ..."}
                              onChange={addData}
                              defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                              invalid={click && (!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.length)) || !((formData?.[item?.value]?.length) < item?.max))}
                              valid={formData?.[item?.value] && (item?.min < (formData?.[item?.value]?.length)) && (item?.max > (formData?.[item?.value]?.length))}
                            />
                            {/*{console.log(item?.value,(!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.length)) || !((formData?.[item?.value]?.length) < item?.max)),*/}
                            {/*  !formData?.[item?.value],!(item?.min < (formData?.[item?.value]?.length)),item?.min,formData?.area?.length,!((formData?.[item?.value]?.length) < item?.max) )}*/}
                            {/*/!*{error.some(t => t === item?.value) && <small className="text-danger">*!/*/}
                            {/*  <span className="pr-1">  <FaTimesCircle /></span>*/}

                            {/*  {item?.name + " را بصورت صحیح وارد کنید "}*/}
                            {/*</small>}*/}
                          </Col>
                        </FormGroup>

                        </div>
                      )
                    }
                    {/*--------------------------------------------------inputNumber-------------------------------------------------*/}

                      <div >
                        <FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            متراژ
                          </Label>
                          <Col lg="10">

                       <div>
                       <input
                        type="number"
                        className="form-control form-control"
                        placeholder="متراژ..."
                         onChange={e =>
                              setFormData(state => limitFloat(e.target.value) ? { ...formData, area : e.target.value } : state)
                            }
                            value={formData?.area  ? formData?.area : '' }
                      
                            />
                       </div>

                          </Col>
                        </FormGroup>
                      </div>

                      <div >
                        <FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            حداکثر متراژ فروش
                          </Label>
                          <Col lg="10">

                       <div>
                       <input
                        type="number"
                        className="form-control form-control"
                        placeholder="حداکثر متراژ فروش..."
                         onChange={e =>
                              setFormData(state => limitFloat(e.target.value) ? { ...formData, max_area_sell : e.target.value } : state)
                            }
                            value={formData?.max_area_sell  ? formData?.max_area_sell : '' }
        
                            />
                       </div>

                          </Col>
                        </FormGroup>
                      </div>


                    {
                      FormListInputNumber.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                          <Label
                            htmlFor="projectname"
                            className="col-form-label col-lg-2"
                          >
                            {item?.name}
                          </Label>
                          <Col lg="10">
                            <Input
                              onWheel={e => {
                                e.target.blur()
                              }}
                              id={item?.value}
                              name={item?.value}
                              type="number"
                              className="form-control"
                              placeholder={item?.name + " ..."}
                              onChange={addData}
                              defaultValue={(formData?.[item?.value] || (formData?.[item?.value] === 0) ) ? formData?.[item?.value] : ""}
                              invalid={click && (!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.toString()?.length)) || !((formData?.[item?.value]?.toString()?.length) < item?.max))}
                              valid={formData?.[item?.value] && (item?.min < (formData?.[item?.value]?.toString()?.length)) && (item?.max > (formData?.[item?.value]?.toString()?.length))}
                            />
                            {/*{error.some(t => t === item?.value) && <small className="text-danger">*/}
                            {/*  <span className="pr-1">  <FaTimesCircle /></span>*/}

                            {/*  {item?.name + " را بصورت صحیح وارد کنید "}*/}
                            {/*</small>}*/}
                          </Col>
                        </FormGroup>

                        </div>
                      )
                    }
                    <div><FormGroup className="mb-4" row>
                      <Label
                        htmlFor="project_location"
                        className="col-form-label col-lg-2"
                      >
                        موقعیت
                      </Label>
                      <Col lg="10">
                        <MapsLeaflet
                          position={formData?.estate_location}
                          setPosition={(t) => setFormData({ ...formData, estate_location: t })} />
                        {error.some(t => t === "estate_location") &&
                        <small className="text-danger">
                          <span className="pr-1">  <FaTimesCircle /></span>

                          {"انتخاب موقعیت بر روی نقشه ... "}
                        </small>}
                      </Col>
                    </FormGroup>

                    </div>

                    {/*--------------------------------------------------select-------------------------------------------------*/}
                    {/*---------users*/}
                    {/* <div className="form-group row">
                      <label className="col-md-2 col-form-label"> صاحب ملک</label>
                      <div className="col-md-10">
                        <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                          <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                            <input
                              disabled={props?.match?.params?.id}
                              type="checkbox"
                              className="custom-control-input"
                              id="customCheckcolor1"
                              checked={owner}

                              onChange={(e) => {
                                setOwner(e.target.checked)
                                if (e.target.checked) {
                                  formData?.user_id && setFormData({ ...formData, default_owner_id: formData?.user_id })
                                } else {
                                  setFormData({ ...formData, default_owner_id: null })
                                }

                              }}
                            />

                            <label
                              className="custom-control-label"
                              htmlFor="customCheckcolor1"
                            >
                              امکان معامله تنها با صاحب ملک می باشد
                            </label>
                          </div>
                          <Select
                            isDisabled={props?.match?.params?.id}
                            value={selectedUser}
                            isMulti={false}
                            name="user_id"
                            id="user_id"
                            onChange={(t) => {
                              setSelectedUser(t)
                              // console.log(t)
                              setFormData({ ...formData, user_id: t.value })
                              // owner ? setFormData({
                              //   ...formData,
                              //   user_id: t.value,
                              //   default_owner_id: t.id
                              // }) : setFormData({
                              //   ...formData,
                              //   user_id: t.value,
                              //   default_owner_id: null
                              // })
                               setFormData({
                                ...formData,
                                user_id: t.value,
                                default_owner_id: t.value
                              })
                            }}
                            options={users}
                            classNamePrefix="select2-selection"
                            isLoading={false}
                            // placeholder="انتخاب کاربر ..."
                          />

                        </FormGroup>
                      </div>
                    </div> */}



                    <div className="form-group row">
                      <label className="col-md-2 col-form-label"> صاحب ملک</label>
                      <div className="col-md-10 px-0 px-md-2">
                        <AssignThePropertyToTheOwner 
                          getContactReports={getContactReports}
                          usersToOwner={usersToOwner}
                          setParams={setParams}
                          params={params}
                          setInputList={setInputList}
                          inputList={inputList}
                          formData={formData} 
                          setFormData={setFormData} 
                        />
                        
                      </div>
                    </div>


















                    {/*---------all*/}
                    {FormListSelect.map((item, i) => <div className="form-group row">
                      <label className="col-md-2 col-form-label">  {item?.name}</label>
                      <div className="col-md-10">
                        <select className="form-control"
                                value={formData?.[item?.value]}
                                onChange={(e) => setFormData({ ...formData, [item?.value]: e.target.value })}
                                id={item?.value}
                                name={item?.value}>
                          <option className="text-secondary"> انتخاب {item?.name} ...</option>
                          {
                            item?.children.map((option, i) => <option value={option?.value}>{option?.name}</option>)
                          }
                        </select>
                        {error.some(t => t === item?.value) && <small className="text-danger">
                          <span className="pr-1">  <FaTimesCircle /></span>

                          {item?.name + " را بصورت صحیح وارد کنید "}
                        </small>}
                      </div>
                    </div>)}
                    {/*---------type*/}
                    <div className="form-group row">
                      <label className="col-md-2 col-form-label"> نوع کاربری ملک</label>
                      <div className="col-md-10">
                        <select className="form-control"
                                onChange={addData}
                                id="estate_type"

                                value={formData?.estate_type}
                                name="estate_type">
                          <option className="text-secondary"> انتخاب نوع کاربری ملک ...</option>
                          {
                            props?.estates_type?.length && props?.estates_type.map((option, i) => <option
                              value={option?.title}>{option?.title_fa}</option>)
                          }
                        </select>
                        {error.some(t => t === "estate_type") && <small className="text-danger">
                          <span className="pr-1">  <FaTimesCircle /></span>

                          {"نوع کاربری ملک" + " را بصورت صحیح وارد کنید "}
                        </small>}
                      </div>
                    </div>
                    {/*---------project*/}
                    <div className="form-group row">
                      <label className="col-md-2 col-form-label"> پروژه</label>
                      <div className="col-md-10">
                        <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                          <Select
                            value={selectedProject}
                            isMulti={false}
                            name="project_id"
                            id="project_id"
                            onChange={(t) => {
                              setSelectedProject(t)
                              setFormData({ ...formData, project_id: t.id })
                            }}
                            options={projects}
                            classNamePrefix="select2-selection"
                            isLoading={false}
                            placeholder="انتخاب پروژه ..."
                          />
                        </FormGroup>

                      </div>
                    </div>


                    {/*--------------------------------------------------textarea-------------------------------------------------*/}
                    {
                      FormListTextArea.map((item, i) => <FormGroup key={i} className="mb-4" row>
                          <Label
                            htmlFor="projectdesc"
                            className="col-form-label col-lg-2"
                          >
                            {item?.name}
                          </Label>
                          <Col lg="10">
                        <textarea
                          className="form-control"
                          id={item?.value}
                          name={item?.value}
                          rows="3"
                          defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                          placeholder={item?.name + " ..."}
                          onChange={addData}

                        />
                            {error.some(t => t === item?.value) && <small className="text-danger">
                              <span className="pr-1">  <FaTimesCircle /></span>

                              {item?.name + " را بصورت صحیح وارد کنید "}
                            </small>}
                          </Col>
                        </FormGroup>
                      )
                    }

                    {/*--------------------------------------------------detail-------------------------------------------------*/}
                    <AddDetail formData={formData} setFormData={setFormData} />

                    {/*--------------------------------------------------extra-------------------------------------------------*/}
                    <CardTitle className="mb-4">موارد بیشتر </CardTitle>
                    {
                      FormListCheckbox.map((item, i) =>
                        <div className="form-group row">
                          <label className="col-md-2 col-form-label"> </label>
                          <div className="col-md-10">
                            <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                              <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                                <input
                                  name={item.value}
                                  type="checkbox"
                                  className="custom-control-input"
                                  id={"customCheckcolorExtra" + (i + 2)}
                                  checked={formData[item.value]}
                                  onChange={(e) => {
                                    setFormData({ ...formData, [item.value]: e.target.checked })
                                  }}
                                />

                                <label
                                  className="custom-control-label"
                                  htmlFor={"customCheckcolorExtra" + (i + 2)}>
                                  {item.name}
                                </label>
                              </div>


                            </FormGroup>
                          </div>
                        </div>
                      )
                    }
                  </Form>


                  {/*--------------------------------------------------upload-------------------------------------------------*/}


                  <UploadImage error={error} setError={setError} selectedFiles={selectedFiles}
                               setselectedFiles={setselectedFiles} delete_Media={(list) => {
                    setFormData({ ...formData, medias: list })
                  }}
                               setFormData={(id) => setFormData({ ...formData, medias: [...formData?.medias, id] })}
                               formData={formData}
                  />
                  {/*--------------------------------------------------submit-------------------------------------------------*/}
                  <div className="text-right">

                    {
                      props?.match?.params?.id ?
                        <>
                          <Button type="button" color="warning" className="mr-2">
                            <Link to="/estate-list" className="px-5 mr-2 w-100 text-white">انصراف</Link>

                          </Button>
                          <Button type="button" color="primary" onClick={() => create("PUT", props?.match?.params?.id)}
                                  className="px-5">
                            ویرایش ملک
                          </Button>

                        </>
                        :
                        <Button type="submit" color="primary" onClick={() => create("POST")} className="px-5">
                          ثبت ملک
                        </Button>
                    }

                  </div>

                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}
StateCreate.propTypes = {
  estates_type: PropTypes.array,
  projects: PropTypes.array
}

const mapStateToProps = ({ Estate, projects }) => {
  return {
    projects: projects.projects?.result || [],
    estates_type: Estate.estates_type?.result || [],
    create: Estate.create,
    StateDetail: Estate.StateDetail,
    estates_users: Estate.estates_users?.result || []

  }
}

const mapDispatchToProps = dispatch => ({
  getEstatesType: (...data) => dispatch(getEstatesType(...data)),
  getProjects: (...data) => dispatch(getProjects(...data)),
  getEstatesUsers: (...data) => dispatch(getEstatesUsers(...data))

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((StateCreate))

