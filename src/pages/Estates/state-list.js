import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, NavLink, withRouter } from "react-router-dom"
import { map } from "lodash"
import {
  Badge,
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
  Table,
  Dropdown,
  UncontrolledDropdown,
  UncontrolledTooltip, Modal, Button, FormGroup
} from "reactstrap"
import PaginationCustom from "../../components/paginationZero"
//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"

//Import Image
// import images from "assets/images"
// import companies from "assets/images/companies"
import moment from "moment-jalaali"
import { fetcher, getStates } from "store/actions"
import { BiDotsVertical, FaCommentDots, FaPlusCircle, FaRegDotCircle, FaUserAlt } from "react-icons/all"
import { GET_COUNT_ESTATES, GET_States_DELETE , GET_LIST_ESTATE_PARTNERS } from "helpers/url_helper"
import { MessageToast } from "utils/message"
import Filter from "./filter"
import ShowListEstatePartners from './ShowListEstatePartners'


const Action = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false)
  const [modal, setModal] = useState(false)



  return <div dir="ltr" className="text-right ">
    <Button color="danger" onClick={() => setModal(true)}>
      حذف
    </Button>
    {/*  <Dropdown isOpen={dropdownOpen}*/}
    {/*                                                        toggle={() => setDropdownOpen(!dropdownOpen)}>*/}
    {/*  <DropdownToggle caret className="btn-light">*/}
    {/*    <span className="text-dark"> <BiDotsVertical /></span>*/}

    {/*  </DropdownToggle>*/}
    {/*  <DropdownMenu right>*/}

    {/*    /!*<DropdownItem href="#" className="text-info">*!/*/}
    {/*    /!*  <Link to="/projects-overview">جزییات</Link>*!/*/}

    {/*    /!*</DropdownItem>*!/*/}
    {/*    <DropdownItem href="#" className="text-warning">*/}
    {/*      <NavLink to={`/state-create/${props?.id}`} className="d-block w-100">ویرایش</NavLink>*/}
    {/*    </DropdownItem>*/}
    {/*    <DropdownItem href="#" className="text-danger" onClick={() => setModal(true)}>*/}
    {/*      حذف*/}
    {/*    </DropdownItem>*/}


    {/*  </DropdownMenu>*/}
    {/*</Dropdown>*/}
    <Modal centered={true} isOpen={modal} toggle={() => setModal(false)}>
      <div className="p-4">
        <p className="pb-4">آیا از حذف این پروژه اطمینان دارید؟</p>
        <div className="row">

          <div className="col-6">
            <Button type="button" color="danger" className="w-100"
                    onClick={() => setModal(false)}>خیر</Button>
          </div>

          <div className="col-6">
            <Button type="button" color="primary" className="w-100"
                    onClick={() => {
                      props.deleteEstate()
                      setModal(false)
                    }}>بله</Button>
          </div>
        </div>
      </div>
    </Modal>
  </div>
}
const StateList = props => {
  const { projects, onGetProjects ,listCount} = props
  const [params, setParams] = useState({})
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  
  const [modalShowListPartners , setModalShowListPartners] = useState(false);
  const [partnersList, setPartnersList] = useState([])

  useEffect(() => {
    // console.log(props)
    setFilter()
    // getCount()
  }, [])
// console.log(props)
  const deleteEstate = (id) => {
    fetcher(GET_States_DELETE(id), {
      method: "DELETE"
    }).then(r => {
      onGetProjects()
      MessageToast("success", "ملک حذف شد")
      // setModal(false)
    }).catch(e => {
      MessageToast("error", "باخطا مواجه شدید")
    })
  }
  // const getCount = (id) => {
  //   fetcher(GET_COUNT_ESTATES, {
  //     method: "GET"
  //   }).then(r => {
  //     if(r?.result)
  //       setCounts(r?.result.estates_count)
  //   }).catch(e => {
  //     // MessageToast("error", "باخطا مواجه شدید")
  //   })
  // }
  const setFilter = (p = 0, s = pageSize) => {
    onGetProjects({  query:{ page: p, size: s ,...params }  })
  }
  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    setFilter(p, s)
  }


    const handleGetListPartners = (id)=> {
      fetcher(GET_LIST_ESTATE_PARTNERS(id), {
        method: "GET"
      }).then(r => {
        if(r?.result)
        setPartnersList(r?.result)
      }).catch(e => {
        // MessageToast("error", "باخطا مواجه شدید")
      })

      setTimeout(() => {
        setModalShowListPartners(true)
      }, 1000);
    }


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="پروژه‌ها" breadcrumbItem="لیست ملک‌ها" />
          <div className="text-right">
            <Link to="/state-create" className="btn btn-info">
              <span className="pr-2"><FaPlusCircle/></span>
              ایجاد ملک جدید </Link>
          </div>
          <Filter params={params} setParams={setParams} setFilter={setFilter} />
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive ">
                  <Table
                    className="project-list-table table-nowrap table-centered table-borderless table-striped table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        شماره
                      </th>
                      <th scope="col" > نام ملک</th>
                      <th scope="col" className="text-center"> نوع ملک</th>
                      <th scope="col" className="text-center"> تعداد شرکا</th>
                      <th scope="col" className="text-center">تاریخ</th>
                      <th scope="col" className="text-center">نمایش</th>
                      <th scope="col" className="text-center">فعالیت</th>
                      <th scope="col" className="text-center">موقعیت پروژه</th>
                      <th scope="col" className="text-center">پروژه ملک</th>
                      <th scope="col" className="text-center">شرکا</th>
                      <th scope="col" className="text-center"> ویرایش</th>
                      {/* <th scope="col" className="text-right">عملیات</th> */}
                    </tr>
                    </thead>
                    <tbody>
                    {map(projects, (project, index) => (
                        <>
                          <tr key={index}>
                            <td className="text-center">
                              {page == 0 ?  ++index : (page * pageSize) + ++index }
                              {/*<img*/}
                              {/*  src={companies[project.img]}*/}
                              {/*  alt=""*/}
                              {/*  className="avatar-sm"*/}
                              {/*/>*/}
                              {/*<span className="avatar-sm"><FaUserAlt/></span>*/}
                            </td>

                            <td style={{ minWidth: "200px" }}>
                              <h5 className="text-truncate font-size-14">
                                {project.title}
                              </h5>
                              {/*<p className="text-muted mb-0 text-wrap">*/}
                              {/*  {project.description}*/}
                              {/*</p>*/}
                            </td>
                            <td>{project?.estate_type?.title_fa}</td>
                            <td className="text-center">{project?.num_partners}</td>
                            <td>{moment(project?.created_at, "YYYY-MM-DDThh:mm:ss").format("jYYYY/jMM/jDD hh:mm")}</td>
                            <td className="text-center">
                              <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                                <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    checked={!project.is_archived}
                                  />
                                  <label
                                    className="custom-control-label">
                                  </label>
                                </div>
                              </FormGroup>
                            </td>
                            <td className="text-center">
                              <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                                <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    checked={project.is_approved}
                                  />
                                  <label
                                    className="custom-control-label">
                                  </label>
                                </div>
                              </FormGroup>
                            </td>
                            <td>{project?.province}</td>
                            <td>{project?.project?.title}</td>
                            <td>
                            <div className="btn btn-info text-center" onClick={()=>handleGetListPartners(project?.id)}>لیست شرکا</div>
                            </td>
                            <td>

                              <NavLink className="btn btn-primary text-center" to={`/state-create/${project?.id}`}>ویرایش</NavLink>

                            </td>
                            {/* <td> */}
                              {/* <Action id={project?.id} title={project?.title}
                                      deleteEstate={() => deleteEstate(project?.id)} /> */}

                              {/*<UncontrolledDropdown right>*/}
                              {/*  <DropdownToggle*/}
                              {/*    href="#"*/}
                              {/*    className="card-drop"*/}
                              {/*    tag="i"*/}
                              {/*  >*/}
                              {/* <FaRegDotCircle/>*/}
                              {/*  </DropdownToggle>*/}
                              {/*  <DropdownMenu className=" dropdown-menu-right" right>*/}
                              {/*    <DropdownItem href="#">Action</DropdownItem>*/}
                              {/*    <DropdownItem href="#">*/}
                              {/*      Another action*/}
                              {/*    </DropdownItem>*/}
                              {/*    <DropdownItem href="#">*/}
                              {/*      Something else here*/}
                              {/*    </DropdownItem>*/}
                              {/*  </DropdownMenu>*/}
                              {/*</UncontrolledDropdown>*/}
                            {/* </td> */}
                          </tr>

                        </>
                    ))}

                    <ShowListEstatePartners 
                      setModalShowListPartners={setModalShowListPartners}
                      modalShowListPartners={modalShowListPartners}
                      partnersList={partnersList}
                    />
                 

                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom handlePageClick={handlePageClick} page={page}
                            totalPage={Math.floor((listCount) / pageSize ) + 1}
                            addPageSize={setPageSize}
                            pageSize={pageSize}
                            chatsCount={listCount} />

          <Row>
            {/*<Col xs="12">*/}
            {/*  <div className="text-center my-3">*/}
            {/*    <Link to="#" className="text-success">*/}
            {/*      <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />*/}
            {/*      Load more*/}
            {/*    </Link>*/}
            {/*  </div>*/}
            {/*</Col>*/}
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

StateList.propTypes = {
  projects: PropTypes.array,
  onGetProjects: PropTypes.func
}

const mapStateToProps = ({ Estate }) => ({

  projects: Estate.states?.result?.items ,
  listCount: Estate.states?.result?.total,

})

const mapDispatchToProps = dispatch => ({
  onGetProjects: (...data) => dispatch(getStates(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(StateList))
