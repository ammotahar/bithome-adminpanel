import React, { useState , useEffect , useRef } from 'react'
import {
    Button,
    Col,
    FormGroup,
    Label, 
    Row,
    InputGroup,
    Input,
    Table
  } from "reactstrap";
import {CgMathPlus} from "react-icons/all"
import ModalConfirmDelete from './ModalConfirmDelete';
import AsyncSelect from "react-select/async"
import { fetcher } from "store/actions"
import { GET_REPORT_CONTACTS } from 'helpers/url_helper';
import { FaTrash } from "react-icons/all"

function AssignThePropertyToTheOwner(props) {
const  {setFormData , formData  , setParams , params , inputList , setInputList} = props
    const [input , setInput] = useState({user_id : '' , area : ''  , user_name : '' , description : null });
    const [modal, setModal] = useState(false)
    const [indexInfo, setIndexInfo] = useState()
    const [typeInputSearch, setTypeInputSearch] = useState(null)
    const [user_id_list, setUser_id_list] = useState([])

    const inputEl = useRef(null);


    
    
    useEffect(() => {
        if(!!props?.formData?.id){
            setInputList(formData?.owner)
        }
        setUser_id_list(formData?.owner?.map(item => item.user_id))
    }, [formData?.owner])



    // handle set owner input change
    const handleSetUser = (e) => {                
        setInput({ ...input , user_id : e.id , user_name : e.credentials.full_name })
    };

    // handle set area input change
    const handleSetArea = (e) => {
        setInput({ ...input , area : e.target.value })
    };


    // handle click event of the Add button
    const handleAddClick = (e) => {
        e.preventDefault()

        
        if( !formData?.estate_last_trade && input?.user_name && input?.area ){
            

            if(!user_id_list.includes(input.user_id)){

                setInputList([...inputList?.filter(item => item.area !== 0) , input ])
                setTypeInputSearch(null)
                setInput({user_id : '' , area : ''  , user_name : '' , description : null })
                inputEl.current.select.state.value = ''
            }

        }

    };


    // api call service for list customers and set in dropdown menu select filter user
    const loadResults = name => {
        return fetcher(
            GET_REPORT_CONTACTS,
        {
            method: "GET",
        },
        {
            query: { name: name, limit: 50000 },
        }
    )
      .then(r => {
        if (r?.result) {
          let search_list = []
          r?.result?.items?.map(i =>
            search_list.push({
              label: i?.credentials?.full_name,
              value: i?.id,
              ...i,
            })
          )

          return search_list
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })
  }


    const handleSetDefaultOwneer = (e , id) => {
        setInputList(inputList?.map(item => item?.user_id === id ? {...item , description : 'defaul_owner'} : item))
        setFormData({...formData , default_owner_id : id})
    }


    
    return (
    <div className="">
                <div className="box">
                    <FormGroup className="mb-4" row>
                        <Label
                              htmlFor="projectname"
                              className="col-form-label col-lg-2" />
                        <Col lg="12">
                            <Row>
                                <Col  lg="4">
                                    <Label
                                        htmlFor="user_id"
                                        className="col-form-label"
                                    >
                                        نام مالک
                                    </Label>
             

                                        <FormGroup className="ajax-select  mt-lg-0 select2-container">
                                            <AsyncSelect

                                                defaultOptions={true}
                                                isClearable={() => handleReset("type")}
                                                name="user_id"
                                                id="user_id"
                                                ref={inputEl}
                                                loadOptions={loadResults}
                                                defaultValue={!!typeInputSearch ? typeInputSearch : ""}
                                                className="basic-multi-select-owner-estate"
                                                onChange={e => {
                                                if (e) {
                                                    handleSetUser(e)
                                                    setTypeInputSearch(e)
                                                    setParams({
                                                    ...params,
                                                    page: 0,
                                                    owner_id: e ? e.value : null,
                                                    })
                                                }
                                                }}
                                                placeholder="انتخاب کنید"
                                            />
                                        </FormGroup>
                                </Col>
                                <Col lg="5">
                                    <Label
                                        htmlFor="area"
                                        className="col-form-label"
                                    >
                                        متراژ
                                    </Label>
                                        
                                        <InputGroup>
                                        <Input
                                            style={{height : '38px'}}
                                            className="form-control"
                                            name="area"
                                            type="number"
                                                        
                                            placeholder="متراژ"
                                            value={input.area}
                                                        
                                            onChange={e => handleSetArea(e)}
                                        />
                                        
                                        </InputGroup>
                                </Col>
                                <Col  lg="3">
                                    <Label
                                        htmlFor="area"
                                        className="col-form-label"
                                    >
                                        عملیات
                                    </Label>
                                    <div className="btn-box">
                                        {/* {inputList?.length !== 1 && <Button
                                        color="danger"
                                        onClick={(e) => handleRemoveClick(e , i)}><i><BsTrashFill /> </i></Button>} */}
                                        {/* {inputList?.length - 1 === i && <Button color="success" className="ml-2" onClick={(e)=>handleAddClick(e)}><i><CgMathPlus /> </i></Button>} */}
                                        { <Button color="success" className="ml-2" onClick={(e)=>handleAddClick(e)}><i><CgMathPlus /> </i></Button>}
                                    </div>
                                    <ModalConfirmDelete 
                                        setModal={setModal} 
                                        modal={modal} 
                                        inputList={inputList} 
                                        setInputList={setInputList}
                                        setFormData={setFormData} 
                                        formData={formData}
                                        index={indexInfo}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </FormGroup> 
            </div>

        {inputList?.length ?
           <>
            <label className="col-md-2 col-form-label"> لیست جزییات</label>
            <div className="col-md-10">
              <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                <thead>
                    <tr>
                    <th>شماره</th>
                    <th className="text-center">مالک پیشفرض</th>
                    <th className="text-center" scope="col">نام مالک</th>
                    <th className="text-center" scope="col">متراژ</th>

                    <th scope="col" className="text-center">حذف</th>
                    </tr>
                </thead>
                <tbody>


                    {inputList?.filter(item=> item.area !== 0)?.map((item, i) => {

                        return (
                            <>
                            <tr >
                                <td>{i + 1}</td>
                                <td className="text-center">
                                    <input  checked={formData?.default_owner_id === item?.user_id  } onChange={(e)=>handleSetDefaultOwneer(e ,  item?.user_id)} type ="checkbox"/>
                                </td>
                                <td className="text-center">{item?.user_name}</td>
                                <td className="text-center">{item?.area}</td>

                                <td className="text-center">
                                        
                                    <span className="text-danger " style={{ cursor: "pointer" }}
                                            onClick={() => {

                                               if(!formData?.estate_last_trade){
                                                    let list = inputList
                                                    delete list[item]
                                                    setInputList(
                                                        inputList?.map(itemInput => itemInput === item ? {...itemInput , area : 0 , description : null } : itemInput)
                                                    )
                                                    setUser_id_list(user_id_list.filter(id => id !== item?.user_id))
                                                }
                                            } 
                                        }
                                    >
                                        <FaTrash />
                                    </span>
                                        
                                </td>
                                </tr>
                            </>

                        )
                    })}


                </tbody>
              </Table>
            </div>
        
        </> : ""
    }
        
    </div>
    )
}

export default AssignThePropertyToTheOwner;
