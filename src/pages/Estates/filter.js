import React, { useEffect, useState } from "react"
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  FormGroup, Input, InputGroup,
  Label,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane
} from "reactstrap"
import { FaPlusCircle, FaSearch } from "react-icons/all"
import { getEstatesType, getEstatesUsers } from "../../store/estates/actions"
import { getProjects } from "../../store/projects/actions"
import { connect } from "react-redux"
import { Link } from "react-router-dom"

const Filter = (props) => {
  const { params, setParams, setFilter } = props
  const [remove, setRemove] = useState(false)
  useEffect(() => {
    props.getEstatesType()
  }, [])
  const addFilter = (e) => {
    setRemove(false)
    let param = { ...params, [e.target.name]: e.target.value }
    setParams(param)
  }
  useEffect(() => {
    if (remove)
      setFilter()
  }, [props.params])
  return (
    <>
      <Form id="formFilterEstate">
        <Row>
          {/*<div className="col-xl col-sm-6">*/}
          {/*<FormGroup className="mt-3 mb-0">*/}
          {/*<Label>Date :</Label>*/}
          {/*<DatePicker*/}
          {/*  selected={startDate}*/}
          {/*  onChange={handleChange}*/}
          {/*  className="form-control"*/}
          {/*  placeholderText="Select date"*/}
          {/*/>*/}
          {/*</FormGroup>*/}
          {/*</div>*/}

          <div className="col-xl col-sm-6">
            <FormGroup className="mt-3 mb-0">
              <Label>استان</Label>
              <select className="form-control select2-search-disable">
                <option value="BTC" defaultValue>
                  همه استان ها
                </option>
                {/*<option value="ETH">Ethereum</option>*/}
                {/*<option value="LTC">litecoin</option>*/}
              </select>
            </FormGroup>
          </div>

          <div className="col-xl col-sm-6">
            <FormGroup className="mt-3 mb-0">
              <Label>نوع ملک</Label>
              <select onChange={addFilter} className="form-control"
                // onChange={addData}
                      id="estate_type"

                // value={formData?.estate_type}
                      name="estate_type_id">
                <option className="text-secondary"> انتخاب نوع کاربری ملک ...</option>
                {
                  props?.estates_type?.length && props?.estates_type.map((option, i) => <option
                    value={option?.id}>{option?.title_fa}</option>)
                }
              </select>
            </FormGroup>
          </div>

          <div className="col-xl col-sm-6 ">

            <FormGroup className="mt-2 mb-0">
              <Label
                htmlFor="sheba_number"
                className="col-form-label"
              >
                جستجو
              </Label>

              <InputGroup>

                <Input
                  id="title"
                  name="title"
                  type="text"
                  className="form-control"
                  placeholder=" نام ملک ..."
                  onChange={addFilter}
                  // defaultValue={bank?.sheba_number}
                />
                <div className="input-group-append">
                          <span className="input-group-text">
                          <FaSearch/>
                          </span>
                </div>
              </InputGroup>
            </FormGroup>
          </div>
          <Col lg="4" xl="3" className="mt-3">
            <label className="text-white d-block"> .</label>
            <Button type="button" color="primary" onClick={()=>setFilter()}>اعمال فیلتر</Button>
            <Button color="warning" className="ml-2" onClick={() => {
              document.getElementById("formFilterEstate").reset()
              // setCreated_at(null)
              // setUpdated_at(null)
              props.setParams({})
              setRemove(true)
            }}>حذف فیلتر</Button>
          </Col>
          {/*<div className="col-xl col-sm-6 align-self-end">*/}
          {/*  <div className="mt-3">*/}
          {/*    <Link*/}
          {/*      to="/state-create"*/}
          {/*      className="btn btn-primary w-100"*/}
          {/*    >*/}
          {/*      <span className="px-2"><FaPlusCircle/></span>*/}
          {/*      ملک جدید*/}
          {/*    </Link>*/}
          {/*  </div>*/}
          {/*</div>*/}
        </Row>
      </Form>
    </>
  )
}

const mapStateToProps = ({ Estate, projects }) => {
  return {
    estates_type: Estate.estates_type?.result || []
  }
}

const mapDispatchToProps = dispatch => ({
  getEstatesType: (...data) => dispatch(getEstatesType(...data))

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((Filter))