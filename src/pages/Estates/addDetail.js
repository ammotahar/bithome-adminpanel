import React, { useState } from "react"
import { Button, CardTitle, Form, Input, Table } from "reactstrap"
import { FaTrash } from "react-icons/all"

const AddDetail = ({setFormData,formData}) => {
  const [detail,setDetail]=useState({ label: "", value: "دارد" })
  return (
    <>
      <CardTitle className="mb-3 mt-5">جزییات</CardTitle>

      <div className=" row">
        <label className="col-md-2 " />
        <div className="col-md-8">
          <small>دراین قسمت می توانید نام جزییات دلخواه و وضعیت ان را وارد کرده و دکمه افزودن را
            بزنید</small>
        </div>
      </div>
      <div className="form-group row">

        <label className="col-md-2 col-form-label"> نام جزییات</label>
        <div className="col-md-10">

          <Input
            placeholder="جزییات دلخواه وارد کنید"
            type="text"
            className="form-control"
            value={detail?.label}
            onChange={(e) => setDetail({ ...detail, label: e.target.value })}
            // defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
          />
          {/*{error.some(t => t === item?.value) && <small className="text-danger">*/}
          {/*<span className="pr-1">  <FaTimesCircle /></span>*/}
          {/**/}
          {/*{item?.name + " را بصورت صحیح وارد کنید "}*/}
          {/*</small>}*/}
        </div>
        <label className="col-md-2 col-form-label mt-3"> وضعیت جزییات</label>
        <div className="col-md-10 mt-3">

          {/*<Input*/}
          {/*  placeholder=" وضعیت جزییات  وارد کنید"*/}
          {/*  type="text"*/}
          {/*  className="form-control"*/}
          {/*  value={detail?.value}*/}
          {/*  onChange={(e) => setDetail({ ...detail, value: e.target.value })}*/}
          {/*/>*/}
          {/*{error.some(t => t === item?.value) && <small className="text-danger">*/}
          {/*<span className="pr-1">  <FaTimesCircle /></span>*/}
          {/**/}
          {/*{item?.name + " را بصورت صحیح وارد کنید "}*/}
          {/*</small>}*/}
          <select   className="form-control"  onChange={(e) => setDetail({ ...detail, value: e.target.value })}>
            <option value="دارد">دارد</option>
            <option value="ندارد">ندارد</option>
          </select>
        </div>

        <label className="col-md-2 " />
        <div className="col-md-10 mt-3">
          <Button type="button" color="primary"
                  onClick={() => {
                    (detail?.label && detail?.value) &&
                    setFormData({
                      ...formData,
                      details: { ...formData?.details, [detail?.label]: detail?.value }
                    })
                    setDetail({ label: "", value: "دارد" })
                  }
                  }>افزودن
            به جزییات</Button>
        </div>
        {Object.keys(formData?.details)?.length ?
          <>
            <label className="col-md-2 col-form-label"> لیست جزییات</label>
            <div className="col-md-10">
              <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                <thead>
                <tr>
                  <th>شماره</th>
                  <th scope="col">نام</th>
                  <th scope="col">وضعیت</th>

                  <th scope="col" className="text-center">حذف</th>
                </tr>
                </thead>
                <tbody>
                {Object.keys(formData?.details).map((f, i) => (
                  <tr key={i}>
                    <td>{i + 1}</td>
                    <td>
                      {f}
                    </td>
                    <td>{formData?.details[f]}</td>

                    <td className="text-center">
                              <span className="text-danger " style={{ cursor: "pointer" }}
                                    onClick={() => {
                                      let list = formData?.details
                                      delete list[f]
                                      setFormData({
                                        ...formData,
                                        details: list
                                      })
                                    }
                                    }
                              >
                                <FaTrash />
                              </span>
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </div>
          </> : ""
        }
      </div>
    </>
  )
}

export default AddDetail