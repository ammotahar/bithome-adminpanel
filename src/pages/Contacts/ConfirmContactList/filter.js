import React, { useEffect, useState } from "react";
import { Button, Col, Container, Form, FormGroup, Row } from "reactstrap";
import DatePicker from "react-datepicker2";
import moment from "moment-jalaali";
import { file_fetcher } from "store/actions";
import { DOWNLOAD_FILE_EXCEL } from "helpers/url_helper";
import queryString from 'query-string';
import { exportExcelFile } from "utils/exportFiles";

const Filter = (props) => {
  const [remove, setRemove] = useState(false)
  const [created_at, setCreated_at] = useState(null)
  const [updated_at, setUpdated_at] = useState(null)
  const addFilter = (e) => {
    setRemove(false)
    props.setParams({ ...props.params, [e.target.name]: e.target.value })
  }

  useEffect(() => {
    if (remove)
      props.getUsers()
  }, [props.params])



  // function for download excel file of 'confirm-contacts'
  const handleExportExcelFile = () => {
    const queries = queryString.stringify(props.params)
    exportExcelFile(
      `${DOWNLOAD_FILE_EXCEL}?${queries}`,
      "confirm-contacts.xlsx"
    )
  }


  return (
    <>
      <Form id="frm">
        <Row>
          <Col lg="4" xl="3" className="mt-2">
            <label>جستجو</label>
            <input type="text" className="form-control"
                   name="name"
                   placeholder="جستجو"
                   onChange={addFilter} />
          </Col>

          <Col lg="4" xl="3" className="mt-2">
            <label>ایجاد شده در تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at}
              isGregorian={false}
              onChange={(value) => {
                if (value) {
                  setCreated_at(value)
                  setRemove(false)
                  props.setParams({ ...props.params, created_at: value.format("YYYY-MM-DD") })
                }
              }}
              name="created_at"
              id="created_at"
              removable={true}
            />
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label>بروز شده در تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={updated_at}
              isGregorian={false}
              onChange={(value) => {
                if (value) {
                  setUpdated_at(value)
                  setRemove(false)
                  props.setParams({ ...props.params, updated_at: value.format("YYYY-MM-DD") })
                }
              }}
              name="updated_at"
              id="updated_at"
              removable={true}
            />
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label className="text-white d-block"> .</label>
            <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
              <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id="customCheckcolor1"
                  checked={props.params?.is_active}
                  onChange={(e) => {
                    setRemove(false)
                    props.setParams({ ...props.params, is_active: e.target.checked })
                  }}
                />
                <label
                  className="custom-control-label"
                  htmlFor="customCheckcolor1"
                >
                  کاربران فعال
                </label>
              </div>

            </FormGroup>
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label className="text-white d-block"> .</label>
            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
              <input
                type="checkbox"
                className="custom-control-input"
                id="customCheckcolor2"
                checked={props.params?.is_approved}
                onChange={(e) => {
                  setRemove(false)
                  props.setParams({ ...props.params, is_approved: e.target.checked })
                }}
              />
              <label
                className="custom-control-label"
                htmlFor="customCheckcolor2"
              >
                احراز شده
              </label>
            </div>
          </Col>
          <Col lg="" xl="" className="mt-2">
            <label className="text-white d-block"> .</label>
            <Button color="primary" onClick={()=>props.getUsers()}>اعمال فیلتر</Button>
            <Button color="btn btn-outline-danger" className="mx-2" onClick={() => {
              document.getElementById("frm").reset()
              setCreated_at(null)
              setUpdated_at(null)
              props.setParams({ order: "-updated_at" })
              setRemove(true)
            }}>حذف فیلتر</Button>
            <Button color="success" onClick={handleExportExcelFile}>خروجی اکسل</Button>
              </Col>
              </Row>
              </Form>
              </>
              )
              }

              export default Filter