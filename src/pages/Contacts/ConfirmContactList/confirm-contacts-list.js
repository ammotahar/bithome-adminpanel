import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, NavLink, withRouter } from "react-router-dom"
import {
  Button,
  Col,
  Container,
  Modal,
  Row, Spinner,
  Table
} from "reactstrap"

//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"

import { getUsers } from "store/contacts/actions"
import { fetcher } from "store/common/action"
import { GET_COUNT_CONTACTS, GET_COUNT_ESTATES, GET_USER_ChECK } from "helpers/url_helper"
import { MessageToast } from "utils/message"
import Filter from "./filter"
import PaginationCustom from "../../../components/paginationZero"

const Action = (props) => {
  const [modal, setModal] = useState(false)
  return <div dir="ltr">
    <Button color="danger" href="#" onClick={() => setModal(true)}>
      حذف
    </Button>
    <Modal centered={true} isOpen={modal} toggle={() => setModal(false)}>
      <div className="p-4">
        <p className="pb-4">آیا از حذف این کاربر اطمینان دارید؟</p>
        <div className="row">

          <div className="col-6">
            <Button type="button" color="danger" className="w-100"
                    onClick={() => setModal(false)}>خیر</Button>
          </div>

          <div className="col-6">
            <Button type="button" color="primary" className="w-100"
                    onClick={() => {
                      // props.deleteProject()
                      setModal(false)
                    }}>بله</Button>
          </div>
        </div>
      </div>
    </Modal>
  </div>
}
const ConfirmContactsList = props => {
  const { users, onGetUsers } = props
  const [userList, setUserList] = useState([])
  const [params, setParams] = useState({ order: "-updated_at" })
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)


  useEffect(() => {
    getCount()
    getUsers()
  }, [])
  useEffect(() => {
    setLoading(false)
  }, [users])
  const getCount = (id) => {
    fetcher(GET_COUNT_CONTACTS, {
      method: "GET"
    }).then(r => {
      if(r?.result)
        setCounts(r.result.users_count)
    }).catch(e => {
      // MessageToast("error", "باخطا مواجه شدید")
    })
  }
  const getUsers = (p=0,s=pageSize) => {
    setLoading(true)
    setPage(p)
    onGetUsers({ query: {page: p, size: s , is_approved : true , ...params }})
  }
  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getUsers(p, s)
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="کاربران" breadcrumbItem="لیست کاربران تایید شده" />

          <Filter params={params} setParams={setParams} getUsers={getUsers} />

          <Row>

            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        ردیف
                      </th>
                      <th scope="col"> نام کاربر</th>
                      {/*<th scope="col" className="text-center"> تعداد ملک</th>*/}
                      <th scope="col"> شماره ملی</th>
                      <th scope="col"> موبایل</th>
                      <th scope="col" className="text-right"> فعال</th>

                      <th scope="col" className="text-right"> احرازهویت</th>


                      <th scope="col" className="text-center">جزییات</th>
                      {/*<th scope="col" className="text-center">حذف</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {users?.items?.length &&
                      users?.items?.map((item, i) =>
                        <tr>
                          {/* <td className="text-center"> {((page-1) * pageSize)+(i + 1)}</td> */}
                          <td className="text-center"> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          {/* <td className="text-center"> {page == 1 ?  ++i : (page * pageSize) + ++i }</td> */}
                          <td>{(item?.credentials?.first_name || "") + " " + (item?.credentials?.last_name || "")}</td>
                          {/*<td className="text-center">*/}
                          {/*  1*/}

                          {/*</td>*/}
                          <td>{item?.national_code}</td>
                          <td>{item?.mobile}</td>
                          <td className="text-right">
                            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id={"customCheckcolorUser" + (i + 1)}
                                checked={item?.is_active}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor={"customCheckcolorUser" + (i + 1)}
                              >
                              </label>
                            </div>
                          </td>
                          <td className="text-right">
                            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id={"customCheckcolorUser" + (i + 1)}
                                checked={item?.is_approved}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor={"customCheckcolorUser" + (i + 1)}
                              >
                              </label>
                            </div>
                          </td>
                          <td className="text-center">
                            <Button color="primary"><Link className="text-white"
                                                          to={"/contacts-profile/" + item?.id}>جزییات</Link></Button>
                          </td>
                          {/*<td className="text-center"><Action id={item?.id} title={item?.title} /></td>*/}
                        </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <PaginationCustom handlePageClick={handlePageClick} page={page}
                            // totalPage={Math.floor(counts / pageSize) + 1}
                            totalPage={Math.floor(users?.total / pageSize) + 1}
                            addPageSize={setPageSize}
                            pageSize={pageSize}
                            chatsCount={users?.total} 
                            
                            
                            
                            
                            // totalPage={Math.floor(listCount / pageSize) + 1}
                            // addPageSize={setPageSize}
                            // pageSize={pageSize}
                            // chatsCount={listCount}
                            
                            />
          <div className="text-center">
            {loading ? <Spinner /> : ""}
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}

ConfirmContactsList.propTypes = {
  users: PropTypes.array,
  onGetUsers: PropTypes.func
}

const mapStateToProps = ({ contacts }) => ({
  users: contacts.users?.result || []
})

const mapDispatchToProps = dispatch => ({
  onGetUsers: (...data) => dispatch(getUsers(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ConfirmContactsList))
