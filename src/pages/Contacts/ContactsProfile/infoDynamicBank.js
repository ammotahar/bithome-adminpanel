import React, { useState , useEffect } from 'react'
import {
    Button,
    Col,
    FormGroup,
    Label, 
    Row,
    InputGroupText,
    InputGroup,
    Input
  } from "reactstrap";
  import { BsTrashFill , CgMathPlus } from "react-icons/all"
import ModalConfirmDelete from './ModalConfirmDelete';



function infoDynamicBank({setFormData , formData }) {

    const [inputList , setInputList] = useState([{card_number : '' , sheba_number : '' }]);
    const [modal, setModal] = useState(false)
    const [indexInfo, setIndexInfo] = useState()

useEffect(() => {
    console.log("formData?.bank_accounts -->>> ",!!formData?.bank_accounts?.length);
    setInputList(!!formData?.bank_accounts?.length ? formData?.bank_accounts : [{card_number : '' , sheba_number : '' }] )

}, [formData])

    // handle card number input change
    const handleInputChangeCardNumber = (e, index) => {
      let { name, value } = e.target;
        let list = [...inputList];
        list[index][name] = value?.length <= 16 && !isNaN(Number(value))  ? value : list[index][name];;
        setInputList(list);
        setFormData({...formData , bank_accounts :  list})
    };

    // handle sheba number input change
    const handleInputChangeShebaNumber = (e, index) => {
        let { name, value } = e.target;
        let list = [...inputList];
        list[index][name] = value?.length <= 24 && !isNaN(Number(value))  ? value : list[index][name];
        setInputList(list);
        setFormData({...formData ,  bank_accounts :  list})
    };

    // handle click event of the Remove button
    const handleRemoveClick = (e , index) => {
        e.preventDefault()
        setIndexInfo(index)
        setTimeout(() => {
            setModal(true);
        }, 500);
  };
   
    // handle click event of the Add button
    const handleAddClick = (e) => {
        e.preventDefault()
        setInputList([...inputList, { card_number: "", sheba_number: "" }]);
        setFormData({...formData ,  bank_accounts : [...inputList, { card_number: "", sheba_number: "" }]})
    };



    return (
    <div className="">
        {inputList?.map((info, i) => (
                <div className="box">
                    <FormGroup className="mb-4" row>
                        <Label
                              htmlFor="projectname"
                              className="col-form-label col-lg-2" />
                        <Col lg="12">
                            <Row>
                                <Col  lg="4">
                                    <Label
                                        htmlFor="sheba_number"
                                        className="col-form-label"
                                    >
                                        شماره کارت
                                    </Label>
                                    <input
                                        className="form-control"
                                        name="card_number"
                                        type="number"
                                       
                                        placeholder="شماره کارت"
                                        value={info.card_number}
                                        
                                        onChange={e => handleInputChangeCardNumber(e, i)}
                                    />
                                </Col>
                                <Col lg="5">
                                    <Label
                                        htmlFor="sheba_number"
                                        className="col-form-label"
                                    >
                                        شماره شبا
                                    </Label>
                                        
                                        <InputGroup>
                                        <Input
                                            className="form-control"
                                            name="sheba_number"
                                            type="number"
                                                        
                                            placeholder="شماره شبا"
                                            value={info.sheba_number}
                                                        
                                            onChange={e => handleInputChangeShebaNumber(e, i)}
                                        />
                                        {/* <InputGroupText>IR</InputGroupText> */}
                                         <div className="input-group-append">
                                            <span className="input-group-text">
                                            IR
                                            </span>
                                        </div>
                                        </InputGroup>
                                </Col>
                                <Col  lg="3">
                                    <Label
                                        htmlFor="sheba_number"
                                        className="col-form-label"
                                    >
                                        عملیات
                                    </Label>
                                    <div className="btn-box">
                                        {inputList?.length !== 1 && <Button
                                        color="danger"
                                        onClick={(e) => handleRemoveClick(e , i)}><i><BsTrashFill /> </i></Button>}
                                        {inputList?.length - 1 === i && <Button color="success" className="ml-2" onClick={(e)=>handleAddClick(e)}><i><CgMathPlus /> </i></Button>}
                                    </div>
                                    <ModalConfirmDelete 
                                        setModal={setModal} 
                                        modal={modal} 
                                        inputList={inputList} 
                                        setInputList={setInputList}
                                        setFormData={setFormData} 
                                        formData={formData}
                                        index={indexInfo}/>
                                </Col>
                            </Row>
                        </Col>
                    </FormGroup>
            </div>
        ))}
    </div>
    )
}

export default infoDynamicBank;
