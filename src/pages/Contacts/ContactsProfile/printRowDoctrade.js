import React, { useEffect } from "react"


const PrintRowDoctrade = ({rowDoc , readyDownload , setReadyDownload}) => {

  useEffect(() => {

    if (!!readyDownload){
      printDiv()
    }

  }, [readyDownload])

  const printDiv = () => {
    window.frames["print_frame"].document.body.innerHTML =
      document.getElementById("printDiv").innerHTML +

      '<style>\n' +

      '   * {\n' +
      '       box-sizing: border-box;\n' +
      '       padding: 0;\n' +
      '       margin: 0;\n' +
      '   }\n' +
      '   p{\n' +
      '    }\n' +
      
      '@media print {\n' +

      '   * {\n' +
      '       box-sizing: border-box;\n' +
      '       padding: 0;\n' +
      '       margin: 0;\n' +
      '   }\n' +
     
      '   #printDiv{\n' +
      '     position: relative !important; \n' +
      '   }\n' +
      '        body{\n' +
      '             background: red !important ;  \n' +
      '             font-family: IRANSansXFaNum !important;\n' +
      '             font-size: 14px !important;\n' +
      '             direction: rtl;\n' +
      '        }\n' + 
      '        p{\n' +
      '        }\n' +
      '        #page {\n' +
      '            background: white;\n' +
      '            display: block;\n' +
      '        }\n' +
      '        /* page[size="A4"] {\n' +
      '             width: 21cm;\n' +
      '             height: 29.7cm;\n' +
      '    } */\n' +
      '        @page {\n' +
      '            size: A4;\n' +
      '        }\n' +
      '        @page :left {\n' +
      '            margin-left: 0;\n' +
      '        }\n' +
      ' \n' +
      '        @page :right {\n' +
      '            margin-right: 0;\n' +
      '        }\n' +

      
      ".hidden-element { \n" +
      "     visibility: hidden !important; \n" +
      "}\n" +


      " #tracking_code{ \n " +
      "  position: absolute !important; \n " +
      "  top : 200px !important \n " +
      "  right : 100px !important \n " +
      "}   \n" +

    '} \n' +
    '    </style>';

    "<div> \n" +
     
    "</div> \n" +

    setTimeout(() => {
      window.frames["print_frame"]?.window.focus()
      window.frames["print_frame"]?.window.print()
      setReadyDownload(null)
    }, 200)
  }

  return (
    <>

    <div style={{display : 'none'}} id='printDiv'>
     <div  id="page">
       
     </div>
       <p style={{position : 'absolute' , top : '115px' , left : '62px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.tracking_code}</p>
       <p style={{position : 'absolute' , top : '139px' , left : '84px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.ir_date}</p>

       <div style={{position : 'absolute' , top : '233px' , right : '179px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.full_name}</div>
       <p style={{position : 'absolute' , top : '235px' , right : '397px' , fontFamily : 'IRANSansXFaNum'}} >{rowDoc?.national_code}</p>
       <p style={{position : 'absolute' , top : '235px' , right : '588px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.birth_date}</p>

       <p style={{position : 'absolute' , top : '356px' , right : '126px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.estate_name}</p>
       <p style={{position : 'absolute' , top : '357px' , right : '348px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.project_name}</p>
       <p style={{position : 'absolute' , top : '358px' , right : '525px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.estate_code}</p>

       <p style={{position : 'absolute' , top : '397px' , right : '139px', fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.estate_address}</p>
       
       <p style={{position : 'absolute' , top : '440px' , right : '200px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.estate_meter}</p>
       <p style={{position : 'absolute' , top : '440px' , right : '332px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.floor}</p>
       <p style={{position : 'absolute' , top : '440px' , right : '542px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.unit_number}</p>
       

       <p style={{position : 'absolute' , top : '566px' , right : '245px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.user_area}</p>
       <p style={{position : 'absolute' , top : '566px' , right : '496px' , fontFamily : 'IRANSansXFaNum'}}>{rowDoc?.estate_calculated_value}</p>

     </div>


     <iframe
       style={{ height: 0  }}
       
       name="print_frame"
       width="100px"
       height="100px"
       frameBorder="0"
       src="about:blank"
       
     />
   </>
  )
}

export default PrintRowDoctrade ;
