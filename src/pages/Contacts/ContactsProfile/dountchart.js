import React from "react"
import ReactApexChart from "react-apexcharts"

const dountchart = ({stock=[],data=[]}) => {
console.log(data.map(t=>Math.floor(Math.random()*16777215).toString(16)))
  const options = {
    labels: data.map(t=>t?.title),
    // labels: ["Series 1"],
    colors:["#3b14a7","#bf1363","#f54748","#fb9300","#480032","#cf0000","#2978b5" , "#52734d","#008891"],
    legend: {
      show: true,
      position: "bottom",
      horizontalAlign: "center",
      verticalAlign: "middle",
      floating: false,
      fontSize: "20px",
      fontFamily: 'IRANSans_light',

      offsetX: 0,
      offsetY: -10,
    },
    responsive: [
      {
        breakpoint: 600,
        options: {
          chart: {
            height:650,
          },
          legend: {
            show: true,
          },
        },
      },
    ],
  }

  return (
    <ReactApexChart
      options={options}
      series={stock}
      type="donut"
      height="650"

    />
  )
}

export default dountchart
