import React, { useEffect, useState } from "react"
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Table
} from "reactstrap"
import { FaTimesCircle } from "react-icons/all"
import { Link, withRouter } from "react-router-dom"
import { fetcher } from "store/common/action"
import { GET_USER_PERMISSIONS, GET_USER_PAYMENT, POST_USER_PAYMENT, POST_USER_UPDATE_STATUS_PAYMENT } from "helpers/url_helper"
import numberWithCommas from "components/threeNumber"
import moment from "moment-jalaali"
import { MessageToast } from "utils/message"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { getMoney } from "store/auth/profile/actions"
import Status from "../../../components/status"
import PaginationCustom from "../../../components/paginationZero"
import { MdUpdate } from "react-icons/md";

const FormListInput = [
  { name: "مبلغ(تومان) ", value: "amount", type: "number" },
  { name: " شماره حساب", value: "user_bank_account", type: "number" },
  { name: "کدپیگیری", value: "tracking_code", type: "number" },
  { name: "توضیحات", value: "description" }

]
const Charge = (props) => {
  const [formData, setFormData] = useState({ money_wallet_id: 1 })
  const [error, setError] = useState([])
  const [payment, setPayment] = useState([])
  const { getMoney, money } = props
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [listCount, setListCount] = useState(0)
  const [userPermissions,setUserPermissions]=useState([])

  let permissions = []

  useEffect(() => {
    getData({ query: {page: page, size: pageSize } })
    getMoney(props?.match?.params?.id)
    getUserPermissions()
  }, [])

  useEffect(() => {
    if (money?.id)
      setFormData({ money_wallet_id: money?.id })
  }, [money])

  // GET USER PERMISSIONS FUNCTION
  const getUserPermissions = () => {
    fetcher(GET_USER_PERMISSIONS, {
      method: "GET"
    }).then(r => {
      if(r?.result)
      setUserPermissions(r?.result)

    }).catch(e => {
      // MessageToast("error", "باخطا مواجه شدید")
    })
  }

  // SET PERMISSIONS TITLE IN LIST
  userPermissions?.map( permission => {
    permissions.push(permission?.permission.title)
  })


  const getData = (query) => {
    fetcher(GET_USER_PAYMENT(props?.match?.params?.id), {
      method: "GET"
    },query).then(r => {
      if (r?.result) {
        setPayment(r?.result?.items)
        setListCount(r?.result?.total)
      }
    }).catch(e => {
      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }
  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    getData({ query: { page: p, size: s } })
  }
  const postData = (e) => {
    e.preventDefault()
    fetcher(POST_USER_PAYMENT(props?.match?.params?.id), {
      method: "POST",
      body: JSON.stringify(formData)
    }).then(r => {
      getData()

      FormListInput.map(t => {
        document.getElementById(t.value).value = ""
      })
      MessageToast("success", "شارژ کیف پول با موفقیت انجام شد")
    }).catch(e => {
      MessageToast("error", "با خطا مواجه شدید")
    })
  }
  const addData = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }


  // api call for update status payment user
  const handleClick = (user_id , trakingCode) => {
    fetcher(POST_USER_UPDATE_STATUS_PAYMENT(user_id), {
      method: "POST",
      body: JSON.stringify({ "tracking_code" : trakingCode })
    }).then(r => {
      window.location.reload()
    }).catch(err => 
      console.error(err)
    )

  }


  return (
    <>
      <div>
        <Row>
          <Col lg="12">


            <CardTitle className="mb-4">
              <div className="d-flex justify-content-between align-items-center">
                <span>شارژ کیف پول</span>
                <span className="alert alert-secondary font-size-12 p-2 m-0" role="alert">
                <span>موجودی کیف پول : </span>
                <span>{numberWithCommas(money?.inventory)}</span>
                <span> تومان </span>
                </span>
              </div>
            </CardTitle>
            <Form>
              {/*--------------------------------------------------input-------------------------------------------------*/}
              {
                FormListInput.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                    <Label
                      htmlFor="projectname"
                      className="col-form-label col-lg-2"
                    >
                      {item?.name}
                    </Label>
                    <Col lg="10">
                      <Input
                        id={item?.value}
                        name={item?.value}
                        type={item?.type || "text"}
                        className="form-control"
                        placeholder={item?.name + " ..."}
                        onChange={addData}
                        value={formData?.[item?.value]}
                        defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                      />
                      {error.some(t => t === item?.value) && <small className="text-danger">
                        <span className="pr-1">  <FaTimesCircle /></span>

                        {item?.name + " را بصورت صحیح وارد کنید "}
                      </small>}
                    </Col>
                  </FormGroup>

                  </div>
                )
              }
              <div className="text-right">

              {/* CHECK PERMISSION FOR ALLOW CHARGE WALLET */}
                {permissions.includes("create_deposit_referral") ?
                  <Button type="submit" color="primary" className="px-5" onClick={postData}>
                    شارژ کیف پول
                  </Button>
                : null}

              </div>
            </Form>

            <CardTitle className="mb-4 my-3">گردش کیف پول</CardTitle>
            <div className="">
              <div className="table-responsive">
                <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                  <thead>
                  <tr>
                    <th scope="col">ردیف</th>
                    <th scope="col"> تاریخ و ساعت</th>

                    <th scope="col"> وضعیت</th>
                    <th scope="col"> مبلغ(تومان)</th>
                    <th scope="col"> کدپیگیری</th>

                    <th scope="col">شماره حساب</th>
                    <th scope="col"> توضیحات</th>
                    <th scope="col">بروزرسانی وضعیت پرداخت</th>
                  </tr>
                  </thead>
                  <tbody>
                  {payment?.length &&
                    payment.map((item, i) =>
                      <tr>
                        <td>{i + 1}</td>
                        <td>{moment(item?.created_at, "YYYY-MM-DDThh:mm").format("jYYYY/jMM/jDD hh:mm")}</td>
                        {/* Status component for state of peyment */}
                        <td><Status name={item?.status} /></td>
                        <td>{numberWithCommas(numberWithCommas(item?.amount))}</td>
                        <td>{item?.tracking_code?.slice(-10)}</td>
                        <td>{item?.user_bank_account}
                        </td>
                        <td>{item?.description}</td>
                        {(item?.status !== "success" &&  !!item?.tracking_code) ? 
                            <td className="text-center">
                              <MdUpdate 
                                style={{cursor : 'pointer'}}
                                className="icon-update-status-payment"
                                size="30"
                                // when click icon, execute function that get (user_id) and (tracking_code) and api call for update status payment
                                onClick={()=>handleClick(item?.user_id ,  item?.tracking_code ? item?.tracking_code : '' )}/>
                            </td> : <td></td>}
                      </tr>)
                  }
                  </tbody>
                </Table>
              </div>
            </div>

          </Col>
        </Row>
        <PaginationCustom 
          handlePageClick={handlePageClick} 
          page={page} totalPage={Math.floor((listCount) / pageSize ) + 1}
          addPageSize={setPageSize}
          pageSize={pageSize}
          chatsCount={listCount} />

      </div>
    </>
  )
}

Charge.propTypes = {
  money: PropTypes.object,
  getMoney: PropTypes.func
}

const mapStateToProps = ({ Profile }) => ({
  money: Profile.money?.result || {}
})

const mapDispatchToProps = dispatch => ({
  getMoney: (...data) => dispatch(getMoney(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Charge))