import React, { useEffect, useState } from "react"
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Table
} from "reactstrap"
import { FaCheck, FaCircle } from "react-icons/all"
import { fetcher } from "../../../store/common/action"
import { POST_USER_ORDER } from "../../../helpers/url_helper"
import { MessageToast } from "../../../utils/message"
import { withRouter } from "react-router-dom"
import moment from "moment-jalaali"
import Status from "../../../components/status"
import numberWithCommas from "../../../components/threeNumber"
import PaginationCustom from "../../../components/paginationZero"


const Order = (props) => {
  const [formData, setFormData] = useState([])
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [listCount, setListCount] = useState(0)
  useEffect(() => {
    getData({ query: {page: page, size: pageSize } })
  }, [])
  const getData = (query) => {
    fetcher(POST_USER_ORDER(props?.match?.params?.id), {
      method: "GET"
    },query).then(r => {
      if (r?.result) {
        setFormData(r?.result?.items)
      }
    }).catch(e => {
      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }
  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    getData({ query: { page: p, size: s } })
  }


  return (
    <>
      <div>
        <Row>
          <Col lg="12">


            <CardTitle className="mb-4">سفارش ها</CardTitle>


            <div className="row align-items-center justify-content-end mt-4">
              <div className="col-5 text-right">
                <span className="d-inline-block m-0">دسته بندی براساس :</span></div>
              <div className="col-6 col-lg-5 col-xl-4">
                <select
                  className="form-control"
                  name="user_id"
                  id="user_id"
                  placeholder="انتخاب  ..."
                >
                  <option value="1">1</option>
                  <option value="1">2</option>
                  <option value="1">1</option>
                  <option value="1">1</option>
                </select>
              </div>

            </div>
            <div className="">
              <div className="table-responsive">
                <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                  <thead className="thead-light">
                  <tr>
                    <th scope="col">
                      کدملک
                    </th>
                    <th scope="col"> عنوان ملک</th>
                    <th scope="col"> وضعیت</th>
                    <th scope="col"> متراژ
                      <small>(مترمربع)</small>
                    </th>
                    <th scope="col"> تاریخ</th>
                    <th scope="col"> میانگین خرید
                      <small>(تومان بر متر مربع)</small>
                    </th>
                    <th scope="col">
                      بهای تمام شده خرید
                      <small>(تومان)</small>
                    </th>

                    <th scope="col">ارزش روز
                      <small>(تومان)</small>
                    </th>
                    {/*<th scope="col"> سود و زیان*/}
                    {/*  <small>(تومان)</small>*/}
                    {/*</th>*/}
                    <th scope="col"> نوع</th>
                    <th scope="col"> سهم از کل دارایی</th>
                    <th scope="col">کارمزد</th>
                  </tr>
                  </thead>
                  <tbody>
                  {formData?.length &&
                    formData.map((item, i) =>
                      <tr>
                        <td>{item?.estate_identifier}</td>
                        <td>{item?.title}</td>
                        <td><Status name={item?.status} /></td>
                        <td>{item?.area}</td>
                        <td>{moment(item?.created_at, "YYYY-MM-DDThh:mm").format("jYYYY/jMM/jDD hh:mm")}</td>
                        <td>{item?.final_price}</td>
                        <td>
                          {item?.final_price}
                        </td>
                        <td>{numberWithCommas(item?.amount)}</td>
                        <td>
                          {item?.order_title ==="estate_sell" && "فروش"}
                          {item?.order_title ==="estate_buy" && "خرید"}

                        </td>
                        <td>{item?.percentage}</td>
                        <td>{item?.commission_price}</td>
                      </tr>)
                  }
                  </tbody>
                </Table>
              </div>
            </div>

          </Col>
        </Row>

        <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor((listCount) / pageSize ) + 1}
                          addPageSize={setPageSize}
                          pageSize={pageSize}
                          chatsCount={listCount} />
      </div>
    </>
  )
}

export default withRouter(Order)