import React from 'react'
import {Modal, Button} from "reactstrap"
function ModalConfirmDelete({setModal, modal ,  inputList , setInputList ,formData ,  setFormData , index}) {


// Remove info bank account
const handleDeleteInfoBank = () => {
    setModal(false)
    let list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
    setFormData({...formData , bank_accounts :  list})
}


    return (
        <Modal centered={true}  isOpen={modal} toggle={() => setModal(false)}>
            <div className="p-4">
                <p className="pb-4">آیا از حذف این حساب اطمینان دارید؟</p>
                <div className="row">

                <div className="col-6">
                    <Button type="button" color="primary" className="w-100"
                        onClick={() => setModal(false)}>انصراف</Button>
                </div>

                <div className="col-6">
                    <Button 
                        type="button" 
                        color="danger" 
                        outline 
                        className="w-100"
                        onClick={handleDeleteInfoBank}>حذف حساب</Button>
                </div>
                </div>
            </div>
        </Modal>
    )
}

export default ModalConfirmDelete;
