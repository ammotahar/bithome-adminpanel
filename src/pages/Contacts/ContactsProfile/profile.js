import React, { useEffect, useState } from "react"
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  InputGroup,
  Label, Modal,
  Row, Spinner
} from "reactstrap"
import moment from "moment-jalaali"
import DatePicker from "react-datepicker2"
import { FaTimesCircle } from "react-icons/all"
import { Link, Redirect, withRouter } from "react-router-dom"
import { MessageToast } from "utils/message"
import { fetcher } from "store/common/action"
import { GET_States, GET_USER_ChECK, GGET_USERS_DETAIL, AUTO_SEND_MESSAGE, GET_USER_ACTIVE } from "helpers/url_helper"
import UploadAuthPic from "./uploadAuthPic"
import InfoDynamicBank from "./infoDynamicBank"






const FormListInput = [
  { name: "موبایل", value: "mobile", type: "number", min: 10, max: 15 },
  { name: "کدملی", value: "national_code", type: "number", min: 9, max: 13 }
]
const FormListInputCredentials = [
  { name: "نام", value: "first_name", min: 2, max: 30 },
  { name: "نام خانوادگی", value: "last_name", min: 2, max: 30 },

  // { name: "تاریخ تولد", value: "birth_date", type: "date" },
  { name: " شماره تلفن", value: "phone", type: "number", min: 10, max: 15 },
  { name: "  آدرس ایمیل", value: "email", type: "email", min: 3, max: 50 },
  { name: "  آدرس منزل", value: "address", min: 2, max: 100 }
]

const initialState = {
  "mobile": "",
  "national_code": "",
  "is_approved": true,
  "bank_accounts": [
  
    {
      
      "card_number": "",
      "sheba_number": ""
    }
  ],
  "first_name": "",
  "last_name": "",
  "email": "",
  "birth_date": "",
  "phone": "",
  "address": "",
  "credentials": {
    "birth_date": moment().format("YYYY/MM/DD"),
    "auth_picture_id": 1,
  }
}
const Profile = (props) => {
  const [formData, setFormData] = useState(initialState)
  const [error, setError] = useState([])
  const [next, setNext] = useState(false)
  const [modal, setModal] = useState(false)
  const [loading, setLoading] = useState(false)
  const [bankAccount, setBankAccount] = useState([{}])

  console.log("formData --->>" , formData);
  console.log("bankAccount --->>" , bankAccount);

  useEffect(() => {
    getDetail()
  }, [])


  const getDetail = () => {
    setLoading(true)
    fetcher(GGET_USERS_DETAIL(props?.match?.params?.id), {
      method: "GET"
    }).then(r => {
      setLoading(false)
      if (r?.result) {
        let t = r?.result
        if (t?.bank_accounts?.length)
          setBankAccount(t?.bank_accounts)
        setFormData(t)
      }
    }).catch(e => {
      setLoading(false)
      MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }

  const addData = e => {
    if (e.target.value)
      setFormData({ ...formData, [e.target.name]: e.target.value })
    else
      setFormData({ ...formData, [e.target.name]: "" })
  }

  const create = async (method = "PUT", id = "") => {
    let t = formData?.credentials
    // if (!t?.birth_date) {
    //   (t["birth_date"] = moment())
    // }
    if (!t?.auth_picture_id) {
      (t["auth_picture_id"] = 1)
    }
    let error = []
    let error1 = []
    let error2 = []
    error1 = await Object.keys(formData).filter(t => ((!formData[t])))

    error2 = await Object.keys(formData?.credentials).filter(t => !(formData?.credentials[t]))
    error = await [...error1, ...error2]
    error = await error.filter(t => {
      let o = [
        "is_approved",
        "is_superuser",
        "auth_picture_id",
        "phone_approved",
        "email_approved",
        "updated_at",
        "created_at"
      ].some(p => p === t)
      return !o
    })
    setError(error)

    if (error.length) {
      console.log(error)

      switch (error[0]) {
        case "first_name":
          MessageToast("error", "نام صحیح وارد کنید")
          break
        case "last_name":
          MessageToast("error", "نام خانوادگی صحیح وارد کنید")
          break
        case  "email":
          MessageToast("error", " ایمیل صحیح وارد کنید")
          break
        case  "birth_date":
          MessageToast("error", " تاریخ تولد صحیح وارد کنید")
          break
        case  "phone":
          MessageToast("error", "  تلفن صحیح وارد کنید")
          break
        case  "address":
          MessageToast("error", "  آدرس صحیح وارد کنید")
          break
        case  "national_code":
          MessageToast("error", "  کدملی صحیح وارد کنید")
          break
        case  "mobile":
          MessageToast("error", "  موبایل صحیح وارد کنید")
          break
        default:
          MessageToast("error", "اطلاعات صحیح وارد کنید")
          break
      }
    } else {
      fetcher(GGET_USERS_DETAIL(props?.match?.params?.id), {
        method,
        body: JSON.stringify(formData)
      }).then(r => {
        r?.code && MessageToast("success", "با موفقیت ویرایش شد")
        setNext(true)
      }).catch(e => {
        console.log(e)
        MessageToast("error", "با خطا مواجه شدید")
      })
    }
  }
  const sendMessage = () => {
    fetcher(AUTO_SEND_MESSAGE, {
      method: "POST",
      body: JSON.stringify({
        "users": [props?.match?.params?.id],
        "pro_type": "not_approve"
      })
    })
      .then(r => {
        if (r?.code === 200)
          MessageToast("success", "پیام نقص اطلاعات ارسال شد")
        else
          return false
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })
  }
  if (next) {
    return <Redirect to="/new-contacts-list" />
  }
  return (
    <>
      <div>
        {
          formData?.is_approved ?
            <Alert color="success">
              کاربر احراز هویت شده است
            </Alert>
            :
            <Alert color="danger">
              کاربر احراز هویت نشده است
            </Alert>
        }
        <div className="text-center">
          {loading ? <Spinner /> : ""}
        </div>
        <Row>
          <Col lg="12">


            <CardTitle className="my-4">اطلاعات شخصی </CardTitle>
            <Form>

              {
                FormListInputCredentials.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                    <Label
                      htmlFor="projectname"
                      className="col-form-label col-lg-2">
                      {item?.name}
                    </Label>

                    <Col lg="10">
                      <Input
                        id={item?.value}
                        name={item?.value}
                        type={item?.type || "text"}
                        className="form-control"
                        placeholder={item?.name + " ..."}
                        onChange={(e) => {
                          let t = formData?.credentials
                          t && (t[e.target.name] = e.target.value)
                          setFormData({
                            ...formData,
                            credentials: {
                              ...formData?.credentials,
                              [e.target.name]: e.target.value
                            }
                          })
                        }}
                        defaultValue={formData?.credentials?.[item?.value] ? formData?.credentials?.[item?.value] : ""}
                        invalid={!formData?.credentials?.[item?.value] || !(item?.min < (formData?.credentials?.[item?.value]?.length)) || !((formData?.credentials?.[item?.value]?.length) < item?.max)}
                        valid={formData?.credentials?.[item?.value] && (item?.min < (formData?.credentials?.[item?.value]?.length) < item?.max)}
                      />
                      {error.some(t => t === item?.value) && <small className="text-danger">
                        <span className="pr-1">  <FaTimesCircle /></span>

                        {item?.name + " را بصورت صحیح وارد کنید "}
                      </small>}
                    </Col>
                  </FormGroup>

                  </div>
                )
              }
              {
                FormListInput.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                    <Label
                      htmlFor="projectname"
                      className="col-form-label col-lg-2"
                    >
                      {item?.name}
                    </Label>
                    <Col lg="10">
                      <Input
                        id={item?.value}
                        name={item?.value}
                        type={item?.type || "text"}
                        className="form-control"
                        placeholder={item?.name + " ..."}
                        onChange={addData}
                        defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                        invalid={!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.length)) || !((formData?.[item?.value]?.length) < item?.max)}
                        valid={formData?.[item?.value] && (item?.min < (formData?.[item?.value]?.length) < item?.max)}
                      />
                      {error.some(t => t === item?.value) && <small className="text-danger">
                        <span className="pr-1">  <FaTimesCircle /></span>

                        {item?.name + " را بصورت صحیح وارد کنید "}
                      </small>}
                    </Col>
                  </FormGroup>

                  </div>
                )
              }
              <div><FormGroup className="mb-4" row>
                <Label
                  htmlFor="projectname"
                  className="col-form-label col-lg-2">
                  تاریخ تولد
                </Label>
                <Col lg="10">

                  <DatePicker
                    className="form-control"
                    value={formData?.credentials?.birth_date ? moment(formData?.credentials?.birth_date, "YYYY-MM-DD") : moment()}
                    timePicker={false}
                    isGregorian={false}
                    onChange={(value) => {
                      if (value) {
                        let t = formData?.credentials
                        t && (t["birth_date"] = value.format("YYYY-MM-DD"))
                      }
                    }}
                    removable={true}
                  />
                  {error.some(t => t === "birth_date") && <small className="text-danger">
                    <span className="pr-1">  <FaTimesCircle /></span>

                    {"تاریخ تولد را بصورت صحیح وارد کنید "}
                  </small>}
                </Col>
              </FormGroup>

              </div>
              {/*--------------------------------------------------bank-------------------------------------------------*/}
              <CardTitle className="mb-2 mt-5">حساب بانکی </CardTitle>
              
              <InfoDynamicBank formData={formData} setFormData={setFormData} bankAccount={bankAccount}/>

              {/*--------------------------------------------------upload-------------------------------------------------*/}
              <CardTitle className="mb-0 mt-5">تصاویر </CardTitle>
              <FormGroup className="my-3" row>
                <Label
                  htmlFor="projectname"
                  className="col-form-label col-lg-2"
                >
                  تصویر پروفایل
                </Label>
                  <Row>
                    <Col lg="10" >
                      <button type="button" onClick={() => setModal(true)}>
                        <img className="img-fluid mr-3"
                            src={
                              formData?.credentials?.auth_picture?.url ||
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXp3DxP80ArpRzsB0XWBG9Ow5GeuefbLrUHw&usqp=CAU"}
                            style={{ maxWidth: "300px" }} alt="profile" />
                      </button>
                      <a href={formData?.credentials?.auth_picture?.url || "#"} target="_blank">دانلود تصویر</a>

                    </Col>
                    
                      <UploadAuthPic  setFormData={setFormData} formData={formData}/>
                   
                  </Row>
              </FormGroup>
              <div className="text-right">
                <Button type="button" color="info" className="mr-2 px-2" onClick={sendMessage}>
                  نقص اطلاعات
                </Button>
                <Button type="button" color="primary" className="mr-2 px-2" onClick={() => create("PUT")}>
                  ویرایش اطلاعات
                </Button>
                <Button type="button" color={formData?.is_approved ? "danger" : "success"} className="px-2 mr-2"
                        onClick={() => {

                          fetcher(GET_USER_ChECK(props?.match?.params?.id), {
                            method: "GET"
                          }).then(r => {
                            getDetail()
                            MessageToast("success", "باموفقیت انجام شد")
                          }).catch(e => {
                            MessageToast("error", "دوباره تلاش کنید")
                          })

                        }}>
                  {formData?.is_approved ? " لغواحرازهویت" : "تایید احراز هویت"}

                </Button>
                <Button type="button" color={formData?.is_active ? "warning" : "success"} className="px-2 mr-2"
                        onClick={() => {

                          fetcher(GET_USER_ACTIVE(props?.match?.params?.id), {
                            method: "GET"
                          }).then(r => {
                            getDetail()
                            MessageToast("success", "باموفقیت انجام شد")
                          }).catch(e => {
                            MessageToast("error", "دوباره تلاش کنید")
                          })

                        }}>
                  {formData?.is_active ? " غیرفعال" : "فعال"}

                </Button>
                {/*<Button type="submit" color="primary" onClick={() => create("POST")} className="px-5">*/}
                {/*  ثبت ملک*/}
                {/*</Button>*/}
              </div>
            </Form>
            {/*--------------------------------------------------submit-------------------------------------------------*/}


          </Col>
        </Row>
      </div>
      <Modal size="xl" centered={true} isOpen={modal} toggle={() => setModal(false)}>

        <div className="p-4">
          <div className="row">
            <div className="col-12">
              <img className="img-fluid mr-3 w-100"
                   src={
                     formData?.credentials?.auth_picture?.url ||
                     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXp3DxP80ArpRzsB0XWBG9Ow5GeuefbLrUHw&usqp=CAU"}
                   alt="profile" />
            </div>
            <div className="col-12 text-center mt-2">
              <Button type="button" color="primary" className="px-4"
                      onClick={() => {
                        setModal(false)
                      }}>بستن</Button>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default withRouter(Profile)