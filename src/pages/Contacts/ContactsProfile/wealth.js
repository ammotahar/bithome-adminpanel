import React, { useEffect, useState } from "react"
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Table
} from "reactstrap"
import { FaCheck, FaCircle } from "react-icons/all"
import DonutChart from "./dountchart"
import Select from "react-select"
import { fetcher } from "../../../store/common/action"
import { POST_USER_ESTATE } from "../../../helpers/url_helper"
import { withRouter } from "react-router-dom"
import moment from "moment-jalaali"
import PropTypes from "prop-types"
import { getMoney } from "store/auth/profile/actions"
import { connect } from "react-redux"
import numberWithCommas from "../../../components/threeNumber"
import Status from "../../../components/status"
import iconDoc from "../../../assets/images/icon-doc-Trade.svg"
import axios from "axios"
import PrintRowDoctrade from "./printRowDoctrade"
import iconPrint from 'assets/images/print.svg'

const Wealth = (props) => {
  const [formData, setFormData] = useState([])
  const [profit, setProfit] = useState(0)
  const [estimation, setEstimation] = useState(0)
  const [stock, setStock] = useState([])
  const [rowDoc, setRowDoc] = useState()
  const [readyDownload , setReadyDownload] = useState(false)
  const { getMoney, money } = props

  useEffect(() => {
    getData()
  }, [])

  useEffect(() => {
      if(readyDownload === null){
        window.location.reload()
      }
  }, [])


  let purchaced_values = formData.reduce((accumulator, currentValue) => {
    return accumulator + currentValue?.purchaced_value
}, 0)

  const getData = () => {
    fetcher(POST_USER_ESTATE(props?.match?.params?.id), {
      method: "GET"
    }).then(r => {
      if (r?.result) {
        setFormData(r?.result)
        let est = 0
        let prf = 0
        let stk = []
        r?.result.map(t => {
          est += Number(t?.estate_price)
          prf += Number(t?.gain_loss)
          stk.push(t?.share_from_total)
        })
        setEstimation(est)
        setProfit(prf)
        setStock(stk)
      }
    }).catch(e => {
      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }

  // get link pdf of doc trade
  const handleDownloadDoc = (user_id , estate_id) => {
      fetcher((`/v1/admin/users/${user_id}/estates/${estate_id}/download/`), 
        {method: "GET"})
        .then(res => {

          console.log("OK Download -> ",res);
          if(res){
            console.log("RESSSSS -----" , res);
            window.location.href = res;
          }
        }).catch(e => {
          // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
        })
      }
      
      // get data entry of row doc trade
      const handleDownloadRowDoc = (user_id , estate_id) => {
        fetcher((`/v1/admin/users/${user_id}/estates/${estate_id}/download/?json=true`), 
        {method: "GET"},{ noAuth: true, toText: true }).then(res => {
          
          if(res.result){
            setRowDoc(res.result)
            setReadyDownload(true)
          }
            
      }).catch(e => {
        // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
      })
    }
  
console.log("Row_Doc ---" , rowDoc);

  return (
    <>
      <div>
        <Row>
          <Col lg="12">


            <CardTitle className="mb-4">دارایی ها</CardTitle>
            <div className="border pt-3 px-3 rounded ">
              <div className="row">
                <div className="col-12 col-xl-6 ">
                  <p>
                    <span className="mr-2 text-primary"><FaCircle /></span>
                    <span>ارزش تخمینی کل دارایی ها براساس آخرین قیمت های فروش </span>
                    <span className="mx-1">:</span>
                    {/* <b>{numberWithCommas(estimation)}</b> */}
                    <b>{numberWithCommas(purchaced_values)}</b>
                    <span> تومان </span>
                  </p>
                </div>
                <div className="col-12 col-xl-6 ">
                  <p>
                    <span className="mr-2 text-primary"><FaCircle /></span>
                    <span>میزان سود براساس مجموع ملک های شما </span>
                    <span className="mx-1">:</span>
                    <b dir="ltr">{profit}</b>
                    <span> تومان </span>
                  </p>
                </div>
                <div className="col-12 col-xl-6 ">
                  <p>
                    <span className="mr-2 text-primary"><FaCircle /></span>
                    <span>تعداد نماد </span>
                    <span className="mx-1">:</span>
                    <b>{formData?.length}</b>
                    <span> نماد </span>
                  </p>
                </div>
                <div className="col-12 col-xl-6 ">
                  <p>
                    <strong className="mr-2 text-primary"><FaCircle /></strong>
                    <strong>موجودی کیف پول </strong>
                    <strong className="mx-1">:</strong>
                    <strong>     {numberWithCommas(money?.inventory)}</strong>
                    <strong> تومان </strong>
                  </p>
                </div>
              </div>
            </div>

            {/*<div className="row align-items-center justify-content-end mt-4">*/}
            {/*  <div className="col-5 text-right">*/}
            {/*    <span className="d-inline-block m-0">دسته بندی براساس :</span></div>*/}
            {/*  <div className="col-6 col-lg-5 col-xl-4">*/}
            {/*    <select*/}
            {/*      className="form-control"*/}
            {/*      name="user_id"*/}
            {/*      id="user_id"*/}
            {/*      placeholder="انتخاب  ..."*/}
            {/*    >*/}
            {/*      <option value="1">1</option>*/}
            {/*      <option value="1">2</option>*/}
            {/*      <option value="1">1</option>*/}
            {/*      <option value="1">1</option>*/}
            {/*    </select>*/}
            {/*  </div>*/}

            {/*</div>*/}
            <div className="">
              <div className="table-responsive">
                <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                  <thead className="thead-light">
                  <tr>
                    <th scope="col">
                      کدملک
                    </th>
                    <th scope="col"> عنوان ملک</th>
                    {/* <th scope="col"> تاریخ</th> */}
                    <th scope="col"> متراژخریداری شده</th>
                    <th scope="col"> متراژ
                      <small>(مترمربع)</small>
                    </th>
                    <th scope="col"> میانگین خرید
                      <small>(تومان بر متر مربع)</small>
                    </th>
                    <th scope="col">
                      بهای تمام شده خرید
                      <small>(تومان)</small>
                    </th>

                    <th scope="col">ارزش روز
                      <small>(تومان)</small>
                    </th>
                    <th scope="col"> سود و زیان
                      <small>(تومان)</small>
                    </th>
                    <th scope="col"> سهم از کل دارایی</th>
                    <th scope="col"> دانلود سند</th>
                    <th scope="col"> پرینت سند</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    formData.map((item, i) =>
                      <tr>
                        <td>{item?.estate_identifier}</td>
                        <td>{item?.title}</td>
                        {/* <td>{moment(item?.created_at, "YYYY-MM-DDThh:mm").format("jYYYY/jMM/jDD hh:mm")}</td> */}
                        <td>
                          {item?.purchaced_area}

                          {/*<Status name={item?.status || ''} />*/}


                        </td>
                        <td>{parseInt(item?.area)}</td>

                        <td>{numberWithCommas(item?.avg_buy_price)}</td>
                        <td>{numberWithCommas(item?.calculated_value)}</td>
                        <td>
                          {numberWithCommas(item?.purchaced_value)}
                        </td>
                        <td dir="ltr">{item?.gain_loss}</td>
                        <td>{item?.share_from_total}</td>

                        <td className="text-center">
                          <img className="icon-doc " onClick={(e)=>handleDownloadDoc(props.match.params.id , item?.id)} src={iconDoc}  alt="icon-doc"/>
                        </td>
                        <td className="text-center">
                          <img className="icon-doc" onClick={(e)=>handleDownloadRowDoc(props.match.params.id , item?.id)} src={iconPrint}  alt="icon-doc"/> 
                          
                          
                        </td>
                      </tr>)
                  }
                  </tbody>
                </Table>
              </div>


              {/* component of handle show preview of print after click icon */}
              {readyDownload &&   
                <PrintRowDoctrade 
                  rowDoc={rowDoc} 
                  readyDownload={readyDownload} 
                  setReadyDownload={setReadyDownload}
              />
              }

            



              <CardTitle className="my-4 "> نمودار درصد سهم ها</CardTitle>
              {stock?.length && <DonutChart stock={stock} data={formData} />}
            </div>

          </Col>
        </Row>


      </div>
    </>
  )
}
Wealth.propTypes = {
  money: PropTypes.object,
  getMoney: PropTypes.func
}

const mapStateToProps = ({ Profile }) => ({
  money: Profile.money?.result || {}
})

const mapDispatchToProps = dispatch => ({
  getMoney: (...data) => dispatch(getMoney(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Wealth))
