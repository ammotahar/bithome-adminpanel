import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"
import { map } from "lodash"
import {
  Card,
  CardBody,
  CardTitle,
  Col,
  Container, Form,
  Media,
  Row,
  Table
} from "reactstrap"

//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"
import { fetcher, getUserProfile } from "store/actions"

import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap"
import classnames from "classnames"
import Profile from "pages/Contacts/ContactsProfile/profile"
import Charge from "pages/Contacts/ContactsProfile/charge"
import Wealth from "pages/Contacts/ContactsProfile/wealth"
import Order from "pages/Contacts/ContactsProfile/order"
import History from "pages/Contacts/ContactsProfile/history"
import Payment from "pages/Contacts/ContactsProfile/payment"
import Enter from "pages/Contacts/ContactsProfile/enter"
import { GGET_USERS_DETAIL } from "../../../helpers/url_helper"
import { MessageToast } from "../../../utils/message"
const tab_list = [
  {title:"اطلاعات کاربری",value:"1",tabPane:<Profile/>},
  {title:"شارژکیف پول",value:"2",tabPane:<Charge/>},
  {title:"دارایی ها",value:"3",tabPane:<Wealth/>},
  {title:"سفارش ها",value:"4",tabPane:<Order/>},
  {title:"تاریخچه معاملات",value:"5",tabPane:<History/>},
  {title:"تاریخچه تراکنش ها",value:"6",tabPane:<Payment/>},
  {title:"تاریخچه ورود",value:"7",tabPane:<Enter/>},
]
const Index = props => {
  const [activeTab, setActiveTab] = useState("1")
  const [formData, setFormData] = useState({})
  const { userProfile, onGetUserProfile } = props
  // eslint-disable-next-line no-unused-vars


  useEffect(() => {
    getDetail()
  }, [])
  const getDetail = () => {
    fetcher(GGET_USERS_DETAIL(props?.match?.params?.id), {
      method: "GET"
    }).then(r => {
      if (r?.result) {
        let t = r?.result
        setFormData(t)
      }
    }).catch(e => {

      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="کاربران" breadcrumbItem={(formData?.credentials?.first_name || "")+ " " +(formData?.credentials?.last_name || "") } />

          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <Nav
                    className="pr-0 mb-3 tabs d-flex align-items-center justify-content-between ">
                    {
                      tab_list.map((tab,i)=> <NavItem className="flex-grow-1">
                        <NavLink
                          className={classnames({ active: activeTab === (i+1).toString()})}
                          onClick={() => {
                            setActiveTab((i+1).toString())
                          }}
                        >
                          {tab.title}
                        </NavLink>
                      </NavItem>)
                    }
                  </Nav>
                  <TabContent activeTab={activeTab}>
                    {
                      tab_list.map((tab,i)=>
                        <TabPane tabId={(i+1).toString()}>
                          {tab.tabPane}
                      </TabPane>)}


                  </TabContent>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

Index.propTypes = {
  // userProfile: PropTypes.any,
  // onGetUserProfile: PropTypes.func
}

const mapStateToProps = ({ contacts }) => ({
  // userProfile: contacts.userProfile
})

const mapDispatchToProps = dispatch => ({
  // onGetUserProfile: () => dispatch(getUserProfile())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Index))
