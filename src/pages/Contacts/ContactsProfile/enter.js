import React, { useEffect, useState } from "react"
import {
  CardTitle,
  Col,
  Row,
  Table
} from "reactstrap"
import { fetcher } from "store/common/action"
import { POST_USER_History_ENTER } from "helpers/url_helper"
import { withRouter } from "react-router-dom"
import moment from "moment-jalaali";
import Status from "components/status"
import numberWithCommas from "components/threeNumber"
import PaginationCustom from "../../../components/paginationZero"

const Payment = (props) => {
  const [formData, setFormData] = useState([])
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [listCount, setListCount] = useState(0)
  useEffect(() => {
    getData({ query: {page: page, size: pageSize } })
  }, [])
  const getData = (query) => {
    fetcher(POST_USER_History_ENTER(props?.match?.params?.id), {
      method: "GET"
    },query).then(r => {
      if (r?.result) {
        setFormData(r?.result?.items)
      }
    }).catch(e => {
      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }
  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    getData({ query: { page: p, size: s } })
  }
  return (
    <>
      <div>
        <Row>
          <Col lg="12">


            <CardTitle className="mb-4"> تاریخچه  ورود</CardTitle>

            {/*<div className="row align-items-center justify-content-end mt-4">*/}
            {/*  <div className="col-5 text-right">*/}
            {/*    <span className="d-inline-block m-0">دسته بندی براساس :</span></div>*/}
            {/*  <div className="col-6 col-lg-5 col-xl-4">*/}
            {/*    <select*/}
            {/*      className="form-control"*/}
            {/*      name="user_id"*/}
            {/*      id="user_id"*/}
            {/*      placeholder="انتخاب  ..."*/}
            {/*    >*/}
            {/*      <option value="1">1</option>*/}
            {/*      <option value="1">2</option>*/}
            {/*      <option value="1">1</option>*/}
            {/*      <option value="1">1</option>*/}
            {/*    </select>*/}
            {/*  </div>*/}

            {/*</div>*/}
            <div className="">
              <div className="table-responsive">
                <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                  <thead className="thead-light">
                  <tr>
                    <th scope="col">تاریخ</th>

                    <th scope="col">  آدرس IP</th>

                    <th scope="col">  شناسه مرورگر</th>
                    <th scope="col">   پلتفرم</th>


                    <th scope="col"> وضعیت</th>

                  </tr>
                  </thead>
                  <tbody>
                  {formData?.length &&
                    formData.map((item, i) =>
                      <tr>
                        <td>{moment(item?.created_at, "YYYY-MM-DDThh:mm").format("jYYYY/jMM/jDD hh:mm")}</td>
                        <td>{item?.user_ip}</td>
                        <td>{item?.browser}</td>
                        <td>{item?.platform}</td>
                        <td>
                          {item?.login_successful ? <span className="font-size-12 badge-soft-success badge badge-success badge-pill"> موفقیت آمیز</span>
                          :<span className="font-size-12 badge-soft-danger badge badge-danger badge-pill">   ناموفق</span>}
                        </td>

                      </tr>)
                  }
                  </tbody>
                </Table>
              </div>

            </div>

          </Col>
        </Row>
        <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor((listCount) / pageSize ) + 1}
                          addPageSize={setPageSize}
                          pageSize={pageSize}
                          chatsCount={listCount} />

      </div>
    </>
  )
}

export default withRouter(Payment)