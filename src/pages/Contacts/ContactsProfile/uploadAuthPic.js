import React , {useState} from 'react';
import Dropzone from "react-dropzone";
import { uploadAuthFile} from "../../../store/projects/function";
import { fetcher } from "../../../store/common/action";
import { media, media_types, projects_detail, projects_types } from "../../../utils/constant";
import {
    Col,
    Form,
  } from "reactstrap"

const initialImg = {
    "title": null,
    "media_type": null,
    img: null,
    loading: false
  }

function UploadAuthPic(props) {

    const [image, setImage] = useState(initialImg)
    // const [mediaType, setMediaType] = useState([])
    const { error, setError, selectedFiles, setselectedFiles, formData ,delete_Media , setFormData} = props


    function handleAcceptedFiles(files) {
        console.log(props)
          setImage({ ...image, loading: true })
          uploadAuthFile(files, image).then(r => {

              // console.log("file_is_upload ::: >>> ",files);
              // console.log("result ::: >>> ",r);

            if (r?.result) {
              console.log("Result  :::: ",r?.result);
              setImage({ ...image, img: (files[0]), ...r?.result, loading: false });
              setFormData({...formData , credentials : { ...formData?.credentials,  auth_picture_id : r?.result?.id}})
            } else {
              setImage({ ...image, loading: false })
            }
          }).catch(e => {
            setImage({ ...image, loading: false })
          })
    
        
      }

      console.log("IIIIIMMMMage :?>> ",image);


    return (
        <>
            <Col lg="5" className="mt-4">
            <Form>
            
            <Dropzone
                onDrop={acceptedFiles => {
                handleAcceptedFiles(acceptedFiles)
                }}
            >
                {({ getRootProps, getInputProps }) => (
                <div className="dropzone">
                    <div
                    className="dz-message needsclick"
                    {...getRootProps()}
                    >
                    <input {...getInputProps()} />

                    <div className="dz-message needsclick">

                        {
                        image?.loading ?
                            <div className="text-center my-3">
                                        <span className="text-success">
                                            <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />
                                        درحال دریافت تصویر ...
                                        </span>
                            </div>
                            :
                            (image?.img ? <img style={{width : '100%' , height : '100%'}} src={URL.createObjectURL(image?.img)} alt="" /> :
                            <>
                                <div className="mb-3">
                                <i className="display-4 text-muted bx bxs-cloud-upload" />
                                </div>
                                <h4>فایل را بکشید یا کلیک کنید</h4>
                            </>)
                        }

                    </div>
                    </div>
                </div>
                )}
            </Dropzone>

            </Form>
      </Col>
        </>
    )
}

export default UploadAuthPic;
