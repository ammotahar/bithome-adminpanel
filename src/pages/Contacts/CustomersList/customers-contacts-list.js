import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import {Tooltip} from 'antd'
import { Link, withRouter } from "react-router-dom"
import { BiSortDown , BiSortUp , BiSort} from "react-icons/bi";
import {
  Button,
  Col,
  Container,
  Modal,
  Row, Spinner,
  Table
} from "reactstrap"
import Breadcrumbs from "components/Common/Breadcrumb"
import { getUsers } from "store/contacts/actions"
import { fetcher } from "store/common/action"
import { GET_CUSTOMERS_LIST } from "helpers/url_helper"
import Filter from "./filter"
import PaginationCustom from "../../../components/paginationZero"
import numberWithCommas from "components/threeNumber"
import moment from "moment-jalaali"

const Action = (props) => {
  const [modal, setModal] = useState(false)
  return <div dir="ltr">
    <Button color="danger" href="#" onClick={() => setModal(true)}>
      حذف
    </Button>
    <Modal centered={true} isOpen={modal} toggle={() => setModal(false)}>
      <div className="p-4">
        <p className="pb-4">آیا از حذف این کاربر اطمینان دارید؟</p>
        <div className="row">

          <div className="col-6">
            <Button type="button" color="danger" className="w-100"
                    onClick={() => setModal(false)}>خیر</Button>
          </div>

          <div className="col-6">
            <Button type="button" color="primary" className="w-100"
                    onClick={() => {
                      // props.deleteProject()
                      setModal(false)
                    }}>بله</Button>
          </div>
        </div>
      </div>
    </Modal>
  </div>
}



const CustomersContactsList = props => {
  const { users, onGetUsers } = props
  const [customerList, setCustomerList] = useState([])
  const [params, setParams] = useState({})
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)
  const [order, setOrder] = useState("-updated_at")
  const [orderIndicator, setOrderIndicator] = useState(<BiSort/>)


  useEffect(() => {
    getCustomersList()
  }, [params, order])

  useEffect(() => {
    setLoading(false)
  }, [users])


  // get list reports log of contacts 
  const getCustomersList = (p = 0, s = pageSize) => {
    setLoading(true)
    fetcher(GET_CUSTOMERS_LIST, {
      method: "GET"
    },{query:{page: p, size: s , order,  ...params}}).then(r => {
    if (r?.result) {
        setCustomerList(r?.result?.items)
        setCounts(r?.result?.total)
        setLoading(false)
    }
  }).catch(e => {
    // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
  })
}



  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getCustomersList(p, s)
  }


  // function for handle set state of order when click on title header
  const handleChangeOrder = (data) =>{
     setOrder( state => orderStateChange(state, data) )
  }

  // function that get previos & current state of order for show carret icon sort and change order state
  const orderStateChange = (prevState, orderState) => {
    // when first time click on title header field that has filter, 'carret down' show and 'sort ascending'
    if(prevState === orderState){
      setOrderIndicator(<BiSortDown/>)
      return `-${orderState}` ;
      // when third time click on title header field that has filter, remove fileter and show 'default icon filter'
    }else if (prevState === `-${orderState}`){
      setOrderIndicator(<BiSort/>)
      return "-updated_at" ;
      // when second time click on title header field that has filter, 'carret up' show and 'sort descending'
    }else {
      setOrderIndicator(<BiSortUp/>)
      return orderState ;
    }
  }

 
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="کاربران" breadcrumbItem="لیست مشتریان" />

          <Filter params={params} setParams={setParams} getUsers={getCustomersList}  />

          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">ردیف</th>
                      <th scope="col"> نام کاربر</th>
                      <th scope="col"> شماره ملی</th>
                      <th scope="col"> موبایل</th>
                      <th scope="col" className="text-center"> 
                      <Tooltip title="مرتب‌سازی بر اساس شارژ حساب" color={'geekblue'} ><span 
                          className="field-header-filter" 
                          onClick={ e => handleChangeOrder("inventory")}>
                          {order === "-updated_at" ?  orderIndicator : ''}{'  '}
                          { order.includes("inventory") && orderIndicator }
                          <span className="ml-1">شارژ حساب</span>
                        </span>
                        </Tooltip>
                      </th>
                      <th scope="col" className="text-center">
                      <Tooltip title="مرتب‌سازی بر اساس تاریخ معامله" color={'geekblue'} >
                        <span className="field-header-filter" onClick={ e => handleChangeOrder("trade_date")}>
                          {order === "-updated_at" ?  orderIndicator : ''}{'  '}
                          { order.includes("trade_date") && orderIndicator }
                          <span className="ml-1">تاریخ معامله</span>
                        </span> 
                        </Tooltip>
                      </th>
                      <th scope="col" className="text-center"> 
                      <Tooltip title="مرتب‌سازی بر اساس آخرین فعالیت" color={'geekblue'} >
                        <span 
                          className="field-header-filter" 
                          onClick={ e => handleChangeOrder("login_date")}>
                          {order === "-updated_at" ?  orderIndicator : ''}{'  '}
                          {order.includes("login_date") && orderIndicator}
                          <span className="ml-1">آخرین فعالیت</span>
                        </span>
                        </Tooltip>
                      </th>
                      <th scope="col" className="text-right"> فعال</th>
                      <th scope="col" className="text-right"> احرازهویت</th>
                      <th scope="col" className="text-center">جزییات</th>
                    </tr>
                    </thead>
                    <tbody>
                    {customerList?.length &&
                      customerList?.map((item, i) =>
                        <tr>
                          <td className="text-center"> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          <td>{(item?.credentials?.first_name || "") + " " + (item?.credentials?.last_name || "")}</td>
                          <td>{item?.national_code}</td>
                          <td>{item?.mobile}</td>
                          <td className="text-center">{item?.inventory ? numberWithCommas(item?.inventory) : ''}</td>
                          <td className="text-center">{item?.trade_date ? moment(item?.trade_date).format('jYYYY/jMM/jDD') : ''}</td>
                          <td className="text-center">{item?.profile?.last_login ? moment(item?.profile?.last_login).format('jYYYY/jMM/jDD') : ''}</td>
                          <td className="text-right">
                            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id={"customCheckcolorUser" + (i + 1)}
                                checked={item?.is_active}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor={"customCheckcolorUser" + (i + 1)}
                              >
                              </label>
                            </div>
                          </td>
                          <td className="text-right">
                            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id={"customCheckcolorUser" + (i + 1)}
                                checked={item?.is_approved}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor={"customCheckcolorUser" + (i + 1)}
                              >
                              </label>
                            </div>
                          </td>
                          <td className="text-center">
                            <Button color="primary">
                              <Link 
                                className="text-white"
                                to={"/contacts-profile/" + item?.id}>جزییات
                              </Link></Button>
                          </td>
                        </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>

          <PaginationCustom 
            handlePageClick={handlePageClick} 
            page={page}
            totalPage={Math.floor(counts / pageSize) + 1}
            addPageSize={setPageSize}
            pageSize={pageSize}
            chatsCount={counts} 
          />

          <div className="text-center">
            {loading ? <Spinner /> : ""}
          </div>

        </Container>
      </div>
    </React.Fragment>
  )
}

CustomersContactsList.propTypes = {
  users: PropTypes.array,
  onGetUsers: PropTypes.func
}

const mapStateToProps = ({ contacts }) => ({
  users: contacts.users?.result || []
})

const mapDispatchToProps = dispatch => ({
  onGetUsers: (...data) => dispatch(getUsers(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CustomersContactsList))
