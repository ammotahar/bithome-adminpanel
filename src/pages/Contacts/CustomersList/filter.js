import React, { useEffect, useState } from "react"
import { Button, Col, Form, Row } from "reactstrap"
import queryString from "query-string"
import { DOWNLOAD_CUSTOMER_FILE_EXCEL } from "helpers/url_helper"
import { exportExcelFile } from "utils/exportFiles"

const Filter = props => {
  const [remove, setRemove] = useState(false)
  const [created_at, setCreated_at] = useState(null)
  const [updated_at, setUpdated_at] = useState(null)

  const addFilter = e => {
    setRemove(false)
    props.setParams({
      ...props.params,
      page: 0,
      [e.target.name]: e.target.value,
    })
  }

  useEffect(() => {
    if (remove) props.getUsers()
  }, [props.params])

  // function for download excel file of 'customers'
  const handleExportExcelFile = () => {
    const queries = queryString.stringify(props.params)
    exportExcelFile(`${DOWNLOAD_CUSTOMER_FILE_EXCEL}?${queries}`, "customers.xlsx")
  }

  return (
    <>
      <Form id="frm">
        <Row>
          <Col lg="4" xl="3" className="mt-2">
            <label>جستجو</label>
            <input
              type="text"
              className="form-control"
              name="name"
              placeholder="جستجو"
              onChange={addFilter}
            />
          </Col>
          <Col lg="" xl="" className="mt-2">
            <label className="text-white d-block"> .</label>
            <Button color="primary" onClick={() => props.getUsers()}>
              اعمال فیلتر
            </Button>
            <Button
              color="btn btn-outline-danger"
              className="mx-2"
              onClick={() => {
                document.getElementById("frm").reset()
                setCreated_at(null)
                setUpdated_at(null)
                props.setParams({ order: "-updated_at" })
                setRemove(true)
              }}
            >
              حذف فیلتر
            </Button>
            <Button color="success" onClick={handleExportExcelFile}>
              خروجی اکسل
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  )
}

export default Filter
