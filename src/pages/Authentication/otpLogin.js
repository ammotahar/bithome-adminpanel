import PropTypes from "prop-types"
import React, { useEffect, useState } from "react"

import { Row, Col, CardBody, Card, Alert, Container } from "reactstrap"

// Redux
import { connect } from "react-redux"
import { withRouter, Link, Redirect } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

//Social Media Imports
import { GoogleLogin } from "react-google-login"
// import TwitterLogin from "react-twitter-auth"
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props"

// actions
import { postLoginOtpUser, apiError, socialLogin } from "store/actions"

// import images
import profile from "assets/images/profile-img.png"
import logo from "assets/images/logo.svg"


const OtpLogin = props => {
  // handleValidSubmit
  const NumberTime=60;
  const [time, setTime] = useState(NumberTime)
  const [intervalId, setIntervalId] = useState(null)
  const handleValidSubmit = (event, values) => {
    if (props.data)
      props.postLoginOtpUser({ ...values, ...props.data }, props.history)
    else
      props.setGetCode(false)
  }
  useEffect(()=>{
    let myInterval = setInterval(() => {
      if (time > 0) {
        setTime(time - 1);
      }
      if (time === 0) {
          clearInterval(myInterval)
      }
    }, 1000)
    return ()=> {
      clearInterval(myInterval);
    };
  });

  return (
    <React.Fragment>
      {(props.loading || props.loadingFirstLogin) ? <div className="loading">
        <i style={{ fontSize: "4rem" }} className="bx bx-loader bx-spin align-middle "/>
      </div> : ""}
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="fas fa-home h2"/>
        </Link>
      </div>
      <div className="account-pages my-5 pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <div className="bg-soft-primary">
                  <Row>
                    <Col className="col-7">
                      <div className="text-primary p-4">
                        <h5 className="text-primary">خوش آمدید !</h5>
                        <p>ورود به پنل ادمین متریوم</p>
                      </div>
                    </Col>
                    <Col className="col-5 align-self-end">
                      <img src={profile} alt="" className="img-fluid"/>
                    </Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logo}
                            alt=""
                            className="rounded-circle"
                            height="34"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div>
                   <span>کد تایید به شماره </span>
                   <b>{props.successFirstLogin?.mobile_number}</b>
                   <span>  ارسال شد </span>
                  </div>
                  <div className="p-2">
                    <AvForm
                      className="form-horizontal"
                      onValidSubmit={(e, v) => {
                        handleValidSubmit(e, v)
                      }}
                    >
                      {props.error && typeof props.error === "string" ? (
                        <Alert color="danger">{props.error}</Alert>
                      ) : null}

                      <div className="form-group">
                        <AvField
                          name="otp"
                          label="کد تایید "
                          // value="admin@themesbrand.com"
                          className="form-control"
                          placeholder="کد تایید..."
                          type="text"
                          required
                        />
                      </div>

                      {/*<div className="custom-control custom-checkbox">*/}
                      {/*  <input*/}
                      {/*    type="checkbox"*/}
                      {/*    className="custom-control-input"*/}
                      {/*    id="customControlInline"*/}
                      {/*  />*/}
                      {/*  <label*/}
                      {/*    className="custom-control-label"*/}
                      {/*    htmlFor="customControlInline"*/}
                      {/*  >*/}
                      {/*    مرا به خاطر بسپار*/}
                      {/*  </label>*/}
                      {/*</div>*/}
                      <div className="mt-4 text-center">
                        <span className="d-inline-block" style={{ minWidth: "20px" }}>{time}</span>
                        <span>ثانیه</span>
                      </div>
                      <div className="mt-3">
                        <button
                          className="btn btn-primary btn-block waves-effect waves-light"
                          type="submit"
                        >
                          ورود
                        </button>
                      </div>


                      <div className="mt-2 text-center">
                        <button type="button" className="btn btn-link" disabled={time>0} onClick={() => {
                          if (props.data){
                            setTime(NumberTime)
                            props.handleValidSubmit("", props.data)
                          }
                          else
                            props.setGetCode(false)
                        }}>
                          <i className="mdi mdi-lock mr-1"/>
                          ارسال مجدد کد
                        </button>
                      </div>
                      <div className="mt-2 text-center">
                        <button type="button" className="btn btn-link"  onClick={() => {
                            props.setGetCode(false)
                        }}>
                          <i className="mdi mdi-lock mr-1"/>
                         تغییر شماره همراه
                        </button>
                      </div>
                      {/*<div className="mt-4 text-center">*/}
                      {/*  <Link to="/forgot-password" className="text-muted">*/}
                      {/*    <i className="mdi mdi-lock mr-1" />*/}
                      {/*    فراموشی رمز عبور*/}
                      {/*  </Link>*/}
                      {/*</div>*/}
                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              {/*<div className="mt-5 text-center">*/}
              {/*  <p>*/}
              {/*    حساب کاری دارید؟*/}
              {/*    <Link*/}
              {/*      to="register"*/}
              {/*      className="font-weight-medium text-primary"*/}
              {/*    >*/}
              {/*      حساب کاربری*/}
              {/*    </Link>{" "}*/}
              {/*  </p>*/}
              {/*  <p>*/}
              {/*    متریوم*/}
              {/*  </p>*/}
              {/*</div>*/}
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  const { error, success, loading } = state.Login
  const { loading:loadingFirstLogin, success:successFirstLogin} = state.Login.firstLogin
  return { error, success, loading,loadingFirstLogin ,successFirstLogin}
}

export default withRouter(
  connect(mapStateToProps, { postLoginOtpUser, apiError, socialLogin })(OtpLogin)
)

OtpLogin.propTypes = {
  error: PropTypes.any,
  history: PropTypes.object,
  postLoginOtpUser: PropTypes.func,
  socialLogin: PropTypes.func,
  success: PropTypes.object
}
