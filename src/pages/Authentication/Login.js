import PropTypes from "prop-types"
import React, { useEffect, useState } from "react"

import { Row, Col, CardBody, Card, Alert, Container } from "reactstrap"

// Redux
import { connect } from "react-redux"
import { withRouter, Link,Redirect } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

//Social Media Imports
import { GoogleLogin } from "react-google-login"
// import TwitterLogin from "react-twitter-auth"
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props"

// actions
import { loginUser, apiError, socialLogin ,removeFirstLoginSuccess} from "store/actions"

// import images
import profile from "assets/images/profile-img.png"
import logo from "assets/images/logo.svg"
import OtpLogin from "./otpLogin"


const Login = props => {
  // handleValidSubmit
  const [getCode,setGetCode]=useState(false)
  const [data,setData]=useState(null)
  const handleValidSubmit = (event, values) => {
    props.loginUser(values, props.history)
    setData(values)
  }
  useEffect(()=>{
    return ()=>props.removeFirstLoginSuccess()
  },[])
  useEffect(()=>{
    if(props?.success){
      setGetCode(true)
    }
  },[props])
  // console.log(props)
  // if(props.success==="1"){
  //   return <Redirect to="/dashboard" />
  // }
  if(!getCode)
  return (
    <React.Fragment>
      {props.loading ? <div className="loading">
        <i style={{ fontSize: "4rem" }} className="bx bx-loader bx-spin align-middle " />
      </div> : ''}
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="fas fa-home h2" />
        </Link>
      </div>
      <div className="account-pages my-5 pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <div className="bg-soft-primary">
                  <Row>
                    <Col className="col-7">
                      <div className="text-primary p-4">
                        <h5 className="text-primary">خوش آمدید !</h5>
                        <p>ورود به پنل ادمین متریوم</p>
                      </div>
                    </Col>
                    <Col className="col-5 align-self-end">
                      <img src={profile} alt="" className="img-fluid" />
                    </Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logo}
                            alt=""
                            className="rounded-circle"
                            height="34"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div className="p-2">
                    <AvForm
                      className="form-horizontal"
                      onValidSubmit={(e, v) => {
                        handleValidSubmit(e, v)
                      }}
                    >
                      {props.error && typeof props.error === "string" ? (
                        <Alert color="danger">{props.error}</Alert>
                      ) : null}

                      <div className="form-group">
                        <AvField
                          name="input"
                          label="نام کاربری یا شماره تلفن همراه"
                          // value="admin@themesbrand.com"
                          className="form-control"
                          placeholder="نام کاربری یا شماره تلفن همراه..."
                          type="text"
                          required
                        />
                      </div>

                      <div className="form-group">
                        <AvField
                          name="password"
                          label="رمز ورود"
                          // value="123456"
                          type="password"
                          required
                          placeholder="رمز ورود..."
                        />
                      </div>

                      <div className="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customControlInline"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customControlInline"
                        >
                          مرا به خاطر بسپار
                        </label>
                      </div>

                      <div className="mt-3">
                        <button
                          className="btn btn-primary btn-block waves-effect waves-light"
                          type="submit"
                        >
                          ورود
                        </button>
                      </div>


                      {/*<div className="mt-4 text-center">*/}
                      {/*  <Link to="/forgot-password" className="text-muted">*/}
                      {/*    <i className="mdi mdi-lock mr-1" />*/}
                      {/*    فراموشی رمز عبور*/}
                      {/*  </Link>*/}
                      {/*</div>*/}
                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              {/*<div className="mt-5 text-center">*/}
              {/*  <p>*/}
              {/*    حساب کاری دارید؟*/}
              {/*    <Link*/}
              {/*      to="register"*/}
              {/*      className="font-weight-medium text-primary"*/}
              {/*    >*/}
              {/*      حساب کاربری*/}
              {/*    </Link>{" "}*/}
              {/*  </p>*/}
              {/*  <p>*/}
              {/*    متریوم*/}
              {/*  </p>*/}
              {/*</div>*/}
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
  else
    return (<OtpLogin data={data} setGetCode={setGetCode} handleValidSubmit={handleValidSubmit}/>)
}

const mapStateToProps = state => {
  const { error,success,loading } = state.Login.firstLogin
  return { error ,success,loading}
}

export default withRouter(
  connect(mapStateToProps, { loginUser, apiError, socialLogin,removeFirstLoginSuccess })(Login)
)

Login.propTypes = {
  error: PropTypes.any,
  history: PropTypes.object,
  loginUser: PropTypes.func,
  socialLogin: PropTypes.func,
  success:PropTypes.object,
}