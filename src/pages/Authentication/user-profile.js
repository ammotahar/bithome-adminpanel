import PropTypes from "prop-types"
import React, { useState, useEffect } from "react"
import {
  Container,
  Row,
  Col,
  Card,
  Alert,
  CardBody,
  Media,
  Button
} from "reactstrap"
import moment from "moment-jalaali"
// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

// Redux
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"

//Import Breadcrumb
import Breadcrumb from "../../components/Common/Breadcrumb"

// import avatar from "../../assets/images/users/avatar-1.jpg"
// actions
import { editProfile } from "../../store/actions"
import { FaCheckCircle } from "react-icons/all"

const UserProfile = props => {
  const [person, setPerson] = useState({
    "mobile": "",
    "national_code": "",
    "is_active": true,
    "is_superuser": false,
    "is_approved": true,
    "id": null,
    "bank_accounts": [],
    "credentials": {
      "first_name": "",
      "last_name": "",
      "email": "e",
      "birth_date": "",
      "phone": "",
      "address": "",
      "auth_picture_id": null,
      "phone_approved": false,
      "email_approved": false
    },
    "profile": {
      "username": "",
      "site": "",
      "commission_level": "",
      "commission_id": null,
      "last_login": "",
      "cards": []
    }
  })


  useEffect(() => {
    if (props?.success?.result) {
      setPerson(props?.success?.result)
    }
  }, [props.success])

  function handleValidSubmit(event, values) {
    props.editProfile(values)
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumb title="کاربر" breadcrumbItem="پروفایل" />

          <Row>
            <Col lg="12">
              {/*{props.error && props.error ? (*/}
              {/*  <Alert color="danger">{props.error}</Alert>*/}
              {/*) : null}*/}
              {/*{props.success && props.success ? (*/}
              {/*  <Alert color="success">{props.success}</Alert>*/}
              {/*) : null}*/}

              <Card>
                <CardBody>
                  <Media>
                    <div className="mr-3">
                      {/*<img*/}
                      {/*  src={avatar}*/}
                      {/*  alt=""*/}
                      {/*  className="avatar-md rounded-circle img-thumbnail"*/}
                      {/*/>*/}
                    </div>
                    <Media body className="align-self-center">
                      <div className=" ">
                        <h5>{person.credentials.first_name + " " + person.credentials.last_name } {(person.is_approved ?
                          <span className="text-success"><FaCheckCircle/></span> : "")}</h5>
                        <div className="">
                          <p className="my-3">
                            <span className="px-1 text-muted">    ایمیل :</span>

                            {person.credentials.email}</p>
                          <p className="mb-3">
                            <span className="px-1 text-muted">    تاریخ تولد :</span>

                            {moment(person.credentials.birth_date, "YYYY-MM-DD").format("jYYYY/jMM/jDD")}</p>
                          <p className="my-3">
                            <span className="px-1 text-muted">     تلفن :</span>
                            {person.credentials.phone}</p>
                          <p className="my-3">
                            <span className="px-1 text-muted">     آدرس :</span>
                            {person.credentials.address}</p>
                          <p className="my-3">
                            <span className="px-1 text-muted">     کدملی :</span>
                            {person.national_code}</p>
                          <p className="my-3">
                            <span className="px-1 text-muted">     موبایل :</span>
                            {person.mobile}</p>
                          <p className="my-3">
                            <span className="px-1 text-muted">     نام کاربری :</span>
                            {person.profile.username}</p>
                          <p className="my-3">
                            <span className="px-1 text-muted">     سایت  :</span>
                            {person.profile.site}</p>
                          <p className="my-3">
                            <span className="px-1 text-muted">     سطح  :</span>
                            {person.is_superuser && "سوپریوزر"}</p>

                        </div>

                      </div>
                    </Media>
                  </Media>
                </CardBody>
              </Card>
            </Col>
          </Row>

          {/*<h4 className="card-title mb-4">Change UserName</h4>*/}

          {/*<Card>*/}
          {/*  <CardBody>*/}
          {/*    <AvForm*/}
          {/*      className="form-horizontal"*/}
          {/*      onValidSubmit={(e, v) => {*/}
          {/*        handleValidSubmit(e, v)*/}
          {/*      }}*/}
          {/*    >*/}
          {/*      <div className="form-group">*/}
          {/*        <AvField*/}
          {/*          name="username"*/}
          {/*          label="UserName"*/}
          {/*          value={name}*/}
          {/*          className="form-control"*/}
          {/*          placeholder="Enter UserName"*/}
          {/*          type="text"*/}
          {/*          required*/}
          {/*        />*/}
          {/*        <AvField name="idx" value={idx} type="hidden" />*/}
          {/*      </div>*/}
          {/*      <div className="text-center mt-4">*/}
          {/*        <Button type="submit" color="danger">*/}
          {/*          Edit UserName*/}
          {/*        </Button>*/}
          {/*      </div>*/}
          {/*    </AvForm>*/}
          {/*  </CardBody>*/}
          {/*</Card>*/}
        </Container>
      </div>
    </React.Fragment>
  )
}

UserProfile.propTypes = {
  editProfile: PropTypes.func,
  error: PropTypes.any,
  success: PropTypes.any
}

const mapStatetoProps = state => {
  const { error, success } = state.Profile
  return { error, success }
}

export default withRouter(
  connect(mapStatetoProps, { editProfile })(UserProfile)
)
