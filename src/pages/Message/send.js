import React, { useEffect, useState } from "react"
import { Button, CardBody, Col, Container, Form, FormGroup, Input, Label, Row } from "reactstrap"
import Breadcrumbs from "components/Common/Breadcrumb"
import Select from "react-select"
import AsyncSelect from "react-select/async"
import { Link, Redirect } from "react-router-dom"
import { fetcher } from "../../store/common/action"
import { SEND_MESSAGE, GET_USERS_SEARCH_SEND_MESSAGE , GET_STATES_SEARCH_SEND_MESSAGE , GET_UNIT_SENDERS } from "helpers/url_helper"
import { MessageToast } from "../../utils/message"
import PropTypes from "prop-types"
import { getEstatesType, getEstatesUsers } from "../../store/estates/actions"
import { getProjects } from "../../store/projects/actions"
import { connect } from "react-redux"
import clearSelect from '../../assets/images/cancel-24px.svg'


const Send = (props) => {
  const [users, setUsers] = useState({  })
  const [next, setNext] = useState(false)
  const [checkAllUsers, setCheckAllUsers] = useState(false)
  const [disabledEstateProjectField, setDisabledEstateProjectField] = useState(true)
  const [disabledUsersField, setDisabledUsersField] = useState(true)
  const [formData, setFormData] = useState({
    "title": "",
    "body": "",
    "type": "manual",
    "pro_type": "manual",
    "sender_unit_id" : ""
  })

  
  const sendData = (e) => {
    e.preventDefault()
    if( !Object.keys(users)?.length){
      MessageToast("error", "کاربر یا ملک موردنظر را مشخص نمایید")
      return;
    }
    if(!formData?.title){
      MessageToast("error", "عنوان پیام را وارد کنید")
      return;
    }
    if(!formData?.body){
      MessageToast("error", "متن پیام را وارد کنید")
      return;
    }
    fetcher(SEND_MESSAGE, {
      method: "POST",
      body: JSON.stringify({ ...formData,...users })
    })
      .then(r => {
        if (r?.result){
          setFormData(r?.result)
          MessageToast("success", "ارسال پیام با موفقیت انجام شد")

        setTimeout(() => {
          setNext(true)
        }, 1000);
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })
  }
  const addData = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const loadOptions = (name) => {
    return fetcher(GET_USERS_SEARCH_SEND_MESSAGE, {
      method: "GET"
    }, { query: { name: name, limit: 10 } })
      .then(r => {
        if (r?.result) {
          let subject_list = []
          r?.result.map(i => subject_list.push({ label: i.credentials?.full_name, value: i.credentials?.full_name, ...i }))
          return subject_list
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })

  }
  const loadStates = (name) => {
    return fetcher(GET_STATES_SEARCH_SEND_MESSAGE, {
      method: "GET"
    }, { query: { name: name, limit: 10 } })
      .then(r => {
        if (r?.result) {
          let subject_list = []
          r?.result.map(i => subject_list.push({ label: i[0], value: i[0], ...i }))
          return subject_list
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })

  }

  const loadUnitSender = (name) => {
        return fetcher(GET_UNIT_SENDERS, {
          method: "GET"
        }, { query: { name: name, limit: 10 } })
          .then(r => {
            if (r?.result) {
              console.log("UNIt_Sender -->> " , r?.result);
              let subject_list = []
              r?.result.map(i => subject_list.push({ label: i?.title, value: i?.id, ...i }))
              return subject_list
            }
          })
          .catch(e => {
            MessageToast("error", "با خطا مواجه شدید")
          })
  }


  if (next) {
    return <Redirect to="/message-history" />
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="پیام ها" breadcrumbItem="پیام جدید " />

          <Form onSubmit={sendData}>
            <div className="form-group row ">
              <label className="col-md-2 col-form-label"> کاربر </label>
              <div className="col-md-10">
                <FormGroup className="ajax-select  mt-lg-0 select2-container">
                  <div className="custom-control custom-checkbox custom-checkbox-primary ">
                    <input
                      // name={item.value}
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheckcolorExtraAllUsers"
                      checked={formData?.all_users}
                      disabled ={(!!users.user || !!users.estate) && (disabledEstateProjectField || disabledUsersField)}
                      onChange={(e) => {
                        setUsers({ all_users:e.target.checked });
                        setCheckAllUsers(e.target.checked);
                      }}
                    />

                    <label
                      className="custom-control-label"
                      htmlFor="customCheckcolorExtraAllUsers">
                      انتخاب همه کاربران
                    </label>
                  </div>


                </FormGroup>

                <FormGroup className="ajax-select  mt-lg-0 select2-container">
                  <AsyncSelect
                    defaultOptions={true}
                    isClearable={true}
                    isDisabled={ checkAllUsers || ( !!users.estate && disabledEstateProjectField )}
                    name="subject-list"
                    id="subject-list"
                    loadOptions={loadOptions}
                    className="basic-multi-select"
                    onChange={(e) => {
                      setUsers({ user: e?.value });
                      setDisabledEstateProjectField(true);
                      
                    }
                    }
                    placeholder="جستجو کنید"
                    
                  />
                  {/* <span><img onClick={clearValue} src={clearSelect} className="clear-users-icon-select" alt="clear-icon"/></span> */}
                </FormGroup>

              </div>
            </div>
            <div className="form-group row">
              <label className="col-md-2 col-form-label">مالک (ملک یا پروژه)</label>
              <div className="col-md-10">
                <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                  <AsyncSelect
                    defaultOptions={true}
                    isClearable={true}
                    isDisabled={checkAllUsers  ||  (!!users.user && disabledUsersField) }
                    name="subject-list"
                    id="subject-list"
                    loadOptions={loadStates}
                    className="basic-multi-select"
                    onChange={(e) => {
                      setUsers({ estate: e?.value });
                      setDisabledUsersField(true);
                      
                    }}
                    placeholder="جستجو کنید"
                    
                  />
                </FormGroup>
              </div>
            </div>


            <div className="form-group row">
              <label className="col-md-2 col-form-label">واحد ارسال کننده</label>
              <div className="col-md-10">
                <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                  <AsyncSelect
                    defaultOptions={true}
                    isClearable={true}
                    // isDisabled={checkAllUsers  ||  (!!users.user && disabledUsersField) }
                    name="subject-list"
                    id="subject-list"
                    loadOptions={loadUnitSender}
                    className="basic-multi-select"
                    onChange={(e) => {
                      setFormData({...formData ,  sender_unit_id : e?.value });
                      // setDisabledUsersField(true);
                      
                    }}
                    placeholder="جستجو کنید"
                    
                  />
                </FormGroup>
              </div>
            </div>

            <FormGroup className="mb-4" row>
              <Label
                htmlFor="projectname"
                className="col-form-label col-md-2"> عنوان پیام </Label>
              <Col md="10">
                <Input
                  type="text"
                  className="form-control"
                  placeholder="عنوان پیام ..."
                  onChange={addData}
                  name="title"
                  // defaultValue={formData?.[item?.value] ? formData?.[item?.value] : ""}
                  // invalid={click && (!formData?.[item?.value] || !(item?.min < (formData?.[item?.value]?.length)) || !((formData?.[item?.value]?.length) < item?.max))}
                  // valid={formData?.credentials?.[item?.value] && (item?.min < (formData?.[item?.value]?.length) < item?.max)}
                />
              </Col>
            </FormGroup>
            <FormGroup className="mb-4" row>
              <Label
                htmlFor="projectname"
                className="col-form-label col-md-2"> متن پیام </Label>
              <Col md="10">
                <textarea className="form-control" id="" cols="30" rows="10"
                          name="body"
                          onChange={ addData}
                          placeholder="متن پیام را بنویسید ... " />
              </Col>
            </FormGroup>
            <div className="text-right">
              <Button type="submit" color="primary" onClick={ sendData} className="px-5">
                ارسال پیام
              </Button>
            </div>
          </Form>
        </Container>
      </div>
    </React.Fragment>
  )
}


export default Send