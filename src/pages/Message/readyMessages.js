import React, { useEffect, useState } from "react"
import {
  Button,
  CardBody,
  CardTitle,
  Col,
  Container, Dropdown, DropdownItem, DropdownMenu, DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  Row,
  Table
} from "reactstrap"
import Breadcrumbs from "components/Common/Breadcrumb"
import Select from "react-select"
import { Link, NavLink } from "react-router-dom"
import numberWithCommas from "../../components/threeNumber"
import { fetcher } from "store/common/action"
import { GET_NEW_MESSAGE } from "../../helpers/url_helper"
import { MessageToast } from "../../utils/message"
import PaginationCustom from "components/paginationZero"
import { BiDotsVertical } from "react-icons/all"
import moment from "moment-jalaali"
import { convertTypeMessage, convertTypeMessageStatus } from "utils/convertToPersion"


const Action = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false)
  const [modal, setModal] = useState(false)

  return <div dir="ltr" className="text-right ">
    <Button color="primary" onClick={() => setModal(true)}>
      مشاهده
    </Button>
    <Modal centered={true} isOpen={modal} toggle={() => setModal(false)}>
      <div className="p-4 text-center">

        <CardTitle className="mb-3 text-left"> مشاهده پیام </CardTitle>
        <div className="p-3 border">

          <div className="row text-left">
            <div className="col-12">
              {props?.item?.body}
            </div>

            <div className="col-12">

            </div>
          </div>
        </div>
        <Button className="d-inline-block mt-3" color="primary" onClick={() => setModal(false)}>
          بستن
        </Button>
      </div>


    </Modal>
  </div>
}
const Send = () => {
  const [formData, setFormData] = useState([])
  const [modal, setModal] = useState(false)
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [chatsCount, setChatsCount] = useState(0)
  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    getData(p,s)
  }
  useEffect(() => {
    getData()
  }, [])

  const getData = (p=0,s=50) => {
    fetcher(GET_NEW_MESSAGE, { method: "GET" },{ query: { page: p, size: s , type : "automatic" } })
      .then(r => {
        if (r?.result) {
          setFormData(r?.result?.items)
          setChatsCount(r?.result?.total)
        }
      }).catch(e => {
      MessageToast("error", "با خطا مواجه شدید")
    })
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="پیام ها" breadcrumbItem="پیام‌های آماده" />

          <Row>
            <Col lg="12">


              <CardTitle className="mb-4 my-3"> تاریخچه پیام ها </CardTitle>
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                    <tr>
                      <th scope="col">شماره</th>
                      <th scope="col"> عنوان پیام</th>
                      <th scope="col"> دریافت کننده</th>
                      <th scope="col"> ارسال کننده</th>
                      <th scope="col"> تاریخ</th>
                      <th scope="col"> نوع</th>
                      <th scope="col">وضعیت</th>
                      <th scope="col">عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    {formData?.length &&
                      formData.map((item, i) =>
                        <tr>
                          <td> {page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                          <td>{item?.title}</td>
                          <td>{item?.additional_data?.receiver === "all" ? 'همه کاربران' : item?.additional_data?.receiver}</td>
                          <td>{item?.additional_data?.sender}</td>
                          <td>{
                            item?.additional_data?.time ?
                            moment(item?.additional_data?.time, "YYYY-MM-DDThh:mm:ss").format("jYYYY/jMM/jDD hh:mm") : ""}</td>
                          <td>{convertTypeMessage(item?.type)}</td>
                          <td>{convertTypeMessageStatus(item?.pro_type)}</td>
                          <td><Action item={item} /></td>


                        </tr>)
                    }
                    </tbody>
                  </Table>
                </div>
              </div>

            </Col>
          </Row>
          <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor(chatsCount / pageSize ) + 1}
                            addPageSize={setPageSize}
                            pageSize={pageSize}
                            chatsCount={chatsCount} />
        </Container>

      </div>
    </React.Fragment>
  )
}

export default Send