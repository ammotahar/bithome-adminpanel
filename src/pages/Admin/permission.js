import React, { useEffect, useState } from "react"
import { GET_PERMISSION, ADD_PERMISSION } from "helpers/url_helper"
import { fetcher } from "../../store/common/action"
import { Button, CardTitle, Form, FormGroup, Modal } from "reactstrap"
import { MessageToast } from "../../utils/message"

const Permission = ({ FormListCheckbox, id }) => {
  const [ListCheckbox, setListCheckbox] = useState([])
  const [modal, setModal] = useState(false)

  const getPermissions = () => {
    fetcher(ADD_PERMISSION(id), {
      method: "GET"
    }).then(r => {
      if (r?.result) {
        let list = []
        list = r?.result.map(t => t?.permission?.id)
        setListCheckbox(list)
      }
    }).catch(e => {
      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }
  const sendPermissions = () => {
    fetcher(ADD_PERMISSION(id), {
      method: "POST",
      body: JSON.stringify({
        "perm_ids": ListCheckbox
      })
    }).then(r => {
      if (r?.result) {
        setModal(false)
        // MessageToast("success", "با موفقیت انجام شد")
          window.location.reload();
      }
    }).catch(e => {
      MessageToast("error", " با خطا مواجه شد ")
    })
  }
  return (
    <div>

      <Button color="warning" onClick={() => {
        getPermissions()
        setModal(true)
      }}>دسترسی</Button>


      <Modal centered={true} isOpen={modal} toggle={() => setModal(false)} size="lg">
        <div className="p-3">
          <CardTitle className="mb-4 my-3">تعیین دسترسی ها </CardTitle>
          <div className="form-group row">

            <div className="col-md-12">
              <div className="form-group row">
                {
                  FormListCheckbox.map((item, i) =>
                    <div className="col-md-6">
                      <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                        <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                          <input
                            name={item.id}
                            type="checkbox"
                            className="custom-control-input"
                            id={"customCheckAdmin2" + (i + 2)}
                            checked={ListCheckbox.some(t => t === item.id)}
                            onChange={(e) => {
                              if (e.target.checked) {
                                setListCheckbox([...ListCheckbox, item.id])
                              } else {
                                let list = ListCheckbox.filter(i => item.id !== i)
                                setListCheckbox(list)
                              }
                            }}
                          />
                          <label
                            className="custom-control-label"
                            htmlFor={"customCheckAdmin2" + (i + 2)}>
                            {item.title_fa}
                          </label>
                        </div>
                      </FormGroup>
                    </div>
                  )
                }
              </div>
              <div className="text-right">
                <Button type="button" color="primary" className="px-5 mr-2" onClick={sendPermissions}>
                  تایید دسترسی
                </Button>
                <Button type="button" color="warning" className="px-5" onClick={() => setModal(false)}>

                  انصراف </Button>
              </div>

            </div>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default Permission