import React, { useEffect, useState } from "react"
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Table
} from "reactstrap"
import { FaTimesCircle } from "react-icons/all"
import { Link, withRouter } from "react-router-dom"
import { fetcher } from "store/common/action"
import { GET_PERMISSION, LIST_ADMIN } from "helpers/url_helper"
import numberWithCommas from "components/threeNumber"
import moment from "moment-jalaali"
import { MessageToast } from "utils/message"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { getMoney } from "store/auth/profile/actions"
import Permission from "./permission"
import PaginationCustom from "components/paginationZero"
// import PaginationCustom from "../../components/pagination"

const FormListInput = [
  { name: "مبلغ(تومان) ", value: "amount", type: "number" },
  { name: " شماره حساب", value: "user_bank_account", type: "number" },
  { name: "کدپیگیری", value: "tracking_code", type: "number" },
  { name: "توضیحات", value: "description" }

]
const List = (props) => {
  const [formData, setFormData] = useState({ money_wallet_id: 1 })
  const [error, setError] = useState([])
  const [payment, setPayment] = useState([])
  const {setNewAdmin ,refresh,FormListCheckbox} = props

  const [params, setParams] = useState({ is_superuser: true })
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [counts, setCounts] = useState(0)
  useEffect(() => {
    getData()
  }, [refresh])

  const getData = (p = 0, s = pageSize) => {
    fetcher(LIST_ADMIN, {
      method: "GET"
    },{query:{page: p, page_size: s,...params}}).then(r => {
      if (r?.result) {
        setPayment(r?.result?.items)
        setCounts(r?.result?.total)
      }
    }).catch(e => {
      MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }


  const addData = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }
  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getData(p, s)
  }
  return (
    <>
      <div>
        <Row>
          <Col lg="12">


            <CardTitle className="mb-4 my-3"> لیست ادمین ها </CardTitle>
            <div className="">
              <div className="table-responsive">
                <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                  <thead>
                  <tr>
                    <th scope="col">شماره</th>
                    <th scope="col">  نام ادمین </th>

                    <th scope="col">  موبایل</th>
                    <th scope="col"> پلتفرم</th>
                    <th scope="col"> دسترسی</th>
                    <th scope="col"> گزارش فعالیت</th>
                  </tr>
                  </thead>
                  <tbody>
                  {payment.length &&
                    payment.map((item, i) =>
                     <tr key={item?.id}>
                        <td>{page == 0 ?  ++i : (page * pageSize) + ++i }</td>
                        <td>{(item?.credentials?.first_name || "") + " " + (item?.credentials?.last_name || "") }</td>

                        <td>{item?.mobile}</td>
                        <td>{numberWithCommas(numberWithCommas(item?.amount))}</td>
                        {/*<td>{item?.created_at && moment(item?.created_at, "YYYY-MM-DDThh:mm").format("jYYYY/jMM/jDD hh:mm")}</td>*/}
                        {/*<td>{item?.credentials?.email }</td>*/}
                        <td><Permission id={item?.id} FormListCheckbox={FormListCheckbox}/></td>
                        <td><Link to={`/admin/reports/${item?.id}`}><button  className="btn btn-info">مشاهده گزارش</button></Link></td>
                        {/*<td><Button color="primary" onClick={()=>setNewAdmin(item)} >ویرایش</Button></td>*/}
                        {/*<td><Button color="danger">حذف</Button>*/}
                        {/*</td>*/}
                      </tr>)
                  }
                  </tbody>
                </Table>
              </div>
            </div>

          </Col>
        </Row>
        <PaginationCustom handlePageClick={handlePageClick} 
            page={page}
            totalPage={Math.floor((counts) / pageSize ) + 1}
            addPageSize={setPageSize}
            pageSize={pageSize}
            chatsCount={counts} />

      </div>
    </>
  )
}

List.propTypes = {
  money: PropTypes.object,
  getMoney: PropTypes.func
}

const mapStateToProps = ({ Profile }) => ({
  money: Profile.money?.result || {}
})

const mapDispatchToProps = dispatch => ({
  getMoney: (...data) => dispatch(getMoney(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(List))