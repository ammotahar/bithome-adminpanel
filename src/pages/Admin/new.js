import React, { useEffect, useState } from "react"
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Table
} from "reactstrap"
import { FaTimesCircle } from "react-icons/all"
import { Link, withRouter } from "react-router-dom"
import { fetcher } from "store/common/action"
import { NEW_ADMIN, GET_PERMISSION, ADD_PERMISSION, GET_USERS_SEARCH_SEND_MESSAGE } from "helpers/url_helper"
import numberWithCommas from "components/threeNumber"
import moment from "moment-jalaali"
import { MessageToast } from "utils/message"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { getMoney } from "store/auth/profile/actions"
import AsyncSelect from "react-select/async/dist/react-select.esm"

const FormListInput = [
  // { name: "نام   ", value: "first_name" },
  // { name: "نام خانوادگی   ", value: "last_name" },
  // { name: "شماره تلفن همراه ", value: "phone", type: "number" },
  // { name: "   آدرس", value: "address" }
]
const FormListSelect = [
  {
    name: " پلتفرم", value: "bathroom", children: [
      { name: "بیت هوم", value: "bithome" },
      { name: "  متریوم", value: " meterium " }
    ]
  }

]

const New = (props) => {
  const [formData, setFormData] = useState({})
  const [error, setError] = useState([])
  const [payment, setPayment] = useState([])
  const [ListCheckbox, setListCheckbox] = useState([])
  const [idUser, setIdUser] = useState(0)
  const [is_active, setIs_active] = useState(true)
const {newAdmin,setRefresh,FormListCheckbox}=props


  useEffect(() => {
      setFormData(newAdmin)
  }, [newAdmin])

  const postData = (e) => {
    e.preventDefault()
    fetcher(NEW_ADMIN(formData?.id), {
      method: "PUT",
      body: JSON.stringify(formData)
    }).then(r => {
      // getData()
      FormListInput.map(t => {
        document.getElementById(t.value).value = ""
      })
      setRefresh(true)
      setFormData({})

      MessageToast("success", " با موفقیت انجام شد")
    }).catch(e => {
      MessageToast("error", "با خطا مواجه شدید")
    })
  }
  const addData = e => {
    setFormData({ ...formData,credentials: { ...formData?.credentials,[e.target.name]: e.target.value } })

  }
  const loadOptions = (name) => {
    return fetcher(GET_USERS_SEARCH_SEND_MESSAGE, {
      method: "GET"
    }, { query: { name: name, limit: 20 } })
      .then(r => {
        if (r?.result) {
          console.log(r?.result);
          let subject_list = []
          // r?.result.map(i => subject_list.push({ label: i.full_name, value: i.user_id, ...i }))
          r?.result.map(i => { setIdUser(i.id) ; subject_list.push({ label : i.credentials?.full_name, value : i.id}) })
          console.log(subject_list);
          return subject_list
        }
      })
      .catch(e => {
        MessageToast("error", "با خطا مواجه شدید")
      })

  }
  const sendPermissions = () => {
    

    if(idUser)
    fetcher(ADD_PERMISSION(idUser), {
      method: "POST",
      body: JSON.stringify({
        "perm_ids": ListCheckbox
      })
    }).then(r => {
      if (r?.result) {
        MessageToast("success", "با موفقیت انجام شد")
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      }
    }).catch(e => {
      MessageToast("error", " با خطا مواجه شد ")
    })
  }

  // const handlePageClick = (p = 1, s = pageSize) => {
  //   setPage(p)
  //   getUsers(p, s)
  // }

  return (
    <>
      <div>
        <Row>
          <Col lg="12">


            <CardTitle className="mb-4"> اضافه کردن ادمین</CardTitle>
            <Form>
              <div className="form-group row">
                <label className="col-md-2 col-form-label">  کاربر</label>
                <div className="col-md-10">
                  <AsyncSelect
                    defaultOptions={true}
                    isClearable={false}
                    name="subject-list"
                    id="subject-list"
                    loadOptions={loadOptions}
                    className="basic-multi-select"
                    onChange={(e) => {
                      console.log(e);
                      setIdUser(e?.value)
                      setIs_active(false)
                    }}
                    placeholder="جستجو کنید"
                  />
                </div>
              </div>
              {/*--------------------------------------------------input-------------------------------------------------*/}
              {
                FormListInput.map((item, i) => <div key={i}><FormGroup className="mb-4" row>
                    <Label
                      htmlFor="projectname"
                      className="col-form-label col-lg-2"
                    >
                      {item?.name}
                    </Label>
                    <Col lg="10">
                      <Input
                        id={item?.value}
                        name={item?.value}
                        type={item?.type || "text"}
                        className="form-control"
                        placeholder={item?.name + " ..."}
                        onChange={addData}
                        value={formData?.credentials?.[item?.value]}
                        defaultValue={formData?.[item?.credentials?.value] ? formData?.[item?.credentials?.value] : ""}
                      />
                      {error.some(t => t === item?.value) && <small className="text-danger">
                        <span className="pr-1">  <FaTimesCircle /></span>

                        {item?.name + " را بصورت صحیح وارد کنید "}
                      </small>}
                    </Col>
                  </FormGroup>

                  </div>
                )
              }
              {/*--------------------------------------------------------------------------------------------select*/}

              {FormListSelect.map((item, i) => <div className="form-group row">
                <label className="col-md-2 col-form-label">  {item?.name}</label>
                <div className="col-md-10">
                  <select className="form-control"
                          value={formData?.[item?.value]}
                          onChange={(e) => setFormData({ ...formData, [item?.value]: e.target.value })}
                          id={item?.value}
                          name={item?.value}>
                    <option className="text-secondary"> انتخاب {item?.name} ...</option>
                    {
                      item?.children.map((option, i) => <option value={option?.value}>{option?.name}</option>)
                    }
                  </select>
                  {/*{error.some(t => t === item?.value) && <small className="text-danger">*/}
                  {/*  <span className="pr-1">  <FaTimesCircle /></span>*/}

                  {/*  {"نوع  سرویس بهداشتی" + " را بصورت صحیح وارد کنید "}*/}
                  {/*</small>}*/}
                </div>
              </div>)}
              <div className="form-group row">
                <label className="col-md-2 col-form-label"> تعیین دسترسی</label>
              <div className="col-md-10">
              <div className="form-group row">

                <div className="col-md-12">
                  <div className="form-group row">
                    {
                      FormListCheckbox.map((item, i) =>
                        <div className="col-md-6">
                          <FormGroup className="ajax-select mt-3 mt-lg-0 select2-container">
                            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
                              <input
                                name={item.id}
                                type="checkbox"
                                className="custom-control-input"
                                id={"customCheckcolorAdmin" + (i + 2)}
                                checked={ListCheckbox.some(t => t === item.id)}
                                onChange={(e) => {
                                  if (e.target.checked) {
                                    setListCheckbox([...ListCheckbox, item.id])
                                  } else {
                                    let list = ListCheckbox.filter(i => item.id !== i)
                                    setListCheckbox(list)
                                  }
                                }}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor={"customCheckcolorAdmin" + (i + 2)}>
                                {item.title_fa}
                              </label>
                            </div>
                          </FormGroup>
                        </div>
                      )
                    }
                  </div>


                </div>
              </div>
              </div>
              </div>
              <div className="text-right">
                  <Button disabled={is_active} type="submit" color="primary" className="px-5" onClick={(e)=>{
                    e.preventDefault();
                    sendPermissions()
                  }}>
                     اضافه کردن ادمین
                  </Button>
              </div>
            </Form>
          </Col>
        </Row>


      </div>
    </>
  )
}

New.propTypes = {
  money: PropTypes.object,
  getMoney: PropTypes.func
}

const mapStateToProps = ({ Profile }) => ({
  money: Profile.money?.result || {}
})

const mapDispatchToProps = dispatch => ({
  getMoney: (...data) => dispatch(getMoney(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(New))