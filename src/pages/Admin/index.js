import React, { useEffect, useState } from "react"
import { Button, Col, Container, Row, Spinner, Table } from "reactstrap"
import Breadcrumbs from "../../components/Common/Breadcrumb"
import { fetcher } from "../../store/common/action"
import { GET_PERMISSION, GET_USER_ChECK } from "../../helpers/url_helper"
import { MessageToast } from "../../utils/message"
import { Link } from "react-router-dom"
import New from "./new"
import List from "./list"


const Index = () => {
const [newAdmin,setNewAdmin]=useState({})
const [refresh,setRefresh]=useState(false)

const [page, setPage] = useState(0)
const [counts, setCounts] = useState(0)
const [pageSize, setPageSize] = useState(50)

  const [FormListCheckbox, setFormListCheckbox] = useState([])
  useEffect(() => {
    getPermissions()
  }, [])
  const getPermissions = () => {
    fetcher(GET_PERMISSION, {
      method: "GET"
    }).then(r => {
      if (r?.result) {
        setFormListCheckbox(r?.result)
      }
    }).catch(e => {
      // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }

  const handlePageClick = (p = 0, s = pageSize) => {
    setPage(p)
    getUsers(p, s)
  }

  
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="ادمین" breadcrumbItem="سطوح دسترسی " />

          <New FormListCheckbox={FormListCheckbox} newAdmin={newAdmin} setRefresh={setRefresh}/>
          <List FormListCheckbox={FormListCheckbox} setNewAdmin={setNewAdmin} refresh={refresh}/>
         
        </Container>
      </div>
    </React.Fragment>
  )
}

export default Index