import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import { isEmpty, map } from "lodash"
import {
  Col,
  Container,
  Row,
  Table
} from "reactstrap"
import classnames from "classnames"
import PaginationCustom from "components/paginationZero"

//Import Scrollbar
import PerfectScrollbar from "react-perfect-scrollbar"
import "react-perfect-scrollbar/dist/css/styles.css"

//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"
// import images from "assets/images"
import {
  addMessage,
  getChats,
  getContacts,
  getGroups,
  getMessages
} from "store/actions"
import moment from "moment-jalaali"

const Chat = props => {
  const { chats, groups, contacts, messages } = props
  const [messageBox, setMessageBox] = useState(null)
  // const Chat_Box_Username2 = "Henry Wells"
  const [currentRoomId, setCurrentRoomId] = useState(1)
  // eslint-disable-next-line no-unused-vars
  const [currentUser, setCurrentUser] = useState({
    name: "Henry Wells",
    isActive: true
  })
  const [notification_Menu, setnotification_Menu] = useState(false)
  const [search_Menu, setsearch_Menu] = useState(false)
  const [settings_Menu, setsettings_Menu] = useState(false)
  const [other_Menu, setother_Menu] = useState(false)
  const [activeTab, setactiveTab] = useState("1")
  const [Chat_Box_Username, setChat_Box_Username] = useState("Steven Franklin")
  // eslint-disable-next-line no-unused-vars
  const [Chat_Box_User_Status, setChat_Box_User_Status] = useState("online")
  const [curMessage, setcurMessage] = useState("")
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const { onGetChats, onGetGroups, onGetContacts, onGetMessages, chatsCount } = props

  useEffect(() => {
    handlePageClick()
  }, [])
  useEffect(() => {
    setTotalPage(Math.floor(chatsCount / pageSize ) + 1)
  }, [chatsCount])

  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    onGetChats({ query: { page: p, size: s } })
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumbs title="پشتیبانی" breadcrumbItem="تیکت" />

          <Row>
            <Col lg="12">

              <div className="">
                <div className="table-responsive ">

                  <Table
                    className="project-list-table table-nowrap table-centered table-borderless table-striped table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        شماره
                      </th>
                      <th scope="col"> موضوع</th>
                      <th scope="col"> فرستنده</th>
                      <th scope="col"> موبایل</th>
                      <th scope="col" className="text-center"> اولویت</th>
                      <th scope="col">وضعیت</th>
                      <th scope="col" className="text-center">تاریخ</th>

                      <th scope="col">عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    {map(chats, (item, index) => (
                      <tr key={index}>
                        <td className="text-center">
                          {/* {((page) * pageSize)+(index + 1)} */}
                          {page == 0 ?  ++index : (page * pageSize) + ++index }

                        </td>

                        <td style={{ minWidth: "200px" }}>
                          <h5 className="text-truncate font-size-14">

                            {item?.category?.title}

                          </h5>
                          {/*<p className="text-muted mb-0 text-wrap">*/}
                          {/*  {item.body}*/}
                          {/*</p>*/}
                        </td>
                        <td>  {(item.user?.credentials?.first_name || "") + " " + (item.user?.credentials?.last_name || "")}</td>
                        <td>  {item?.user?.mobile}</td>
                        <td className="text-center">
                          {item?.priority === "high" && "فوری"}
                          {item?.priority === "medium" && "متوسط"}
                          {item?.priority === "low" && "پایین"}
                        </td>

                        <td className="text-center">
                          {item?.state === "unread" && <span className="text-danger">خوانده نشده</span>}
                          {item?.state === "pending" && <span className="text-warning">در انتظار</span>}
                          {item?.state === "read" && <span className="text-success">پاسخ داده شده</span>}
                          {item?.state === "close" && <span className="text-muted">بسته شده</span>}
                        </td>
                        <td>{moment(item?.created_at, "YYYY-MM-DDThh:mm:ss").format("jYYYY/jMM/jDD hh:mm")}</td>

                        <td>
                          <Link to={"/chat/" + item?.id} className="btn btn-primary text-white">مشاهده</Link>
                        </td>
                      </tr>
                    ))}
                    </tbody>
                  </Table>
                </div>


                <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor(chatsCount / pageSize ) + 1}
                                  addPageSize={setPageSize}
                                  pageSize={pageSize}
                                  chatsCount={chatsCount} />


              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

Chat.propTypes = {
  chats: PropTypes.array,
  groups: PropTypes.array,
  contacts: PropTypes.array,
  messages: PropTypes.array,
  onGetChats: PropTypes.func,
  onGetGroups: PropTypes.func,
  onGetContacts: PropTypes.func,
  onGetMessages: PropTypes.func,
  onAddMessage: PropTypes.func
}

const mapStateToProps = ({ chat }) => ({
  chats: chat.chats?.result?.items || [],
  chatsCount: chat.chats?.result?.total || 0,
  groups: chat.groups,
  contacts: chat.contacts,
  messages: chat.messages
})

const mapDispatchToProps = dispatch => ({
  onGetChats: (...data) => dispatch(getChats(...data)),
  onGetGroups: () => dispatch(getGroups()),
  onGetContacts: () => dispatch(getContacts()),
  onGetMessages: roomId => dispatch(getMessages(roomId)),
  onAddMessage: roomId => dispatch(addMessage(roomId))
})

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
