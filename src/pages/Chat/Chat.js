import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import { isEmpty, map, set } from "lodash"
import moment from "moment-jalaali"
import {
  Button,
  Card,
  Col,
  Container,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Media,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  UncontrolledDropdown,
  UncontrolledTooltip,
} from "reactstrap"
import classnames from "classnames"

//Import Scrollbar
import PerfectScrollbar from "react-perfect-scrollbar"
import "react-perfect-scrollbar/dist/css/styles.css"

//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"
// import images from "assets/images"
import {
  addMessage,
  closeTicket,
  getChats,
  getContacts,
  getGroups,
  getMessages,
} from "store/actions"
import { MessageToast } from "utils/message"


const Chat = props => {
  const { chats, groups, contacts, messages } = props
  const [messageBox, setMessageBox] = useState(null)
  // const Chat_Box_Username2 = "Henry Wells"
  const [currentRoomId, setCurrentRoomId] = useState(1)
  // eslint-disable-next-line no-unused-vars
  const [currentUser, setCurrentUser] = useState({
    name: "Henry Wells",
    isActive: true,
  })
  const [notification_Menu, setnotification_Menu] = useState(false)
  const [search_Menu, setsearch_Menu] = useState(false)
  const [settings_Menu, setsettings_Menu] = useState(false)
  const [other_Menu, setother_Menu] = useState(false)
  const [activeTab, setactiveTab] = useState("1")
  // const [Chat_Box_Username, setChat_Box_Username] = useState("Steven Franklin")
  // eslint-disable-next-line no-unused-vars
  const [Chat_Box_User_Status, setChat_Box_User_Status] = useState("online")
  const [curMessage, setcurMessage] = useState("")

  useEffect(() => {
    const { onGetChats, onGetGroups, onGetContacts, onGetMessages,match } = props
    // onGetChats()
    // onGetGroups()
    // onGetContacts()
    onGetMessages(match?.params?.id)
  }, [])

  useEffect(() => {
    if (!isEmpty(messages)) scrollToBottom()
  }, [props, messages])

  // const toggleNotification = () => {
  //   setnotification_Menu(!notification_Menu)
  // }

  //Toggle Chat Box Menus
  // const toggleSearch = () => {
  //   setsearch_Menu(!search_Menu)
  // }

  // const toggleSettings = () => {
  //   setsettings_Menu(!settings_Menu)
  // }

  // const toggleOther = () => {
  //   setother_Menu(!other_Menu)
  // }

  // const toggleTab = tab => {
  //   if (activeTab !== tab) {
  //     setactiveTab(tab)
  //   }
  // }

  //Use For Chat Box
  // const userChatOpen = (id, name, status, roomId) => {
  //   const { onGetMessages } = props
  //   // setChat_Box_Username(name)
  //   // setCurrentRoomId(roomId)
  //   // onGetMessages(roomId)
  // }
const { onCloseTicket } = props
  const closeTicket = (id) => {
    console.log(id)
    
    onCloseTicket(id)

    setTimeout(() => {
      MessageToast("success", " تیکت با موفقیت بسته شد")

    }, 300);
  }

  const addMessage = (roomId, sender) => {
    const { onAddMessage } = props
    const message =
      {
      id:messages?.id,
      message: {
      "title": "پاسخ",
        "body":  curMessage,
        "category_id": 1,
        "state": "unread",
        "priority": "high",
        "media_id": 1
    }
    }
    setcurMessage("")
    // console.log(messages)
    onAddMessage(message)
  }

  const scrollToBottom = () => {
    if (messageBox) {
      messageBox.scrollTop = messageBox.scrollHeight + 1000
    }
  }

  const onKeyPress = e => {
    const { key, value } = e
    if (key === "Enter") {
      setcurMessage(value)
      addMessage(currentRoomId, currentUser.name)
    }
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumbs title="گفتگو" breadcrumbItem="مشاهده " />

          <Row>
            <Col lg="12">
              <div className="d-lg-flex">

                <div className="w-100 user-chat">
                  <Card>
                    <div className="p-4 border-bottom ">
                      <Row>
                        <Col md="12" xs="12">
                          <h5 className="font-size-15 mb-1">
                            {(messages?.user?.credentials?.first_name || "") + " " + (messages?.user?.credentials?.last_name || "")}
                          </h5>

                          <p className="text-muted mb-0">
                            {/*<i*/}
                            {/*  className={*/}
                            {/*    Chat_Box_User_Status === "online"*/}
                            {/*      ? "mdi mdi-circle text-success align-middle mr-1"*/}
                            {/*      : Chat_Box_User_Status === "intermediate"*/}
                            {/*      ? "mdi mdi-circle text-warning align-middle mr-1"*/}
                            {/*      : "mdi mdi-circle align-middle mr-1"*/}
                            {/*  }*/}
                            {/*/>*/}
                            {/*{Chat_Box_User_Status}*/}
                          </p>
                        </Col>
                      </Row>
                    </div>

                    <div>
                      <div className="chat-conversation p-3">
                        <ul className="list-unstyled">
                          <PerfectScrollbar
                            style={{ height: "470px" }}
                            containerRef={ref => setMessageBox(ref)}
                          >

                            <li
                              key={"test_k" + messages?.id}
                              className="right">
                              <div className="conversation-list">
                                <div className="ctext-wrap">
                                  <div className="conversation-name">
                                    <p>
                                      <i className="bx bx-user-circle align-middle mr-1" />
                                      {(messages?.user?.credentials?.first_name || "") + " " + (messages?.user?.credentials?.last_name || "")}
                                    </p>
                                  </div>
                                  <p>{messages?.body}</p>
                                  <p className="chat-time mb-0">
                                    <i className="bx bx-time-five align-middle mr-1" />
                                    {moment(messages?.created_at, "YYYY-MM-DDThh:mm").format("jYYYY/jMM/jDD hh:mm")}
                                  </p>
                                </div>
                              </div>
                            </li>
                            {messages?.reply?.length ?
                              map(messages?.reply, message => (
                                <li
                                  key={"test_k" + message.id}
                                  className={
                                    message.user?.id === messages?.user?.id
                                      ? "right"
                                      : ""
                                  }
                                >
                                  <div className="conversation-list">

                                    <div className="ctext-wrap">
                                      <div className="conversation-name">
                                        <p>
                                          <i className="bx bx-user-circle align-middle mr-1" />
                                          {(message.user?.credentials?.first_name || "") + " " + (message.user?.credentials?.last_name || "")}
                                        </p>
                                      </div>

                                      <p>{message.body}</p>
                                      <p className="chat-time mb-0">
                                        <i className="bx bx-time-five align-middle mr-1" />
                                        {moment(message?.created_at, "YYYY-MM-DDThh:mm").format("jYYYY/jMM/jDD hh:mm")}
                                      </p>
                                    </div>
                                  </div>
                                </li>
                              )):''}
                          </PerfectScrollbar>
                        </ul>
                      </div>
                      <div className="p-3 chat-input-section">
                        <Row className="align-items-center">
                          <Col className="col-auto">
                            <Button
                              type="button"
                              color="primary"
                              onClick={() =>
                                addMessage(currentRoomId, currentUser.name)
                              }
                              className="btn-rounded chat-send w-md waves-effect waves-light"
                            >
                              <span className="d-none d-sm-inline-block mr-2">
                                ارسال
                              </span>
                              <i className="mdi mdi-send" />
                            </Button>
                          </Col>
                          <Col>
                            <div className="position-relative">
                              <textarea
                                rows={2}
                                value={curMessage}
                                onKeyPress={onKeyPress}
                                onChange={e => setcurMessage(e.target.value)}
                                className="form-control chat-input"
                                placeholder="وارد کردن پیام ..."
                              />

                            </div>
                          </Col>

                        </Row>


                        <Row className="align-items-center">
                          <Col className="col-auto">
                            <Button
                              type="button"
                              color="danger"
                              onClick={() =>
                                closeTicket(props.match.params.id)
                              }
                              className="btn-rounded chat-send w-md waves-effect waves-light"
                            >
                              <span className="d-none d-sm-inline-block mr-2">
                                بستن تیکت
                              </span>
                              <i className="mdi mdi-send" />
                            </Button>
                          </Col>
                          
                        </Row>
                      </div>
                    </div>
                  </Card>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

Chat.propTypes = {
  chats: PropTypes.array,
  groups: PropTypes.array,
  contacts: PropTypes.array,
  messages: PropTypes.array,
  onGetChats: PropTypes.func,
  onGetGroups: PropTypes.func,
  onGetContacts: PropTypes.func,
  onGetMessages: PropTypes.func,
  onAddMessage: PropTypes.func,
  onCloseTicket: PropTypes.func,
}

const mapStateToProps = ({ chat }) => ({
  chats: chat.chats,
  groups: chat.groups,
  contacts: chat.contacts,
  messages: chat.messages?.result,
})

const mapDispatchToProps = dispatch => ({
  onGetChats: () => dispatch(getChats()),
  onGetGroups: () => dispatch(getGroups()),
  onGetContacts: () => dispatch(getContacts()),
  onGetMessages: roomId => dispatch(getMessages(roomId)),
  onAddMessage: roomId => dispatch(addMessage(roomId)),
  onCloseTicket: roomId => dispatch(closeTicket(roomId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
