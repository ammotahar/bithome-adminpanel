import React, { Component } from "react"
import Leaflet from "leaflet"
import { Map, TileLayer, Marker, Popup } from "react-leaflet"
import "leaflet/dist/leaflet.css"
import L from "leaflet"
// Leaflet.Icon.Default.imagePath = "../node_modules/leaflet"
//
// delete Leaflet.Icon.Default.prototype._getIconUrl
//
// Leaflet.Icon.Default.mergeOptions({
//   iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
//   iconUrl: require("leaflet/dist/images/marker-icon.png"),
//   shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
// })

export default class SimpleMap extends Component {


  state = {
    lat: this.props?.position?.length ? this.props?.position[0] : 35.711395371053676,
    lng: this.props?.position?.length ? this.props?.position[1] : 51.39279842376709,
    zoom: 13
  }

  componentDidMount() {

    // delete L.Icon.Default.prototype._getIconUrl;
    //
    // L.Icon.Default.mergeOptions({
    //   iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
    //   iconUrl: require("leaflet/dist/images/marker-icon.png"),
    //   shadowUrl: require("leaflet/dist/images/marker-shadow.png")
    // });
    //
    // navigator.geolocation.getCurrentPosition(function (location) {
    //
    // });
  }

  render() {
    const position = [this.state.lat, this.state.lng]
    return (
      <Map
        ref={r => this.map = r}
        center={this.props?.position?.length ? this.props?.position : [35.711395371053676,
          51.39279842376709]}
        zoom={this.state.zoom}
        onzoomend={e => this.setState({ zoom: e.target._zoom })}
        style={{ height: "70vh" }}
        onclick={e => {
          // this.props.getPoint({latitude: e.latlng.lat, longitude: e.latlng.lng})
          this.setState({ lat: e.latlng.lat, lng: e.latlng.lng })
          this.props.setPosition([e.latlng.lat, e.latlng.lng])
        }}
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
        />
        <Marker position={position}>
          <Popup>مکان شما </Popup>
        </Marker>
      </Map>
    )
  }
}
