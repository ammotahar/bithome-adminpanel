import React, { useEffect, useState } from "react"

import { Container, Row, Col, Card, CardBody, Modal, Button } from "reactstrap"

//Import maps
import SimpleMap from "./LeafletMap/SimpleMap"


//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"
import { FaTimes } from "react-icons/all"

const MapsLeaflet = (props = { position: [] }) => {
  const [modal, setModal] = useState(false)
  const [position, setPosition] = useState(props?.position)

  useEffect(() => {
    // console.log(props?.position)
    setPosition(props?.position)
  }, [props.position])

  return (
    <React.Fragment>
      <button type="button" className="form-control text-left" onClick={() => setModal(true)}>

        {
          position?.length ?
            <span>
              <span>lat:</span>
              <span> {position[0]}</span>
              <span className="px-2"></span>
              <span>long:</span>
            <span> {position[1]}</span>
            </span>

            :
            " انتخاب موقعیت بر روی نقشه ..."
        }


      </button>
      <Modal centered={true} size="xl" isOpen={modal} toggle={() => setModal(false)}>

        {/*<Breadcrumbs title="Maps" breadcrumbItem="Leaflet" />*/}
        <Card>
          <CardBody>
            <div className="d-flex justify-content-between align-items-center mb-3">
              <h4 className="card-title">انتخاب موقعیت</h4>
              <Button onClick={() => setModal(false)} className=" btn btn-danger"><FaTimes /></Button>
            </div>

            <div id="leaflet-map" className="leaflet-map">
              <SimpleMap position={props?.position} setPosition={(t) => {
                setPosition(t)
                props.setPosition(t)
              }} />
            </div>
            <div className="text-center mt-3">
              <Button onClick={() => setModal(false)} className="btn btn-info px-5">تایید</Button>
            </div>
          </CardBody>
        </Card>

      </Modal>
    </React.Fragment>
  )
}

export default MapsLeaflet
