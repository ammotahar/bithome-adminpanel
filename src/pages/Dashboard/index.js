import PropTypes from "prop-types"
import React, { useEffect, useState } from "react"
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardBody,
  CardTitle,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Media,
  Table
} from "reactstrap"
import { Link } from "react-router-dom"

//import Charts
import StackedColumnChart from "./StackedColumnChart"

import modalimage1 from "../../assets/images/profile-img.png"
import modalimage2 from "../../assets/images/profile-img.png"

// Pages Components
import WelcomeComp from "./WelcomeComp"
import MonthlyEarning from "./MonthlyEarning"
import SocialSource from "./SocialSource"
import ActivityComp from "./ActivityComp"
import TopCities from "./TopCities"
import LatestTranaction from "./LatestTranaction"

//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"

//i18n
import { withTranslation } from "react-i18next"
import { FaFolder, FaHome, FaHorse, FaUser, FaUsers , FaUserCheck , FaUserTie , FaChartArea ,TiChartArea,AiOutlineAreaChart, GiTakeMyMoney , BiPurchaseTagAlt , GiMoneyStack , IoWalletSharp} from "react-icons/all"
import { fetcher } from "store/common/action"
import { DASHBOARD_REPORTS } from "helpers/url_helper"
import { MessageToast } from "../../utils/message"
import numberWithCommas from "components/threeNumber"

const icons = {
  FaUser: <FaUser/>,
  FaFolder: <FaFolder/>,
  FaHome: <FaHome/>,
  FaUsers: <FaUsers/>,
  FaUserCheck : <FaUserCheck/>,
  FaChartArea : <FaChartArea/>,
  TiChartArea : <TiChartArea/>,
  AiOutlineAreaChart : <AiOutlineAreaChart/>,
  GiTakeMyMoney : <GiTakeMyMoney/>,
  BiPurchaseTagAlt : <BiPurchaseTagAlt/>,
  GiMoneyStack : <GiMoneyStack/>,
  IoWalletSharp : <IoWalletSharp/>,
  FaUserTie : <FaUserTie/>

}
const Dashboard = props => {
  const [modal, setmodal] = useState(false)
  const [reports, setReports] = useState([])

useEffect(()=>{
  getData()

},[])
  const getData = () => {
    fetcher(DASHBOARD_REPORTS, {
      method: "GET"
    }).then(r => {
      if (r?.result) {

        let list = [
          { title: "تعداد کاربران", iconClass: "FaUser", description: "1,235" },
          { title: "کاربران احراز هویت شده", iconClass: "FaUserCheck", description: "1,235" },
          { title: "کاربران دارای ملک", iconClass: "FaUserTie", description: "1,235" },
          { title: "تعداد پروژه‌ها", iconClass: "FaFolder", description: "1,235" },
          { title: "تعداد املاک", iconClass: "FaHome", description: "16.2" },
          { title: "مجموع متراژ عرضه شده", iconClass: "FaChartArea", description: "1,235" },
          { title: "مجموع متراژ فروخته شده", iconClass: "TiChartArea", description: "1,235" },
          { title: "مجموع متراژ معامله شده", iconClass: "AiOutlineAreaChart", description: "1,235" },
          { title: "مجموعه ورودی مالی (تومان)", iconClass: "GiTakeMyMoney", description: "1,235" },
          { title: "مجموع فروش (تومان)", iconClass: "BiPurchaseTagAlt", description: "1,235" },
          { title: "مجموع معاملات (تومان)", iconClass: "GiMoneyStack", description: "1,235" },
          { title: " موجودی کیف پول‌ها (تومان)", iconClass: "IoWalletSharp", description: "1,235" },

        ]

        list[0]["description"]=r?.result?.total_user                // تعداد کاربران  
        list[1]["description"]=r?.result?.approved_user             // کاربران احراز هویت شده
        list[2]["description"]=r?.result?.total_owners              // کاربران دارای ملک
        list[3]["description"]=r?.result?.projects_count            // تعداد پروژه ها
        list[4]["description"]=r?.result?.estates_count             // تعداد املاک

        list[5]["description"]=r?.result?.offerd_area             // مجموع متراژ عرضه شده

        list[6]["description"]=r?.result?.total_area_buy            // مجموع متراژ فروخته شده
        list[7]["description"]=r?.result?.total_area_trade          // کل متراژ معامله شده

        list[8]["description"]=numberWithCommas(r?.result?.total_deposit)          //مجموع ورودی مالی
        
        list[9]["description"]=numberWithCommas(r?.result?.total_amount_buy)            // مجموع فروش
        list[10]["description"]=numberWithCommas(r?.result?.total_amount_trade)    // مجموع معاملات (تومان)
        list[11]["description"]=numberWithCommas(r?.result?.total_inventory)         //مجموع موجودی کیف پول ها

        setReports(list)
      }
    }).catch(e => {
      MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
    })
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumbs
            title={props.t("Dashboards")}
            breadcrumbItem={props.t("Dashboard")}
          />

          <Row>
            <Col xl="4">
              <WelcomeComp/>
              {/*<MonthlyEarning />*/}
            </Col>
          </Row>
          <Row>
            {/* Reports Render */}
            {reports.map((report, key) => (
              <Col md="6" xl="4" key={"_col_" + key}>
                <Card className="mini-stats-wid">
                  <CardBody>
                    <Media>
                      <Media body>
                        <p className="text-muted font-weight-medium">
                          {report.title}
                        </p>
                        <h4 className="mb-0">{report.description}</h4>
                      </Media>
                      <div className="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                            <span className="avatar-title font-size-24">
                              {
                                icons?.[report.iconClass]
                              }
                      </span>
                      </div>
                    </Media>
                  </CardBody>
                </Card>
              </Col>
            ))}
          </Row>

          {/*<Card>*/}
          {/*  <CardBody>*/}
          {/*    <CardTitle className="mb-4 float-sm-left">*/}
          {/*      Email Sent*/}
          {/*    </CardTitle>*/}
          {/*    <div className="float-sm-right">*/}
          {/*      <ul className="nav nav-pills">*/}
          {/*        {email.map((mail, key) => (*/}
          {/*          <li className="nav-item" key={"_li_" + key}>*/}
          {/*            <Link*/}
          {/*              className={*/}
          {/*                mail.isActive ? "nav-link active" : "nav-link"*/}
          {/*              }*/}
          {/*              to={mail.linkto}*/}
          {/*            >*/}
          {/*              {mail.title}*/}
          {/*            </Link>*/}
          {/*          </li>*/}
          {/*        ))}*/}
          {/*      </ul>*/}
          {/*    </div>*/}
          {/*    <div className="clearfix"></div>*/}
          {/*    <StackedColumnChart />*/}
          {/*  </CardBody>*/}
          {/*</Card>*/}

          {/*
          <Row>
            <Col xl="4">
              <SocialSource />
            </Col>
            <Col xl="4">
              <ActivityComp />
            </Col>

            <Col xl="4">
              <TopCities />
            </Col>
          </Row>*/}

          {/*<Row>*/}
          {/*  <Col lg="12">*/}
          {/*    <LatestTranaction />*/}
          {/*  </Col>*/}
          {/*</Row>*/}
        </Container>
      </div>
      <Modal
        isOpen={modal}
        role="dialog"
        autoFocus={true}
        centered={true}
        className="exampleModal"
        tabindex="-1"
        toggle={() => {
          setmodal(!modal)
        }}
      >
        <div className="modal-content">
          <ModalHeader
            toggle={() => {
              setmodal(!modal)
            }}
          >
            Order Details
          </ModalHeader>
          <ModalBody>
            <p className="mb-2">
              Product id: <span className="text-primary">#SK2540</span>
            </p>
            <p className="mb-4">
              Billing Name: <span className="text-primary">Neal Matthews</span>
            </p>

            <div className="table-responsive">
              <Table className="table table-centered table-nowrap">
                <thead>
                <tr>
                  <th scope="col">Product</th>
                  <th scope="col">Product Name</th>
                  <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <th scope="row">
                    <div>
                      <img src={modalimage1} alt="" className="avatar-sm"/>
                    </div>
                  </th>
                  <td>
                    <div>
                      <h5 className="text-truncate font-size-14">
                        Wireless Headphone (Black)
                      </h5>
                      <p className="text-muted mb-0">$ 225 x 1</p>
                    </div>
                  </td>
                  <td>$ 255</td>
                </tr>
                <tr>
                  <th scope="row">
                    <div>
                      <img src={modalimage2} alt="" className="avatar-sm"/>
                    </div>
                  </th>
                  <td>
                    <div>
                      <h5 className="text-truncate font-size-14">
                        Hoodie (Blue)
                      </h5>
                      <p className="text-muted mb-0">$ 145 x 1</p>
                    </div>
                  </td>
                  <td>$ 145</td>
                </tr>
                <tr>
                  <td colSpan="2">
                    <h6 className="m-0 text-right">Sub Total:</h6>
                  </td>
                  <td>$ 400</td>
                </tr>
                <tr>
                  <td colSpan="2">
                    <h6 className="m-0 text-right">Shipping:</h6>
                  </td>
                  <td>Free</td>
                </tr>
                <tr>
                  <td colSpan="2">
                    <h6 className="m-0 text-right">Total:</h6>
                  </td>
                  <td>$ 400</td>
                </tr>
                </tbody>
              </Table>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button
              type="button"
              color="secondary"
              onClick={() => {
                setmodal(!modal)
              }}
            >
              Close
            </Button>
          </ModalFooter>
        </div>
      </Modal>
    </React.Fragment>
  )
}

Dashboard.propTypes = {
  t: PropTypes.any
}

export default withTranslation()(Dashboard)
