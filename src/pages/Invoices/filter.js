import React, { useEffect, useState } from "react"
import { Button, Col, Container, Form, FormGroup, Row } from "reactstrap"
import DatePicker from "react-datepicker2"
import moment from "moment-jalaali"
import AsyncSelect from "react-select/async"
import { fetcher } from "store/actions"
import { GET_TYPES_TRANSACTION} from "helpers/url_helper"


const Filter = (props) => {

  const [remove, setRemove] = useState(false)
  const [created_at, setCreated_at] = useState(null)
  const [updated_at, setUpdated_at] = useState(null)
  const [typeInput, setTypeInput] = useState(null)
  const [statusTransaction, setstatusTransaction] = useState(null)


  useEffect(() => {
    if (remove)
            props.onGetInvoices({...props.params})
    

  }, [props.params])

// Get list type transaction
  const loadTypeTransactions = (name) => {
    return fetcher(GET_TYPES_TRANSACTION, {
      method: "GET"
    }, { query: { name: name, limit: 10 } })

      .then(r => {
        if (r?.result) {

          let type_list = []
          r?.result?.transaction_type.map(i => type_list.push({ label: i[1] , value: i[0] }))
          
          return type_list
        }
      })
      .catch(e => {
        // MessageToast("error", "با خطا مواجه شدید")
      })
}



const handleReset = (input) => {
    setTypeInput(null)
 
}

  return (
    <>
      <Form id="frm">
        <Row>

          <Col lg="4" xl="3" className="mt-2">
            <label>نوع تراکنش</label>
                <FormGroup className="ajax-select  mt-lg-0 select2-container">
                    <AsyncSelect
                        defaultOptions={true}
                        isClearable={()=>handleReset('type')}
                        name="subject-list"
                        id="subject-list"
                        loadOptions={loadTypeTransactions}
                        defaultValue={!!typeInput ? typeInput : ''}
                        className="basic-multi-select"
                        onChange={(e) => {
                          if(e){
                            setTypeInput(e.label);
                            props.setParams({ ...props.params,  type : e ?  e.value : null})
                          }
                        }
                        }
                        placeholder="جستجو کنید"
                        
                    />
                </FormGroup>
          </Col>

          <Col lg="4" xl="3" className="mt-2">
            <label>از تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={created_at}
              isGregorian={false}
              onChange={(value) => {
                if (value) {
                  setCreated_at(value)
                  setRemove(false)
                  props.setParams({ ...props.params, created_at__gt : value.format("YYYY-MM-DD") })
                }
              }}
              name="created_at"
              id="created_at"
              removable={true}
            />
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label>تا تاریخ</label>
            <DatePicker
              className="form-control"
              timePicker={false}
              value={updated_at}
              isGregorian={false}
              onChange={(value) => {
                if (value) {
                  setUpdated_at(value)
                  setRemove(false)
                  props.setParams({ ...props.params , created_at__lt : value.format("YYYY-MM-DD") })
                }
              }}
              name="updated_at"
              id="updated_at"
              removable={true}
            />
          </Col>
 
          <Col lg="4" xl="3" className="mt-2">
            {/* <label className="text-white d-block"> .</label>
            <div className="custom-control custom-checkbox custom-checkbox-primary mb-2">
              <input
                type="checkbox"
                className="custom-control-input"
                id="customCheckcolor2"
                checked={props.params?.is_approved}
                onChange={(e) => {
                  setRemove(false)
                  props.setParams({ ...props.params, is_approved: e.target.checked })
                }}
              />
              
            </div> */}
          </Col>
          <Col lg="4" xl="3" className="mt-2">
            <label className="text-white d-block"> .</label>
            <Button color="primary" onClick={()=>{
              props.onGetInvoices()
            }}>اعمال فیلتر</Button>
            <Button color="warning" className="ml-2" onClick={() => {
              setTypeInput('lkajsd');
              setstatusTransaction('lkajsd');
              document.getElementById("frm").reset();
              setCreated_at(null);
              setUpdated_at(null);
              props.setParams({});
              setRemove(true)
            }}>حذف فیلتر</Button>
              </Col>
              </Row>
              </Form>
              </>
              )
              }

              export default Filter