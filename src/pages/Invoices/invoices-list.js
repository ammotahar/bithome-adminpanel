import React, { useEffect, useState } from "react"
import { Badge, Button, Col, Container, Row, Table } from "reactstrap"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"
import { map } from "lodash"
import moment from "moment-jalaali"
//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"

//Import Card invoice
// import CardInvoice from "./card-invoice"
import { fetcher, getInvoices } from "store/actions"
import Status from "../../components/status"
import numberWithCommas from "../../components/threeNumber"
import PaginationCustom from "../../components/paginationZero"
import { convertTypeTransaction } from "../../utils/convertToPersion"
import Filter from "./filter"
import { GET_TYPES_TRANSACTION } from "helpers/url_helper"
import { MessageToast } from "utils/message"


const InvoicesList = props => {

  const { invoices, onGetInvoices ,listCount} = props
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [params, setParams] = useState({ 

    // type : '',
    // created_at__gt : '',
    // created_at__lt : ''

  })

  console.log("invoices / pageSize / page --->> " , invoices , pageSize , page);

  // useEffect(() => {
  //   onGetInvoices({ query: {page: page, size: pageSize  } })
  // }, [onGetInvoices , params]) 
   useEffect(() => {
    onGetInvoices({ query: {page: page, size: pageSize  } })
  }, [onGetInvoices ])

  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    onGetInvoices({ query: { ...params,page: p, size: s} })
  }

  const getTransaction = () => {
    setPage(0)
    onGetInvoices({ query: { page: 0, size: pageSize , ...params} })
  }




  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="حسابداری" breadcrumbItem="تراکنش‌ها" />

          <Row>
            {/*{map(invoices, (invoice, key) => (*/}
            {/*  <CardInvoice data={invoice} key={"_invoice_" + key} />*/}
            {/*))}*/}
          </Row>
          <Row>
            <Col lg="12">
              <div className="">
              <Filter 
                params={params} 
                setParams={setParams} 
                onGetInvoices={getTransaction} 
                setPage={setPage}
                />

                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped table-striped">
                    <thead>
                    <tr>
                      <th scope="col">ردیف</th>
                      <th scope="col">نام کاربر</th>
                      <th scope="col">نوع تراکنش</th>
                      <th scope="col">  مبلغ(تومان)</th>
                      {/*<th scope="col"> شماره حساب</th>*/}
                      {/* <th scope="col"> مبدا</th>
                      <th scope="col"> مقصد</th> */}

                      <th scope="col">توضیحات</th>
                      <th scope="col">تاریخ</th>
                      <th scope="col">کدپیگیری</th>
                      <th scope="col"> وضعیت</th>

                      {/*<th scope="col" className="text-right">عملیات</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {map(invoices, (project, index) => (
                      <tr key={index}>
                        <td>
                          {page == 0 ?  ++index : (page * pageSize) + ++index }
                        </td>

                        <td>
                          {(project?.extra_data?.first_name || '') + " " +(project?.extra_data?.last_name || '')}
                          <p className="text-muted mb-0 text-wrap">
                            {project?.description}
                          </p>
                        </td>
                        <td>{convertTypeTransaction(project?.type)}</td>
                        <td>{numberWithCommas(project?.amount)}</td>
                        <td>
                          {project?.extra_data?.description}
                        </td>
                        {/*<td >{project?.user_bank_account}</td>*/}
                        {/* <td >{project.extra_data?.origin}</td>
                        <td >{project.extra_data?.destination}</td> */}
                        <td>{moment(project?.created_at, "YYYY-MM-DDThh:mm:ss").format("jYYYY/jMM/jDD hh:mm")}</td>

                        <td>{project?.tracking_code?.slice(project?.tracking_code.lastIndexOf("-")+1)}</td>

                        <td>

                          <Status name={project?.status}/>

                          {/*{console.log(project?.id)}*/}
                        </td>

                      </tr>
                    ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            {/*<Col xs="12">*/}
            {/*  <div className="text-center my-3">*/}
            {/*    <Link to="#" className="text-success">*/}
            {/*      <i className="bx bx-loader bx-spin font-size-18 align-middle mr-2" />*/}
            {/*      Load more*/}
            {/*    </Link>*/}
            {/*  </div>*/}
            {/*</Col>*/}
          </Row>
        </Container>
        <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor((listCount) / pageSize ) + 1}
                          addPageSize={setPageSize}
                          pageSize={pageSize}
                          chatsCount={listCount} />
      </div>
    </React.Fragment>
  )
}

InvoicesList.propTypes = {
  invoices: PropTypes.array,
  onGetInvoices: PropTypes.func,
}

const mapStateToProps = ({ invoices }) => ({
  invoices: invoices.invoices?.result?.items || [],
  listCount: invoices.invoices?.result?.total || 0,
})

const mapDispatchToProps = dispatch => ({
  onGetInvoices: (...data) => dispatch(getInvoices(...data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(InvoicesList))
