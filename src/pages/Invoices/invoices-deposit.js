import React, { useEffect, useState } from "react"
import { Badge, Button, Col, Container, Row, Table } from "reactstrap"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"
import { map } from "lodash"
import moment from "moment-jalaali"
import {Tooltip} from 'antd';
import { BiSortDown , BiSortUp , BiSort} from "react-icons/bi";
import Breadcrumbs from "components/Common/Breadcrumb"
import { fetcher, getInvoicesDeposit } from "store/actions"
import Status from "../../components/status"
import { FaCheckCircle } from "react-icons/all"
import { DOWNLOAD_PIC_DIPOSIT, GET_INVOICES_DEPOSIT_APPROVE, GET_INVOICES_DEPOSIT_CANCEL } from "helpers/url_helper";
import { MessageToast } from "utils/message"
import numberWithCommas from "../../components/threeNumber"
import PaginationCustom from "../../components/paginationZero"
import icon_download_pic from '../../assets/images/download_pic_icon.svg';

const InvoicesDeposit = props => {
  const { invoices, onGetInvoices,listCount } = props
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [orderIndicator, setOrderIndicator] = useState(<BiSort/>)
  const [order, setOrder] = useState("-updated_at")


  useEffect(() => {
    onGetInvoices({ query: {page: page, size: pageSize, order_type:"money_deposit_bank" , order  : order  } })
  }, [onGetInvoices , order])


  const approve = (id) => {
    fetcher(GET_INVOICES_DEPOSIT_APPROVE(id), {
      method: "GET"
    }).then(r => {
      onGetInvoices()
      MessageToast("success", "تایید واریز با موفقیت انجام شد")
    }).catch(e => {
      MessageToast("error", "باخطا مواجه شدید")
    })
  }
  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    onGetInvoices({ query: { page: p, size: s , order_type:"money_deposit_bank"} , order : order })
  }

  const cancelDeposit = (id) => {
    fetcher(GET_INVOICES_DEPOSIT_CANCEL(id), {
      method: "GET"
    }).then(r => {
      
      console.log(r)
      onGetInvoices()
      MessageToast("success", "لغو واریز با موفقیت انجام شد")
    }).catch(e => {
      MessageToast("error", "باخطا مواجه شدید")
    })
  }


  const handleDownloadPic = (id) => {
    if(id){
      fetcher(DOWNLOAD_PIC_DIPOSIT(id), {
        method: "GET"
      }).then(r => {
        console.log(r)
        window.location.href = r
    })}
  }



   // function for handle set state of order when click on title header
   const handleChangeOrder = (data) =>{
    setOrder( state => orderStateChange(state, data) )
 }

 // function that get previos & current state of order for show carret icon sort and change order state
 const orderStateChange = (prevState, orderState) => {
   // when first time click on title header field that has filter, 'carret down' show and 'sort ascending'
   if(prevState === orderState){
     setOrderIndicator(<BiSortDown/>)
     return `-${orderState}` ;
     // when third time click on title header field that has filter, remove fileter and show 'default icon filter'
   }else if (prevState === `-${orderState}`){
     setOrderIndicator(<BiSort/>)
     return "-updated_at" ;
     // when second time click on title header field that has filter, 'carret up' show and 'sort descending'
   }else {
     setOrderIndicator(<BiSortUp/>)
     return orderState ;
   }
 }



  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="حسابداری" breadcrumbItem=" واریز بانکی" />

          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        شماره
                      </th>
                      <th className="text-center" scope="col">کاربر</th>
                      <th scope="col">  مبلغ(تومان)</th>
                      <th scope="col">  توضیحات</th>
                      <th scope="col"> دانلود تصویر فیش</th>
                      <th scope="col"> شماره حساب</th>

                     <th className="text-center">
                        <Tooltip title="مرتب‌سازی بر اساس  تاریخ" color={'geekblue'} ><span 
                            className="field-header-filter" 
                            onClick={ e => handleChangeOrder("created_at")}>
                            {order === "-updated_at" ?  orderIndicator : ''}{'  '}
                            { order.includes("created_at") && orderIndicator }
                            <span className="ml-1">تاریخ</span>
                          </span>
                        </Tooltip>
                     </th>

                      <th scope="col">کدپیگیری</th>

                      <th>
                        <Tooltip title="مرتب‌سازی بر اساس  وضعیت" color={'geekblue'} ><span 
                            className="field-header-filter" 
                            onClick={ e => handleChangeOrder("status")}>
                            {order === "-updated_at" ?  orderIndicator : ''}{'  '}
                            <span className="ml-1">وضعیت</span>
                          </span>
                        </Tooltip>
                      </th>

                      <th scope="col"> تایید برداشت</th>
                      <th colSpan="2" scope="col" className="text-center">عملیات</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                    {map(invoices, (project, index) => (
                      <tr key={index}>
                        <td>
                        {page == 0 ?  ++index : (page * pageSize) + ++index }
                        </td>

                        <td className="text-center">
                          {(project?.user?.full_name)}
                        </td>
                        <td className="text-center">{numberWithCommas(project?.amount)}</td>
                        <td >{project?.description}</td>
                        
                        <td className="text-center">
                          {project?.media_id ? <img 
                            onClick={()=>handleDownloadPic(project?.media_id)} 
                            src={icon_download_pic}  
                            alt="icon-pic-deposit"
                            className="icon-download_pic"
                            /> : null}
                        </td>
                        <td >{project?.user_bank_account}</td>
                           <td className="text-center">{moment(project?.created_at, "YYYY-MM-DD").format("hh:mm - jYYYY/jMM/jDD")}</td>

                        <td>{project?.tracking_code}</td>

                        <td>
                          <Status name={project?.status}/>
                        </td> 
                        
                        <td>
                          {(project?.status==="pending") ?
                            <Button type="submit" color="primary"  onClick={()=>approve(project?.id)}>
                              تایید واریز
                            </Button> :<span>
                             <span className="text-info pr-1"><FaCheckCircle/></span>
                            تایید شده</span>}

                        </td>


                        <td>
                          {(project?.status !=="success" && project?.status !=="cancel") ?
                            <Button type="submit" color="danger"  onClick={()=>cancelDeposit(project?.id)}>
                              لغو واریز
                            </Button> :
                            <Button type="submit"   disabled>
                              لغو واریز
                            </Button>}

                        </td>
                      </tr>
                    ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <PaginationCustom handlePageClick={handlePageClick} page={page} totalPage={Math.floor((listCount) / pageSize ) + 1}
                          addPageSize={setPageSize}
                          pageSize={pageSize}
                          chatsCount={listCount} />
      </div>
    </React.Fragment>
  )
}

InvoicesDeposit.propTypes = {
  invoices: PropTypes.array,
  onGetInvoices: PropTypes.func,
}

const mapStateToProps = ({ invoices }) => ({
  invoices: invoices.invoicesDeposit?.result?.items || [],
  listCount: invoices.invoicesDeposit?.result?.total || 0,
})

const mapDispatchToProps = dispatch => ({
  onGetInvoices: (...data) => dispatch(getInvoicesDeposit(...data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(InvoicesDeposit))
