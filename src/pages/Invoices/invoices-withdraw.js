import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Table } from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {withRouter } from "react-router-dom";
import { map } from "lodash";
import moment from "moment-jalaali";
import Breadcrumbs from "components/Common/Breadcrumb";
import { fetcher, getInvoicesWithdraw } from "store/actions";
import Status from "../../components/status";
import {Tooltip} from 'antd';
import { BiSortDown , BiSortUp , BiSort} from "react-icons/bi";
import {
  GET_INVOICES_WITHDRAW_APPROVE,
  GET_INVOICES_WITHDRAW_STATUS,
  GET_INVOICES_WITHDRAW_CANCEL
} from "helpers/url_helper"
import { MessageToast } from "../../utils/message"
import { FaCheckCircle, FaTimesCircle } from "react-icons/all"
import numberWithCommas from "../../components/threeNumber"
import PaginationCustom from "../../components/paginationZero"


const InvoicesWithdraw = props => {
  const { invoices, onGetInvoices, listCount } = props
  const [page, setPage] = useState(0)
  const [pageSize, setPageSize] = useState(50)
  const [orderIndicator, setOrderIndicator] = useState(<BiSort/>)
  const [order, setOrder] = useState("-updated_at")

  useEffect(() => {
    onGetInvoices({ query: { page: page, size: pageSize, order_type: "money_withdraw" , order : order} })
  }, [onGetInvoices , order])

  const handlePageClick = (p = 0, s = 50) => {
    setPage(p)
    onGetInvoices({ query: { page: p, size: s, order_type: "money_withdraw" , order : order } })
  }

  const status = (id) => {
    fetcher(GET_INVOICES_WITHDRAW_STATUS(id), {
      method: "GET"
    }).then(r => {
      onGetInvoices()
      MessageToast("success", "تایید برداشت با موفقیت انجام شد")
    }).catch(e => {
      MessageToast("error", "باخطا مواجه شدید")
    })
  }
  const approve = (id) => {
    fetcher(GET_INVOICES_WITHDRAW_APPROVE(id), {
      method: "GET"
    }).then(r => {
      onGetInvoices()
      MessageToast("success", "تسویه حساب با موفقیت انجام شد")
    }).catch(e => {
      MessageToast("error", "باخطا مواجه شدید")
    })
  }

  const cancelWithdraw = (id) => {
    fetcher(GET_INVOICES_WITHDRAW_CANCEL(id), {
      method: "GET"
    }).then(r => {
      onGetInvoices()
      MessageToast("success", "لغو برداشت با موفقیت انجام شد")
    }).catch(e => {
      MessageToast("error", "باخطا مواجه شدید")
    })
  }



   // function for handle set state of order when click on title header
   const handleChangeOrder = (data) =>{
    setOrder( state => orderStateChange(state, data) )
 }

 // function that get previos & current state of order for show carret icon sort and change order state
 const orderStateChange = (prevState, orderState) => {
   // when first time click on title header field that has filter, 'carret down' show and 'sort ascending'
   if(prevState === orderState){
     setOrderIndicator(<BiSortDown/>)
     return `-${orderState}` ;
     // when third time click on title header field that has filter, remove fileter and show 'default icon filter'
   }else if (prevState === `-${orderState}`){
     setOrderIndicator(<BiSort/>)
     return "-updated_at" ;
     // when second time click on title header field that has filter, 'carret up' show and 'sort descending'
   }else {
     setOrderIndicator(<BiSortUp/>)
     return orderState ;
   }
 }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="حسابداری" breadcrumbItem=" تایید برداشت"/>
          <Row>
            <Col lg="12">
              <div className="">
                <div className="table-responsive">
                  <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                    <thead>
                    <tr>
                      <th scope="col" className="text-center">
                        شماره
                      </th>
                      <th scope="col">کاربر</th>
                      <th scope="col"> مبلغ(تومان)</th>
                      <th scope="col"> شماره حساب</th>

                      <th className="text-center">
                        <Tooltip title="مرتب‌سازی بر اساس  تاریخ" color={'geekblue'} ><span 
                            className="field-header-filter" 
                            onClick={ e => handleChangeOrder("created_at")}>
                            {order === "-updated_at" ?  orderIndicator : ''}{'  '}
                            { order.includes("created_at") && orderIndicator }
                            <span className="ml-1">تاریخ</span>
                          </span>
                        </Tooltip>
                     </th>
                      <th scope="col">کدپیگیری</th>
                      <th>
                        <Tooltip title="مرتب‌سازی بر اساس  وضعیت" color={'geekblue'} ><span 
                            className="field-header-filter" 
                            onClick={ e => handleChangeOrder("status")}>
                            {order === "-updated_at" ?  orderIndicator : ''}{'  '}
                            <span className="ml-1">وضعیت</span>
                          </span>
                        </Tooltip>
                      </th>
                      <th scope="col">تایید برداشت</th>
                      <th scope="col">توضیحات</th>
                      <th scope="col"> تسویه</th>
                      <th colSpan="3" scope="col" className="text-center">عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    {map(invoices, (project, index) => (
                      <tr key={index}>
                        <td>
                        {page == 0 ?  ++index : (page * pageSize) + ++index }
                        </td>

                        <td>
                          {(project?.user?.full_name)}
                        </td>
                        <td>{numberWithCommas(project?.amount)}</td>
                        <td>{project?.user_bank_account}</td>
                        <td>{moment(project?.created_at, "YYYY-MM-DDThh:mm:ss").format("jYYYY/jMM/jDD hh:mm")}</td>

                        <td>{
                          project?.tracking_code?.slice(project?.tracking_code.lastIndexOf("-") + 1)
                        }</td>

                        <td>
                          <Status name={project?.status}/>
                        </td>
                        <td>
                          {project?.status === "pending" ?
                            <Button type="button" color="primary" onClick={() => status(project?.id)}>
                              تایید برداشت
                            </Button> :
                            (project?.status !== "cancel" ? <span>
                            <span className="text-info pr-1"><FaCheckCircle/></span>
                            تایید شده
                          </span> : <span>
                            <span className="text-danger pr-1"><FaTimesCircle/></span>
                            لغو شده
                          </span>)}
                        </td>
                        <td>{project?.description}</td>
                        <td>

                          {((project?.status === "pending") || (project?.status === "active")) ?
                            <Button type="button" color="success" onClick={() => approve(project?.id)}>
                              تسویه حساب
                            </Button> :
                            (project?.status !== "cancel" ? <span>
                            <span className="text-success pr-1"><FaCheckCircle/></span>
                            تسویه شده
                          </span> : <span>
                            <span className="text-danger pr-1"><FaTimesCircle/></span>
                            لغو شده
                          </span>)}

                        </td>


                        <td>

                          {((project?.status === "pending") || (project?.status !== "success") || (project?.status === "active")) && (project?.status !== "cancel") ?
                            <Button type="button" color="danger" onClick={() => cancelWithdraw(project?.id)}>
                              لغو برداشت
                            </Button> : <Button type="button" disabled>
                              لغو برداشت
                            </Button>}

                        </td>
                      </tr>
                    ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <PaginationCustom handlePageClick={handlePageClick} page={page}
                          totalPage={Math.floor((listCount) / pageSize) + 1}
                          addPageSize={setPageSize}
                          pageSize={pageSize}
                          chatsCount={listCount}/>
      </div>
    </React.Fragment>
  )
}

InvoicesWithdraw.propTypes = {
  invoices: PropTypes.array,
  onGetInvoices: PropTypes.func
}

const mapStateToProps = ({ invoices }) => ({
  invoices: invoices.invoicesWithdraw?.result?.items || [],
  listCount: invoices.invoicesWithdraw?.result?.total || 0
})

const mapDispatchToProps = dispatch => ({
  onGetInvoices: (...data) => dispatch(getInvoicesWithdraw(...data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(InvoicesWithdraw))

