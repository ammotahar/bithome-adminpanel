import React, { useEffect, useState } from "react"
import { Button, Col, Container, Row, Spinner, Table } from "reactstrap"
import Breadcrumbs from "../../components/Common/Breadcrumb"
import { fetcher } from "../../store/common/action"
import { GET_REPORTS_ACTIVITY } from "../../helpers/url_helper"
import { MessageToast } from "../../utils/message"
import List from './tableReport'
import PaginationCustom from "components/paginationZero"
import Filter from "./filter"

function ReportAdmin(props) {
  
    const [reports, setReports] = useState([])
    
    const [page, setPage] = useState(0)
    const [counts, setCounts] = useState(0)
    const [pageSize, setPageSize] = useState(50)
    const [params, setParams] = useState({ order: "-updated_at" })

      useEffect(() => {
        getReports()
      }, [])

      // get list activities admin 
      const getReports = (p = 0, s = pageSize) => {
          fetcher(GET_REPORTS_ACTIVITY, {
            method: "GET"
          },{query:{page: p, size: s, owner_id : props?.match?.params?.id ,  ...params}}).then(r => {
          if (r?.result) {
            setReports(r?.result?.items)
            setCounts(r?.result?.total)
          }
        }).catch(e => {
          // MessageToast("error", "دریافت اطلاعات با خطا مواجه شد ، صفحه را دوباره لود کنید")
        })
      }
    
      const handlePageClick = (p = 0 , s = pageSize) => {
        setPage(p)
        getReports(p, s)
      }
    
      
      return (
        <React.Fragment>
          <div className="page-content">
            <Container fluid>
              {/* Render Breadcrumbs */}
              <Breadcrumbs title="ادمین" breadcrumbItem="گزارش فعالیت" />

              <Filter params={params} setParams={setParams} getReports={getReports} />
            
                {/* table for show list activity of admin */}
                <List reports={reports} counts={counts} pageSize={pageSize} page={page} />

                <PaginationCustom handlePageClick={handlePageClick} 
                    page={page}
                    totalPage={Math.floor((counts) / pageSize ) + 1}
                    addPageSize={setPageSize}
                    pageSize={pageSize}
                    chatsCount={counts} 
                />
             
            </Container>
          </div>
        </React.Fragment>
      )
    }

export default ReportAdmin;
