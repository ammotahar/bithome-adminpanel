import React , {useState} from 'react';
import { Button, Col, Container, Row, Spinner, Table  } from "reactstrap";
import { Link } from "react-router-dom";
import moment from "moment-jalaali";


function tableReport({reports , page , pageSize}) {

 
    return (
        <>
      <div>
        <Row>
          <Col lg="12">


            <div className="">
              <div className="table-responsive">
                <Table className="project-list-table table-nowrap table-centered table-borderless table-striped">
                  <thead>
                  <tr>
                    <th className="text-center" scope="col">شماره</th>
                    <th className="text-center" scope="col">زمان و تاریخ ورود</th>
                    <th className="text-center" scope="col">زمان و تاریخ فعالیت</th>
                    <th className="text-center" scope="col">نوع فعالیت</th>
                    <th className="text-center" scope="col">جزئیات فعالیت</th>
                  </tr>
                  </thead>
                  <tbody>
                  {reports?.length && reports?.map((item, i) =>

                     <tr key={item?.id}>
                        <td className="text-center" >{page == 0 ?  ++i : (page * pageSize) + ++i}</td>
                        <td className="text-center" >
                          {item?.login_datetime ? moment(item?.login_datetime).format("HH:mm - jYYYY/jMM/jDD") : ''}
                        </td>
                        <td className="text-center" >
                          {item?.created_at ? moment(item?.created_at).format("HH:mm - jYYYY/jMM/jDD") : ''}
                        </td>
                        <td className="text-center" >
                          {item?.tracker_template?.title_fa}
                        </td>
                        <td className="text-center" >
                          {item?.body}
                        </td>
                      </tr>

                      )
                  }
                  </tbody>
                </Table>
              </div>
            </div>

          </Col>
        </Row>

      </div>
    </>
    )
}

export default tableReport;
